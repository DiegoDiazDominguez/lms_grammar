//
// Created by diegodiaz on 25-09-20.
//

#include "../include/cdt/se_int_queue.h"
#include "../include/cdt/hash_table.hpp"
#include "../include/cdt/int_array.h"
#include "../include/cdt/vlc_int_array.hpp"
#include "../include/cdt/si_int_array.h"

#include <vector>
#include <unordered_set>
int main(int argc, char** argv){
    /*se_int_queue<uint8_t> queue("some_file",8);
    queue.push_back(0);
    queue.push_back(1);
    queue.push_back(2);
    queue.pop();
    queue.pop();
    queue.pop();
    queue.push_back(2);
    queue.push_back(5);
    queue.pop();
    queue.pop();
    queue.push_back(9);
    queue.push_back(0);
    queue.push_back(3);
    queue.push_back(4);
    queue.push_back(5);
    queue.push_back(6);
    queue.push_back(7);
    queue.push_back(8);
    queue.push_back(2);
    std::cout<<(int)queue.pop()<<std::endl;
    std::cout<<(int)queue.pop()<<std::endl;
    std::cout<<(int)queue.pop()<<std::endl;
    std::cout<<(int)queue.pop()<<std::endl;
    std::cout<<(int)queue.pop()<<std::endl;*/


    /*typedef int_array<unsigned int> string_t;
    typedef std::basic_string<unsigned int> string2_t;

    size_t buffer_size = 1024*1024*500;
    void * buffer = malloc(buffer_size);
    bit_hash_table<size_t> map1(buffer_size, "dump_file.txt", 0.8);

    size_t width = 1+(rand() % 32);
    size_t max_val = (1UL<<width)-1UL;
    size_t val, val2;
    std::vector<int_array<unsigned int>> tmp_vector;

    for(size_t i=0;i<10000000;i++){

        size_t cap = 3+(rand() % 500);
        string_t  tmp_string(cap, width);
        val = rand();

        std::basic_string<unsigned int> tmp_string2;
        for(size_t j=0;j<cap;j++){
            size_t sym = (rand() % max_val)+1;
            tmp_string.push_back(sym);
            tmp_string2.push_back(sym);
        }
        tmp_string.mask_tail();
        auto res1 = map1.insert(tmp_string.data(), tmp_string.n_bits(), val);
        auto res2 = map1.find(tmp_string.data(), tmp_string.n_bits());
        assert(res2.second);

        if(res1.second){
            map1.get_value_from(*res2.first, val2);
            assert(*res2.first==*res1.first && val==val2);
        }
    }
    map1.flush();
    free(buffer);
    std::cout<<"termino"<<std::endl;*/

    /*
    std::random_device rd; // obtain a random number from hardware
    std::mt19937 gen(rd()); // seed the generator
    std::uniform_int_distribution<size_t> distr(0, std::numeric_limits<size_t>::max()); // define the range

    vlc_int_array<elias_delta> vlc;
    std::vector<size_t> tmp_vector;
    for(size_t i=0;i<100000000;i++){
        size_t tmp = distr(gen) & masks[52];
        vlc.push_back(tmp);
        assert(vlc.back()==tmp);
        tmp_vector.push_back(tmp);
    }
    vlc.shrink_to_fit();

    vlc_int_array<elias_delta> vlc2 = vlc;
    auto it2 = vlc2.begin();
    auto end2 = vlc2.end();

    size_t i=0;
    for(auto it=vlc.begin();it!=vlc.end();++it){
        assert(*it==tmp_vector[i]);
        assert(*it==*it2);
        i++;
        ++it2;
    }
    assert(it2==end2);

    i=0;
    for(auto const& elm : vlc2){
        assert(elm==tmp_vector[i]);
        i++;
    }
    std::cout<<double(tmp_vector.size()*sizeof(size_t))/1000/1000<<" "<<double(vlc.buffer_bytes())/1000/1000<<std::endl;*/

    /*for(auto const& elm : vlc){
        std::cout<<elm<<std::endl;
    }*/
    std::random_device rd; // obtain a random number from hardware
    std::mt19937 gen(rd()); // seed the generator
    std::uniform_int_distribution<size_t> distr(1, 1000); // define the range

    std::vector<size_t> tmp_vector2;
    si_int_array si;

    size_t val =0;
    for(size_t i=0;i<100000000;i++){
        size_t tmp = distr(gen) & masks[52];
        val +=tmp;
        si.push_back(val);
        assert(si.back()==val);
        tmp_vector2.push_back(val);
    }
    si.shrink_to_fit();

    size_t i=0;
    for(auto const& elm : si){
        assert(elm==tmp_vector2[i]);
        i++;
    }
    assert(tmp_vector2.size()==si.size());

    std::cout<<double(tmp_vector2.size()*sizeof(size_t))/1000/1000<<" "<<double(si.buffer_bytes())/1000/1000<<std::endl;
}
