//
// Created by diego on 22-04-20.
//
#include "lpg2bwt/protbwt_algo.hpp"

void protbwt_algo(const lpg &g, alphabet &alph, sdsl::cache_config &config, size_t step) {

    std::cout<<"Building the prototype eBWT of the compressed string"<<std::endl;
    //ind_data ind(std::move(alph), trit_vector_t());

    auto it = g.g_begin();

    vector_t bck_counts(alph.size()+2, 0, sdsl::bits::hi(g.tree.nodes())+1);

    //create the table with the nodes
    ++it;//skip the root node
    std::ptrdiff_t root_children = g.tree.n_children(lpg::root);
    while(it.map <= (root_children+1)){
        bck_counts[alph.lexrank(it.label)]++;
        ++it;
    }
    assert(bck_counts[0]==0);

    //place the nodes in the array to be sorted
    it-=root_children;//move the iterator to the first child of the root
    ind_table table(root_children, bck_counts);
    table.set_mode(L_TYPE);
    table.init_pointers();
    while(it.map <= (root_children+1)){
        table.push(alph.lexrank(it.label), it.map);
        ++it;
    }

    //I substract one to not consider the root
    std::cout<<"  There are "<<g.tree.n_ints(g.tree.child(lpg::root, root_children))-1<<" distinct symbols"<<std::endl;
    std::cout<<"  Computing suffix nonterminals"<<std::endl;
    bit_vector_t suff_nodes = get_suffix_nodes(g);

    //circular sort of the nonterminals at depth one
    std::cout<<"  Quick-sorting circular suffixes of the compressed string"<<std::endl;
    sort_nt(g, alph, table, suff_nodes);

    std::cout<<"  Producing prototype eBWT from the sorted suffixes"<<std::endl;
    bwt_wrapper bwt_wrapper(g, alph, table, suff_nodes, config);
    sdsl::store_to_file(bwt_wrapper, sdsl::cache_file_name("bwt_wrapper", config));
    sdsl::register_cache_file("bwt_wrapper", config);
}

//suffix node: a node whose last terminal symbol is a separator symbol
bit_vector_t get_suffix_nodes(const lpg &g){

    size_t tmp_node, label, int_rank;
    std::ptrdiff_t root_children = g.tree.n_children(lpg::root);

    //!note: we need to do this to make circular references of the strings in the BWT
    // expands to is a separator symbol
    //0: node has not been visited
    //1: node is not suffix
    //2: node is suffix
    trit_vector_t sep_nt(g.tree.int_nodes(), 0);
    bit_vector_t suffix_nodes(root_children+2, false);

    bool continue_trav, is_suffix;
    std::stack<size_t> stack;

    auto it = g.g_begin();
    it++;//skip the root

    while(it.map <= (root_children+1)){

        if(it.children!=0){

            tmp_node = it.id;

            //TODO: can we have a terminal as a child of the root?
            continue_trav = it.label>g.sigma;

            while(continue_trav){

                stack.push(tmp_node);

                //get the right-most pt_child
                tmp_node = g.tree.child(tmp_node, g.tree.n_children(tmp_node));

                if(g.tree.is_leaf(tmp_node)){

                    label = g.label(tmp_node);
                    if(label<=g.sigma){//it points to a terminal symbol
                        is_suffix = label==1; //is the pointed terminal a sep symbol?
                        while(!stack.empty()){ //mark all the ancestors
                            tmp_node = stack.top();
                            stack.pop();
                            int_rank = g.tree.int_rank(tmp_node);
                            sep_nt[int_rank - 1] = (is_suffix + 1);
                        }
                        continue_trav = false;
                        suffix_nodes[it.map] = is_suffix;
                    }else{
                        int_rank = label-g.sigma;
                        tmp_node = g.tree.int_select(int_rank);

                        if(sep_nt[int_rank - 1]){
                            is_suffix = sep_nt[int_rank - 1] == 2;
                            //stack.push(tmp_node);
                            while(!stack.empty()){
                                tmp_node = stack.top();
                                stack.pop();
                                int_rank = g.tree.int_rank(tmp_node);
                                sep_nt[int_rank - 1] = is_suffix + 1;
                            }
                            continue_trav = false;
                            suffix_nodes[it.map] = is_suffix;
                        }
                    }
                }
            }
        }else{
            suffix_nodes[it.map] = (it.label==1) ||
                    (it.label>g.sigma && sep_nt[it.label-g.sigma-1]==2);
        }
        it++;
    }
    return suffix_nodes;
}

bool smaller_nt(const lpg &g, size_t node_map_a, size_t node_map_b, const alphabet &alph, const bit_vector_t &suffix_nt) {

    size_t rank_a, rank_b, next_a, next_b;

    rank_a = alph.lexrank(g.label(g.tree.node_select(node_map_a)));
    rank_b = alph.lexrank(g.label(g.tree.node_select(node_map_b)));
    next_a = circular_next(node_map_a, suffix_nt);
    next_b = circular_next(node_map_b, suffix_nt);

    while(rank_a==rank_b && next_a != node_map_a && next_b != node_map_b){
        rank_a = alph.lexrank(g.label(g.tree.node_select(next_a)));
        rank_b = alph.lexrank(g.label(g.tree.node_select(next_b)));

        next_a = circular_next(next_a, suffix_nt);
        next_b = circular_next(next_b, suffix_nt);
    }
    return rank_a<rank_b;
}

long quick_sort_par(const lpg &g, const alphabet& alph, ind_table& table, const bit_vector_t &suffix_nt,
                    long low_idx, long high_idx) {

    long pivot = table.sa[high_idx], tmp;
    long i = low_idx-1;
    for(long j=low_idx;j<=high_idx-1;j++){
        if(smaller_nt(g, table.sa[j], pivot, alph, suffix_nt)){
            i++;
            tmp = table.sa[i];
            table.sa[i] = table.sa[j];
            table.sa[j] = tmp;
        }
    }

    tmp = table.sa[i+1];
    table.sa[i+1] = table.sa[high_idx];
    table.sa[high_idx] = tmp;
    return i+1;
}

void quick_sort(const lpg& g , const alphabet& alph, ind_table& table, const bit_vector_t &suffix_nt, long low_idx, long high_idx){

    if(low_idx<high_idx){
        long pi = quick_sort_par(g, alph, table, suffix_nt, low_idx, high_idx);
        quick_sort(g, alph, table, suffix_nt, low_idx, pi - 1);
        quick_sort(g, alph, table, suffix_nt, pi + 1, high_idx);
    }
}

void sort_nt(const lpg& g, const alphabet& alph, ind_table& table, const bit_vector_t &suff_nodes) {

    for(size_t i=1;i<=alph.size();i++){
        auto bck_locus = table.bucket_locus(i);
        if(bck_locus.second>1){
            /*std::cout<<"before: "<<std::endl;
            for(size_t j=bck_locus.first;j<=(bck_locus.first+bck_locus.second-1);j++){
                std::cout<<suff_nodes[ind.table.sa[j]-2]<<" "<<suff_nodes[ind.table.sa[j]-1]<<" "<<ind.table.sa[j]<<" : ";
                circular_print(g, ind.table.sa[j], ind, suff_nodes);
                if(suff_nodes[ind.table.sa[j]-2] && suff_nodes[ind.table.sa[j]-1]){
                    std::cout<<g.decompress_node(g.lg_tree.node_select(ind.table.sa[j]))<<std::endl;
                    std::cout<<g.decompress_node(g.lg_tree.node_select(ind.table.sa[j])-1)<<std::endl;
                }
            }*/
            quick_sort(g, alph, table, suff_nodes,
                       bck_locus.first, long(bck_locus.first + bck_locus.second) - 1L);
            /*std::cout<<"after: "<<std::endl;
            for(size_t j=bck_locus.first;j<=(bck_locus.first+bck_locus.second-1);j++){
                std::cout<<suff_nodes[ind.table.sa[j]-2]<<" "<<suff_nodes[ind.table.sa[j]-1]<<" "<<ind.table.sa[j]<<" : ";
                circular_print(g, ind.table.sa[j], ind, suff_nodes);
            }*/
        }
    }
}
