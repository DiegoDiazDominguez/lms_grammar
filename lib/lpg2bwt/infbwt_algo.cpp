//
// Created by diego on 22-04-20.
//
#include <unordered_set>
#include "lpg2bwt/infbwt_algo.hpp"

size_t infbwt_algo(const lpg& g,  sdsl::cache_config& config, size_t step){
    infer_bwt_level(g, config, --step);
    return 0;
}

void infer_bwt_level(const lpg &g, sdsl::cache_config &config, size_t step) {

    std::cout << "Building the BWT for level " << step << std::endl;

    //ind_data ind;
    //sdsl::load_from_file(ind, ind_file);


    /*if (remove(bwt_w_file.c_str()) != 0){
        std::cout<<"Error deleting file "<<bwt_w_file<<" exiting .."<<std::endl;
        exit(1);
    }*/
    std::string ind_file = sdsl::cache_file_name("ind_data_" + std::to_string(step), config);
    std::ifstream in(ind_file, std::ios::binary | std::ios::in);
    trit_vector_t sym_status;

    sym_status.load(in);
    link_left_symbols(g, sym_status, config);
}

void link_left_symbols(const lpg &g, trit_vector_t& sym_status, sdsl::cache_config& config) {

    //scale the elements in the previous BWT to this grammar tree level
    //resort the pointers of the SP nodes
    //count the new frequencies of S-nodes per bucket!!
    size_t g_symbol, map, node, bwt_symbol, run_len, status;

    buffer_t bwt_blocks(sdsl::cache_file_name("bwt_blocks", config),
                        std::ios::out,
                       4*1024*1024,
                       sdsl::bits::hi(g.tree.nodes())+1);
    buffer_t extra_phrases(sdsl::cache_file_name("extra_phrases", config),
                           std::ios::out,
                        4*1024*1024,
                        sdsl::bits::hi(g.tree.nodes())+1);

    bwt_wrapper bwt_w;
    sdsl::load_from_file(bwt_w, sdsl::cache_file_name("bwt_wrapper", config));
    vector_t bwt_bck_pointers(bwt_w.n_buckets() + 1, 0, sdsl::bits::hi(bwt_w.m_max_run) + 1);
    chaining_data c_data(g, sym_status, bwt_w);
    bool encapsulated;
    for (size_t i = 1; i <= bwt_w.n_buckets(); i++) {

        auto const locus = bwt_w.bck_locus(i);
        size_t pos = locus.first.first;

        for (size_t j = locus.second.first; j <= locus.second.second; j++) {

            bwt_symbol = bwt_w.bwt[j];
            run_len = 0;

            while (!bwt_w.runs[pos] && pos < locus.first.second) {
                run_len++;
                pos++;
            }

            run_len++;
            pos++;

            map = bwt_symbol;
            node = g.tree.node_select(map);
            g_symbol = g.label(node);
            status = sym_status[g_symbol];
            encapsulated = false;

            //while the value is not a symbol in the current alphabet
            while (status != 4 && status != 5) {
                encapsulated = true;

                // this is an LMS occurrence
                if ((status != 1 && map == bwt_symbol) || c_data.has_unch_occ(g_symbol)) {
                    size_t bucket = bwt_w.gsym2bwtbucket(g_symbol);
                    size_t pointer = bwt_bck_pointers[bucket];
                    auto next_locus = bwt_w.gsym2bwtlocus(g_symbol);
                    size_t start = next_locus.first.first + pointer;
                    size_t end = next_locus.first.second + pointer + run_len - 1;
                    c_data.chain(g_symbol, start, end, bwt_blocks);
                    bwt_bck_pointers[bucket] += run_len;
                } else {
                    bwt_blocks.push_back(g_symbol);
                    bwt_blocks.push_back(map - 1);
                    bwt_blocks.push_back(run_len);
                }
                //rightmost child of its parent
                node = g.pt_rm_child(node);
                map = g.tree.node_map(node);
                g_symbol = g.label(node);
                status = sym_status[g_symbol];
            }
            assert(status == 4 || status == 5);

            if (status == 5) {
                if (encapsulated) {
                    bwt_blocks.push_back(g_symbol);
                    bwt_blocks.push_back(map - 1);
                    bwt_blocks.push_back(run_len);
                } else {
                    size_t bucket = bwt_w.gsym2bwtbucket(g_symbol);
                    size_t pointer = bwt_bck_pointers[bucket];
                    auto next_locus = bwt_w.gsym2bwtlocus(g_symbol);
                    size_t start = next_locus.first.first + pointer;
                    size_t end = next_locus.first.second + pointer + run_len - 1;
                    c_data.chain(g_symbol, start, end, bwt_blocks);
                    bwt_bck_pointers[bucket] += run_len;
                }
            } else {
                extra_phrases.push_back(i);
                extra_phrases.push_back(map);
                extra_phrases.push_back(run_len);
            }
        }
    }
}

void sort_extra_phrases(const lpg& g, const trit_vector_t & sym_status, const alphabet& alph, sdsl::cache_config& config){

    buffer_t extra_phrases(sdsl::cache_file_name("extra_phrases", config),
            std::ios::in,
            4*1024*1024,
            sdsl::bits::hi(g.tree.nodes())+1);

    std::unordered_map<size_t, size_t> counts;
    for(size_t i=0;i<extra_phrases.size();i++){
        if(((i+1)%3)==2) counts[extra_phrases[i]]++;
    }
    std::vector<std::tuple<size_t, size_t, size_t>> sorted_pairs;

    sorted_pairs.reserve(counts.size());
    for(auto const& elm : counts){
        sorted_pairs.emplace_back(elm.first, elm.second, alph.lexrank(g.label(g.tree.node_select(elm.first))));
    }

    std::sort(sorted_pairs.begin(), sorted_pairs.end(),
            [](std::tuple<size_t, size_t, size_t> a, std::tuple<size_t, size_t, size_t> b){
        return std::get<2>(a)<std::get<2>(b);
    });



}