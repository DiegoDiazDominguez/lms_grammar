//
// Created by diego on 13-07-20.
//

//#include "lpg2bwt/glex_algo.hpp"
#include "cdt/macros.h"
#include <thread>
#include <chrono>

/*
void ind_data::l_type_ind(const lpg &g) {

    size_t node, p_node, map, label, ind_bucket, curr_bucket, ptr_val, ptr_idx=0;

    auto ptr_it = suffix_pts.arr.begin();
    std::pair<size_t, size_t>  ptr_loci;

    curr_bucket = 1;
    long next_bucket_start = table.bck_limit(curr_bucket + 1);

    auto it_sa = table.sa.begin();
    for(long i=0;i<table.size();i++){

        if(i == next_bucket_start){
            curr_bucket++;
            next_bucket_start = table.bck_limit(curr_bucket + 1);
        }

        map = *it_sa;
        ++it_sa;

        if(map==0) continue;//empty element

        node = g.tree.node_select(map);

        if(!g.tree.is_leaf(node)) { //internal node

            label = g.tree.int_rank(node)+g.sigma;
            assert(sym_status[label]);

            if(sym_status[label] == 1U) continue;

            if (sym_status[label] >= 6U) { // it has pointers

                ptr_loci = suffix_pts.get_list_locus(label);

                //this is just to move the pointer inside the array
                if(ptr_loci.first > ptr_idx){
                    ptr_it+= ptr_loci.first - ptr_idx;
                }else{
                    ptr_it-= ptr_idx - ptr_loci.first;
                }
                ptr_val = map+*ptr_it;

                for (size_t j=0; j < ptr_loci.second; j++) {

                    p_node = g.tree.node_select(ptr_val-1);

                    ind_bucket = alph.lexrank(g.label(p_node));
                    assert(ind_bucket!=0);
                    if(ind_bucket>=curr_bucket){
                        if(g.tree.is_lm_child(p_node)){
                            p_node = g.tree.parent(p_node);
                            table.push(ind_bucket, g.tree.node_map(p_node));
                        }else{
                            table.push(ind_bucket, ptr_val-1);
                        }
                    }

                    ptr_it++;
                    ptr_val+=*ptr_it;
                }
                ptr_idx = ptr_loci.first + ptr_loci.second;
            }
            //note: three different status can reach this segment:
            //4 = 100 a node that represents a symbol in the current alphabet
            //3 = 011 a node with an occurrence as a suffix and with one or more LMS occurrences (the first occurrence is in the suffix occ)
            //2 = 010 a node with only once suffix occurrence : this happens when there are several
            // occurrences of the pair, but they are all preceded by the same symbol
        }

        if(g.tree.parent(node)==lpg::root) continue;//parent is the root, i.e, it is an lms node
        node = g.tree.p_sibling(node);
        if(node==0) continue; // left most pt_child of its parent, i.e, it is an lms node in this context

        ind_bucket = alph.lexrank(g.label(node));
        if(ind_bucket==0) continue; // the prev sibling is not in the alphabet, i.e, it is an lms node

        if(ind_bucket>=curr_bucket){
            if(g.tree.is_lm_child(node)){
                node = g.tree.parent(node);
                table.push(ind_bucket, g.tree.node_map(node));
            }else{
                table.push(ind_bucket, map-1);
            }
        }
    }
}

void ind_data::s_type_ind(const lpg &g) {

    size_t  node, p_node, map, label, ind_bucket, curr_bucket, ptr_val, ptr_idx=0;
    auto ptr_it = suffix_pts.arr.begin();
    std::pair<size_t, size_t>  ptr_loci;

    curr_bucket = alph.size();
    long prev_lex_bucket_end = table.bck_limit(curr_bucket) - 1;
    long next_av_pos;

    auto it_sa = table.sa.end();
    --it_sa;

    for(long i=table.size();i-->0;){

        if(i == prev_lex_bucket_end){
            curr_bucket--;
            prev_lex_bucket_end = table.bck_limit(curr_bucket) - 1;
            //bck_suff_limit = table.bck_suff_limit(curr_bucket);
        }

        next_av_pos = table.next_av_pos(curr_bucket);
        map = *it_sa;
        --it_sa;

        if(map==0) continue;//empty element

        node = g.tree.node_select(map);

        if(!g.tree.is_leaf(node)) {//internal node

            label = g.tree.int_rank(node)+g.sigma;
            assert(sym_status[label]);

            if(sym_status[label] == 1U) continue;

            if (sym_status[label] >= 6U) { // it has pointers

                ptr_loci = suffix_pts.get_list_locus(label);

                //this is just to move the pointer inside the array
                if(ptr_loci.first > ptr_idx){
                    ptr_it+= ptr_loci.first - ptr_idx;
                }else{
                    ptr_it-= ptr_idx - ptr_loci.first;
                }
                ptr_val = *ptr_it + map;

                for (size_t j=0; j < ptr_loci.second; j++) {

                    p_node = g.tree.node_select(ptr_val-1);

                    ind_bucket = alph.lexrank(g.label(p_node));
                    assert(ind_bucket!=0);
                    if(ind_bucket < curr_bucket ||
                       (ind_bucket == curr_bucket && i > next_av_pos)){

                        if(g.tree.is_lm_child(p_node)){
                            p_node = g.tree.parent(p_node);
                            table.push(ind_bucket, g.tree.node_map(p_node));
                        }else{
                            table.push(ind_bucket, ptr_val-1);
                        }
                    }

                    ptr_it++;
                    ptr_val+=*ptr_it;
                }
                ptr_idx = ptr_loci.first + ptr_loci.second;
            }
            //note: three different status can reach this segment:
            //4 = 100 a node that represents a symbol in the current alphabet
            //3 = 011 a node with an occurrence as a suffix and with one or more LMS occurrences (the first occurrence is in the suffix occ)
            //2 = 010 a node with only once suffix occurrence : this happens when there are several
            // occurrences of the pair, but they are all preceded by the same symbol
        }

        if(g.tree.parent(node)==lpg::root) continue;// parent is the root, i.e, it is an lms node
        node = g.tree.p_sibling(node);
        if(node==0) continue; // left-most pt_child of its parent, i.e, it is an lms node

        ind_bucket = alph.lexrank(g.label(node));
        if(ind_bucket==0) continue; // the prev sibling is not in the alphabet, i.e, it is an lms node

        if(ind_bucket < curr_bucket ||
           (ind_bucket == curr_bucket && i > next_av_pos)){
            if(g.tree.is_lm_child(node)){
                node = g.tree.parent(node);
                table.push(ind_bucket, g.tree.node_map(node));
            }else{
                table.push(ind_bucket, map-1);
            }
        }
    }
}

void ind_data::compute_next_alphabet(const lpg &g) {
    size_t pos=0, tmp_node;
    for(size_t i=0;i<alph.alph.size();i++){
        if(alph.alph[i]) {
            if(sym_status[i]!=5U){
                alph.alph[i] = false;
            }
            pos++;
        }

        if(sym_status[i] ==1U || sym_status[i]==3U || sym_status[i]==7U){
            alph.alph[i] = true;
        }
    }
    sdsl::util::init_support(alph.alph_rs, &alph.alph);

    size_t sym_rank = 1;
    size_t next_alph_size = alph.alph_rs(alph.alph.size());

    alph.ranks.width(sdsl::bits::hi(next_alph_size)+1);
    alph.ranks.resize(next_alph_size);

    size_t curr_bucket=0;
    long next_bucket_start = table.bck_limit(curr_bucket + 1);
    for (long i = 0; i < table.size(); i++) {
        if(i == next_bucket_start){
            curr_bucket++;
            next_bucket_start = table.bck_limit(curr_bucket + 1);
        }

        assert(table.sa[i]!=0);
        tmp_node = g.tree.node_select(table.sa[i]);
        pos = g.label(tmp_node);

        if(sym_status[pos]==5U){
            alph.ranks[alph.alph_rs(pos)] = sym_rank++;
            while(i!=(next_bucket_start-1)) i++;
        }else if (!g.tree.is_leaf(tmp_node)) {
            //pos = g.tree.int_rank(tmp_node) + g.sigma;
            if (alph.alph[pos]) {
                alph.ranks[alph.alph_rs(pos)] = sym_rank++;
            }
        }
    }
    //mask the nodes involved in this induction
    for (auto && status : sym_status) {
        if (status!=0) status = 4U;// 100 means masked
    }
}

size_t ind_data::compute_ind_table(const lpg &g, sdsl::cache_config& config) {

    auto end = g.g_end();
    bool lm_node, prev_in_alph=false;
    size_t n_sibs, f_child, f_child_lab, parent, bucket;
    uint8_t parent_status;
    bool rm_node;
    size_t nodes_width = sdsl::bits::hi(g.tree.nodes())+1;
    size_t alph_width = sdsl::bits::hi(alph.size())+1;

    buffer_t rm_nodes(sdsl::cache_file_name("rm_nodes", config),std::ios::out, 1024*1024*2, nodes_width);
    buffer_t bck_rm_nodes(sdsl::cache_file_name("bck_rm_nodes", config), std::ios::out, 1024*1024*2, alph_width);
    buffer_t sp_buff(sdsl::cache_file_name("suffix_pointers", config), std::ios::out, 1024 * 1024 * 2, nodes_width);

    vector_t bck_counts(alph.size() + 2, 0, nodes_width);
    size_t max_node_map=0;

    auto it = g.g_begin();
    parent_status=4U;
    n_sibs=1;
    while (it != end) {

        lm_node = n_sibs == 0;//left-most pt_child of its parent
        rm_node = n_sibs == 1;//right-most pt_child of its parent

        if (lm_node) {
            parent = g.tree.parent(it.id);
            n_sibs = g.tree.n_children(parent);
            parent_status = sym_status[g.label(parent)];
        }

        if (parent_status == 4U || parent_status==5U) goto finish_loop;

        if (alph.inalph(it.label)) { //label of the node in alphabet, (it will be induced)

            if (parent_status) {
                max_node_map = it.map;
                bucket = alph.lexrank(it.label); //alph.ranks[alph.alph_rs(it.label)];
                bck_counts[bucket]++;
                if(rm_node){
                    rm_nodes.push_back(it.map);
                    bck_rm_nodes.push_back(bucket);
                }
                //symbol is in the alphabet and its parent it is induced:
                //I use this information during the BWT construction
                //alph.t_symbols[sym_pos] = (alph.t_symbols[sym_pos] | 2U);
            } else {
                //symbol is in the alphabet but its parent it is not induced:
                //the symbol has to be transferred to the next iteration
                //alph.t_symbols[sym_pos] = (alph.t_symbols[sym_pos] | 1U);
                sym_status[it.label] = 5U;
            }
            prev_in_alph = true;

        } else {

            if (it.children != 0) {//it is unknown yet if the int node has to be induced

                if (prev_in_alph && !lm_node && parent_status) {
                    assert(sym_status[it.label] == 0);
                    sym_status[it.label] = 2U; //010 //SP occ
                    max_node_map = it.map;
                }else{
                    f_child = g.tree.child(it.id, 1);
                    f_child_lab = g.label(f_child);

                    if (alph.inalph(f_child_lab)) {//node will be induced!
                        assert(sym_status[it.label] == 0);
                        sym_status[it.label] = 1U; //001 //lms occ
                        max_node_map = it.map;
                    }
                }
            } else if (sym_status[it.label]) {//we already known if the node is induced
                if (prev_in_alph && !lm_node && parent_status) {//suffix occ
                    sym_status[it.label] = sym_status[it.label] | 6U; // 11x
                    sp_buff.push_back(it.label);
                    sp_buff.push_back(it.map-g.tree.node_map(g.tree.int_select(it.label-g.sigma)));
                } else {//lms occ
                    sym_status[it.label] = sym_status[it.label] | 1U; // 001
                }
            }
            prev_in_alph = false;
        }
        finish_loop:
        ++it;
        n_sibs--;
    }

    //create the sp pointers
    create_sp_pointers(sp_buff);

    //create the induction table
    create_table(max_node_map, bck_counts, rm_nodes, bck_rm_nodes);

    //compute new lmsg_nonterminals,
    // tr and multilevel symbols
    size_t lms_nodes=0;
    for(auto const  elm : sym_status){
        if((elm & 1U) && elm!=5U) lms_nodes++;
    }
    //TODO: can terminal nodes be transferred? :
    // what if there is one symbol
    // (N for instance in case of FASTQ?)
    size_t tr_nodes=0, k=0;
    for(size_t i=0;i<alph.alph.size();i++){
        if(alph.alph[i]){
            if(sym_status[i]==5U){
                tr_nodes++;
            }
            k++;
        }
    }

    std::cout<<"    Ind. table size:                "<<table.size()<<std::endl;
    std::cout<<"    Lex. buckets:                   "<<table.tot_buckets()<<std::endl;
    std::cout<<"    Nonterminals with sec. SP occ.: "<<suffix_pts.tot_lists()<<std::endl;
    std::cout<<"    LMSg nonterminals:              "<<lms_nodes<<std::endl;
    std::cout<<"    Transferred symbols:            "<<tr_nodes << std::endl;

    return lms_nodes;
}

void ind_data::insert_tr_to_table(const lpg &g) {
    //insert the transferred symbols to the induction table
    // as their positions after are 0 and I need them for the
    // computation of the new alphabet
    size_t pos=0, tmp_node;
    for(size_t i=0;i<alph.alph.size();i++){
        if(alph.alph[i]){
            if(sym_status[i]==5U && table.empty_bucket(alph.lexrank(i))) {
                tmp_node = g.tree.node_map(g.tree.int_select(i - g.sigma));
                table.push(alph.ranks[pos], tmp_node);
            }
            pos++;
        }
    }
}
*/


