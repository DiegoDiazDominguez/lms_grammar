#include <chrono>
#include <thread>

#include "utils/CLI11.hpp"
#include "lpg/lpg.hpp"


struct arguments{
    //common arguments
    std::string input_file;
    std::string output_file;
    std::string tmp_dir;
    size_t n_threads{};
    bool bwt=false;
    bool comp=false;
    bool decomp=false;
    char sep_symbol='\n';
    float hbuff_frac=0.5;

    bool keep=false;
    bool verbose=false;
    bool ver=false;
    uint8_t c_level=2;
    uint8_t bwt_of=0;
};

class MyFormatter : public CLI::Formatter {
public:
    MyFormatter() : Formatter() {}
    std::string make_option_opts(const CLI::Option *) const override { return ""; }
};


static void parse_app(CLI::App& app, struct arguments& args){
    auto fmt = std::make_shared<MyFormatter>();

    fmt->column_width(23);
    app.formatter(fmt);
    int max_thread = (int)std::thread::hardware_concurrency();

    app.add_option("FILE",
                   args.input_file,
                   "Input file")->
                     check(CLI::ExistingFile)->required();

    auto *comp = app.add_flag("-c,--compress",
                 args.comp, "Compress the input file");
    auto *decom = app.add_flag("-d,--decompress",
                               args.decomp, "Decompress the input file")->excludes(comp);
    auto *bwt = app.add_flag("-b,--bwt",
                             args.bwt, "Build the eBWT of the input file")->excludes(decom)->excludes(comp);

    app.add_option("-o,--output-file",
                   args.output_file,
                   "Output file")->type_name("");

    app.add_flag("-k,--keep", args.keep, "Keep input file")->excludes(bwt);

    app.add_option("-s,--separator",
                   args.sep_symbol, "Separator symbol for string collections (def. '\\n')")->
                   excludes(decom)->
                   excludes(bwt);

    app.add_option("-l,--level",
                   args.c_level, "Compression level (def. [1-3]=2)")->
                     check(CLI::Range(1,3))->
                     default_val(2)->
                     excludes(bwt)->
                     excludes(decom);

    app.add_option("-t,--threads",
                   args.n_threads,
                   "Maximum number of threads (def. [1-n_cores]=1)")->
                   check(CLI::Range(1, max_thread))->
                   default_val(1);

    app.add_option("-f,--hashing-buffer",
                   args.hbuff_frac,
                   "Hashing step will use at most INPUT_SIZE*f bytes. O means no limit (def. 0.5)")->
                   check(CLI::Range(0.0,1.0))->
                   default_val(0.5)->
                   excludes(decom)->
                   excludes(bwt);

    app.add_option("-w,--bwt-format",
                   args.bwt_of,
                   "Format of the resulting eBWT (def. [0-2]=0)")->
                   check(CLI::Range(0,2))->
                   default_val(0)->
                   excludes(comp)->
                   excludes(decom);

    app.add_option("-T,--tmp",
                   args.tmp_dir,
                   "Temporal folder (def. /tmp/lpg.xxxx)")->
                     check(CLI::ExistingDirectory)->
                     default_val("/tmp");

    app.add_flag("-v,--verbose",
                 args.verbose, "Print extra information");
    app.add_flag("-V,--version",
                 args.ver, "Print the software version and exit");

    app.footer("By default, lpg will compress FILE if -c,-d or -b are not set\n\nReport bugs to <diediaz@dcc.uchile.cl>");
}

int main(int argc, char** argv) {

    arguments args;

    CLI::App app("A string compressor based on LMS induction");
    parse_app(app, args);

    CLI11_PARSE(app, argc, argv);

    if(!args.comp && !args.decomp && !args.bwt) args.comp = true;

    if(args.comp) {
        lpg<huff_vector<>> lms_g(args.input_file, args.tmp_dir, args.sep_symbol,
                                 args.n_threads, args.c_level, args.hbuff_frac);

        if(args.output_file.empty()){
            args.output_file = args.input_file;
        }

        args.output_file = args.output_file+".lg";

        sdsl::store_to_file(lms_g, args.output_file);

        if(!args.keep){
            if(remove(args.input_file.c_str())){
                std::cout<<"There was an error trying to remove input file"<<std::endl;
            }
        }
    }else if(args.decomp){

        //TODO I have to include some mechanism to check if the file is ok
        std::cout<<"Loading the grammar"<<std::endl;
        lpg<huff_vector<>> g;
        sdsl::load_from_file(g, args.input_file);
        std::cout<<"  Terminals:                "<<(size_t)g.sigma<<std::endl;
        std::cout<<"  Nonterminals:             "<<g.tree.int_nodes()<<std::endl;
        std::cout<<"  Size of the comp. string: "<<g.tree.n_children(lpg<huff_vector<>>::root)<<std::endl;
        std::cout<<"  Grammar size:             "<<g.tree.nodes()<<std::endl;

        if(args.output_file.empty()){
            args.output_file = args.input_file.substr(0, args.input_file.size()-3);
        }

        g.decompress_text(args.tmp_dir, args.output_file, args.n_threads);

        if(!args.keep){
            if(remove(args.input_file.c_str())){
                std::cout<<"There was an error trying to remove input file"<<std::endl;
            }
        }
    }else{

        if(args.output_file.empty()){
            args.output_file = args.input_file.substr(0, args.input_file.size()-3)+".bwt";
        }else{
            if(!(args.output_file.size() >= 4 &&
                 args.output_file.compare(args.output_file.size() - 4, 4, ".bwt"))==0){
                args.output_file+=".bwt";
            };
        }

        std::cout<<"Loading the grammar"<<std::endl;
        lpg<sdsl::int_vector<>> g;
        sdsl::load_from_file(g, args.input_file);
        std::cout<<"  Terminals:                "<<(size_t)g.sigma<<std::endl;
        std::cout<<"  Nonterminals:             "<<g.tree.int_nodes()<<std::endl;
        std::cout<<"  Size of the comp. string: "<<g.tree.n_children(lpg<huff_vector<>>::root)<<std::endl;
        std::cout<<"  Grammar size:             "<<g.tree.nodes()<<std::endl;
        g.compute_bwt(args.output_file, args.bwt_of, args.n_threads, args.tmp_dir);

        /*sdsl::int_vector<> nodes;
        sdsl::load_from_file(nodes,"/home/ddiaz/lms_grammar_remote/cmake-build-debug-remote-host/plain_bwt");
        sdsl::bit_vector b_limits;
        sdsl::load_from_file(b_limits, "/home/ddiaz/lms_grammar_remote/cmake-build-debug-remote-host/b_limits");
        sdsl::bit_vector::select_1_type b_limits_ss(&b_limits);
        sdsl::cache_config config;

        std::cout<<"Building RLBWT"<<std::endl;
        std::string file = "/home/ddiaz/lms_grammar_remote/cmake-build-debug-remote-host/plain_bwt";
        rlfm_index fm(file, b_limits_ss, 2533, 23150900, config);

        std::cout<<"Testing run buckets"<<std::endl;
        size_t sym = nodes[0], freq=1, freq2=1, run=0, bk=0;
        std::vector<std::tuple<size_t, size_t, size_t>> res;
        for(size_t j=1;j<nodes.size();j++){
            if(nodes[j]!=sym){

                res.emplace_back(sym, freq2, bk);
                freq2=1;
                if(b_limits[j]) bk++;
                auto res2 = fm.run_buckets(run);
                if(res2.size()!=res.size()){
                    fm.run_buckets(run);
                    for(size_t i=0;i<res2.size();i++){
                        std::cout<<std::get<0>(res2[i])<<" "<<std::get<1>(res2[i])<<" "<<std::get<2>(res2[i])<<std::endl;
                    }
                    std::cout<<"mio: "<<res2.size()<<" otro: "<<res.size()<<std::endl;
                    for(size_t i=0;i<res.size();i++){
                        std::cout<<std::get<0>(res[i])<<" "<<std::get<1>(res[i])<<" "<<std::get<2>(res[i])<<std::endl;
                    }
                }
                assert(res2.size()==res.size());
                for(size_t i=0;i<res2.size();i++){
                    if(std::get<0>(res2[i])!=std::get<0>(res[i]) ||
                       std::get<1>(res2[i])!=std::get<1>(res[i]) ||
                       std::get<2>(res2[i])!=std::get<2>(res[i])){
                        std::cout<<std::get<0>(res2[i])<<" "<<std::get<0>(res[i])<<std::endl;
                        std::cout<<std::get<1>(res2[i])<<" "<<std::get<1>(res[i])<<std::endl;
                        std::cout<<std::get<2>(res2[i])<<" "<<std::get<2>(res[i])<<std::endl;
                        exit(1);
                    }
                    assert(std::get<0>(res2[i])==std::get<0>(res[i]));
                    assert(std::get<1>(res2[i])==std::get<1>(res[i]));
                    assert(std::get<2>(res2[i])==std::get<2>(res[i]));
                }
                sym = nodes[j];
                freq=0;
                run++;
                res.clear();
            }else{
                if(b_limits[j]){
                    res.emplace_back(sym, freq2, bk);
                    bk++;
                    freq2=1;
                }else{
                    freq2++;
                }
            }
            freq++;
        }*/
        /*sdsl::wt_int<> wt_tmp;
        sdsl::load_from_file(wt_tmp, "/home/ddiaz/lms_grammar_remote/cmake-build-debug-remote-host/regular_bwt");
        for(size_t i=0;i<wt_tmp.size();i++){
            auto is = wt_tmp.inverse_select(i);
            auto is2 = fm.inv_select(i);
            assert(is.first==is2.first && is.second==is2.second);
        }*/
    }

    GET_MEM_PEAK();
    return 0;
}