//
// Created by Diego Diaz on 12/18/20.
//
#include <iostream>
#include <sdsl/config.hpp>
#include "cdt/huff_vector.hpp"
#include "cdt/macros.h"


int main(int argc, char** argv){
    sdsl::int_vector_buffer<64> arr("/home/ddiaz/lpg.Q4TIkZ/grammar_pointers_18511_0.sdsl", std::ios::in);

    sdsl::cache_config config(false, "./tmp_folder");
    huff_vector<> c_arr(arr, config);
    std::cout<<"Input file contains "<<c_arr.size()<<" elements"<<std::endl;
    auto it = c_arr.begin();
    auto it_end = c_arr.end();
    size_t i=0;
    while(it!=it_end){
        //std::cout<<*it<<" ("<<arr[i]<<","<<arr[i+1]<<")"<<std::endl;
        assert(*it==arr[i]);
        ++it;
        ++i;
    }
}