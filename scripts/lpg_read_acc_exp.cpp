//
// Created by Diego Diaz on 11/8/20.
//
#include "lpg/lpg.hpp"
#include "sdsl/bit_vectors.hpp"

typedef lpg<sdsl::int_vector<>> grammar_t;

//suffix node: a node whose last terminal symbol is a separator symbol
sdsl::bit_vector compute_suffixes(const grammar_t &g){

    size_t tmp_node, label, int_rank;
    std::ptrdiff_t root_children = g.tree.n_children(grammar_t::root);

    //!note: we need to do this to make circular references of the strings in the BWT
    //0: node has not been visited
    //1: node is not suffix
    //2: node is suffix
    sdsl::int_vector<2> sep_nt(g.tree.int_nodes()+1, 0);
    sdsl::bit_vector suffix_nodes(root_children+2, false);

    bool continue_trav, is_suffix;
    std::stack<size_t> stack;

    size_t n_children = g.tree.n_children(grammar_t::root);

    for(size_t i=2;i<=n_children+1;i++){

        tmp_node = g.tree.node_select(i);

        if(!g.tree.is_leaf(tmp_node)){

            //TODO: can we have a terminal as a child of the root?
            continue_trav = g.label(tmp_node) > g.sigma;

            while(continue_trav){
                stack.push(tmp_node);

                //get the right-most pt_child
                tmp_node = g.tree.child(tmp_node, g.tree.n_children(tmp_node));

                if(g.tree.is_leaf(tmp_node)){
                    label = g.label(tmp_node);
                    if(label<=g.sigma){//it points to a terminal symbol
                        is_suffix = (label==0); //is the pointed terminal a sep symbol?
                        while(!stack.empty()){ //mark all the ancestors
                            tmp_node = stack.top();
                            stack.pop();
                            int_rank = g.tree.int_rank(tmp_node);
                            sep_nt[int_rank] = (is_suffix + 1);
                        }
                        continue_trav = false;
                        suffix_nodes[i] = is_suffix;
                    }else{
                        int_rank = label-g.sigma;
                        tmp_node = g.tree.int_select(int_rank);
                        if(sep_nt[int_rank]){
                            is_suffix = (sep_nt[int_rank] == 2);
                            while(!stack.empty()){
                                tmp_node = stack.top();
                                stack.pop();
                                int_rank = g.tree.int_rank(tmp_node);
                                sep_nt[int_rank] = is_suffix + 1;
                            }
                            continue_trav = false;
                            suffix_nodes[i] = is_suffix;
                        }
                    }
                }
            }
        }else{
            label = g.label(tmp_node);
            suffix_nodes[i] = (label==0) ||
                              (label>g.sigma && sep_nt[label-g.sigma]==2);
        }
    }
    return suffix_nodes;
}

int main(int argc, char* argv[]) {

    if(argc!=3){
        std::cout<<"Usage: ./lpg_read_acc_exp input.lpg string_len\n";
        exit(0);
    }

    char *p;
    size_t string_len = strtol(argv[2], &p, 10);
    std::string tmp = std::string(argv[1]);

    std::cout<<"Input file: "<<tmp<<std::endl;
    std::cout<<"String length: "<<string_len<<std::endl;

    std::ifstream in_file(tmp, std::ios::binary);
    in_file.seekg(0, std::ios::end);
    double file_size = in_file.tellg()/(1024.0*1024.0);
    in_file.close();

    std::cout<<"Loading the grammar"<<std::endl;
    lpg<sdsl::int_vector<>> g;
    sdsl::load_from_file(g, tmp);

    std::cout<<"Computing the suffix nodes in the grammar"<<std::endl;
    sdsl::bit_vector suffx_nodes = compute_suffixes(g);
    size_t n_reads=0;
    for(size_t i=2;i<suffx_nodes.size();i++){
        if(suffx_nodes[i]){
            n_reads++;
        }
    }

    sdsl::bit_vector::select_1_type strings_ss(&suffx_nodes);
    double mc=0;
    size_t pos, s, e;
    std::vector<uint8_t> dc_string;
    dc_string.reserve(200);

    std::cout<<"Measuring random accession"<<std::endl;
    for(size_t i=0;i<5000;i++){
        pos = (rand() % n_reads) + 1;
        e = strings_ss(pos);
        if(pos>1){
            s = strings_ss(pos-1)+1;
        }else{
            s=1;
        }
        dc_string.resize(0);

        auto start = std::chrono::high_resolution_clock::now();
        for(size_t j=s;j<=e;j++){
            g.decompress_node<std::vector<uint8_t>>(dc_string, g.tree.node_select(j));
        }

        auto end = std::chrono::high_resolution_clock::now();
        std::chrono::duration<double, std::micro> elapsed = end-start;
        mc+=elapsed.count();
    }

    //double tree_overhead = sdsl::size_in_mega_bytes(g.tree) - ((double(g.tree.shape_size())/8)/1000000);
    //double pointer_overhead = sdsl::size_in_mega_bytes(g.l_labels.sampling);
    //double pointer_overhead = sdsl::size_in_mega_bytes(g.l_labels);
    //double bv_overhead = sdsl::size_in_mega_bytes(suffx_nodes)+sdsl::size_in_mega_bytes(strings_ss);

    std::cout<<"Length of the strings: "<<string_len<<std::endl;
    std::cout<<"Average time for decompressing a random string: "<< (mc)/5000 <<" microseconds"<<std::endl;
    std::cout<<"Average time for decompressing a symbol: "<< (mc)/(5000*string_len) <<" microseconds"<<std::endl;
    //std::cout<<"Space overhead :"<<tree_overhead+pointer_overhead+bv_overhead<<" MB "<<std::endl;
    std::cout<<"Original grammar size: "<<file_size<<" MiB "<<std::endl;
    std::cout<<"Resulting grammar size: "<<sdsl::size_in_mega_bytes(g)+sdsl::size_in_mega_bytes(suffx_nodes)+sdsl::size_in_mega_bytes(strings_ss)<<" MiB "<<std::endl;
    std::cout<<"Space overhead : "<<sdsl::size_in_mega_bytes(g)-file_size<<std::endl;
    std::cout<<"Done!"<<std::endl;
}