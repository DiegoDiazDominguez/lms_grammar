//
// Created by diegodiaz on 29-11-20.
//
#define BUFFER_SIZE (1024*1024)
#include "iostream"
#include "../include/cdt/rlfm_index.h"

void run_len_compress_bwt(char * input_file, std::string& rlfmi_file, sdsl::int_vector<8>& map){

    FILE *f = fopen(input_file, "rb");

    fseek(f, 0, SEEK_END);
    long fsize = ftell(f);
    fseek(f, 0, SEEK_SET);

    auto string = (char *)malloc(fsize+1);
    fread(string,1,fsize,f);
    string[fsize] = 0;
    fclose(f);

    size_t C[255]={0};
    for(size_t i=0;i<fsize;i++) C[string[i]]++;
    size_t inv_map[255]={0};
    size_t alph=0;

    std::vector<char> map_tmp;
    for(size_t i=0;i<255;i++){
        if(C[i]!=0){
            inv_map[i]=alph;
            map_tmp.push_back(i);
            alph++;
        }
    }

    map.resize(map_tmp.size());
    for(size_t i=0;i<map_tmp.size();i++){
        map[i] = map_tmp[i];
    }
    std::string alph_file = std::string(input_file)+"_alph";
    sdsl::store_to_file(map, alph_file);

    std::string r_heads_file = std::string(input_file)+"_run_heads";
    std::string r_len_file = std::string(input_file)+"_run_len";
    std::string bck_acc_file = std::string(input_file)+"_sym_acc";
    sdsl::int_vector_buffer<> run_heads(r_heads_file, std::ios::out);
    sdsl::int_vector_buffer<> run_len(r_len_file, std::ios::out);
    sdsl::int_vector_buffer<> sym_acc(bck_acc_file, std::ios::out);


    char head = string[0];
    size_t len=1;

    for(size_t i=1;i<fsize;i++){
        if(string[i]!=string[i-1]){
            run_heads.push_back(inv_map[head]);
            run_len.push_back(len);

            head = string[i];
            len=0;
        }
        len++;
    }
    run_heads.push_back(inv_map[head]);
    run_len.push_back(len);

    free(string);

    sym_acc.push_back(0);
    size_t acc=0;
    for(unsigned long i : C){
        acc+=i;
        if(i!=0) sym_acc.push_back(acc);
    }

    run_heads.close();
    run_len.close();
    sym_acc.close();
    rlfm_index<sdsl::wt_int<>> rlfmi(r_heads_file, r_len_file, bck_acc_file);
    sdsl::store_to_file(rlfmi, rlfmi_file);

    remove(r_heads_file.c_str());
    remove(r_len_file.c_str());
    remove(bck_acc_file.c_str());
}

int main(int argc, char** argv){

    if(!(argc==4 || argc==5)){
        std::cout<<"Usage: ./test_eBWT file is_indexed n_seqs alph_file\n"
                   "file is the eBWT file\nis_indexed is a bool: 0 = file is a RLFM-index, 1= file is plain a eBWT and an index has to be build\n"
                   "n_seqs is the number of sequences to be decompressed"<<std::endl;
        exit(0);
    }


    errno = 0;
    char *endptr;
    long option = strtol(argv[2], &endptr, 10);
    long n_seqs = strtol(argv[3], &endptr, 10);
    std::cout<<n_seqs<<" asked to decompress"<<std::endl;

    sdsl::int_vector<8> sym_map;
    rlfm_index<sdsl::wt_int<>> rlfmi;

    if(option==0){
        sdsl::load_from_file(rlfmi, std::string(argv[1]));
        sdsl::load_from_file(sym_map, std::string(argv[4]));
    }else if(option==1){
        std::cout<<"Building the FM-index"<<std::endl;
        std::string rlfmi_file = std::string(argv[1])+"_rlfmi";
        run_len_compress_bwt(argv[1], rlfmi_file, sym_map);
        std::cout<<"Loading the FM-index"<<std::endl;
        sdsl::load_from_file(rlfmi, rlfmi_file);
    }else{
        std::cout<<"Unknown option"<<std::endl;
    }

    size_t sym, next_pos;
    std::string seq;
    size_t n_dummy_runs = rlfmi.C[1] - rlfmi.C[0];
    size_t n_dummy = rlfmi.bwt_run_sums_ss.select(n_dummy_runs+1);
    n_dummy = std::min<size_t>(n_dummy, (size_t)n_seqs);

    std::cout<<"Decompressing the first "<<n_dummy<<" sequences "<<std::endl;

    for(size_t i=0;i<n_dummy;i++){
        seq.clear();
        auto res = rlfmi.lf(i);
        next_pos = res.first;
        sym = res.second;
        while(sym!=0){
            seq.push_back(sym_map[sym]);
            res = rlfmi.lf(next_pos);
            next_pos = res.first;
            sym = res.second;
        }
        std::reverse(seq.begin(), seq.end());
        std::cout<<seq<<std::endl;
    }
}

