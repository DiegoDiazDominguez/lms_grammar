//
// Created by diegodiaz on 14-12-20.
//

#include <iostream>
#include "cdt/bitstream.h"
#include "cdt/file_streams.hpp"
#include "sdsl/io.hpp"

int main(int argc, char** argv) {

    std::string file = "some_file";
    size_t n_vals = 100000;

    std::random_device rd;  //Will be used to obtain a seed for the random number engine
    std::mt19937 gen(rd()); //Standard mersenne_twister_engine seeded with rd()
    std::uniform_int_distribution<size_t> distrib(1, std::numeric_limits<size_t>::max());

    for(size_t k=0;k<100;k++){
        size_t max_sym1, max_sym2, sym1, sym2;
        size_t width1 = 1 + (rand()%64);
        size_t width2 = 1 + (rand()%64);
        std::vector<size_t> syms1_list;
        std::vector<size_t> syms2_list;

        bitstream<size_t> stream;
        size_t tot_bytes = INT_CEIL((width1+width2)*n_vals, 8);
        stream.stream_size = INT_CEIL(tot_bytes, sizeof(size_t));
        stream.stream = new size_t[stream.stream_size];

        bitstream<size_t> stream2;
        stream2.stream_size = 2;
        stream2.stream = new size_t[2];

        max_sym1 = (1ULL<<width1)-1;
        max_sym2 = (1ULL<<width2)-1;
        for(size_t i=0;i<n_vals;i++){

            sym1 = distrib(gen)%max_sym1;
            sym2 = distrib(gen)%max_sym2;
            syms1_list.push_back(sym1);
            syms2_list.push_back(sym2);

            stream2.write(0, width1-1, sym1);
            stream2.write(width1, width1+width2-1, sym2);
            size_t start = i*(width1+width2);
            stream.write_chunk(stream2.stream, start, start+width1+width2-1);
        }
        std::ofstream ofs(file, std::ios::out | std::ios::binary);
        ofs.write((char *)stream.stream, stream.stream_size*sizeof(size_t));
        ofs.close();

        i_file_stream<size_t> ifs(file, 512);
        bitstream<size_t> stream3;
        stream3.stream_size = 2;
        stream3.stream = new size_t[2];
        for(size_t i=0;i<n_vals;i++){
            size_t j= rand() % n_vals;
            size_t start = j*(width1+width2);
            ifs.read_chunk(stream3.stream, start, start+width1+width2-1);
            //std::cout<<j<<" -> "<<stream3.read(0, width1-1)<<" "<<stream3.read(width1, width1+width2-1)<<" "<<syms1_list[j]<<" "<<syms2_list[j]<<std::endl;
            assert(stream3.read(0, width1-1)==syms1_list[j]);
            assert(stream3.read(width1, width1+width2-1)==syms2_list[j]);
        }

        delete [] stream.stream;
        delete [] stream2.stream;
        delete [] stream3.stream;
    }

    if(remove(file.c_str())){
        std::cout<<"Error trying to remove file: "<<file<<std::endl;
    }
}

