#include <sdsl/suffix_arrays.hpp>
#include <sdsl/wavelet_trees.hpp>
#include <fstream>

using namespace sdsl;
using namespace std;

int main(int argc, char** argv) {

    wt_huff<> wt;
    construct(wt, argv[1], 1);

    std::vector<sdsl::wt_huff<>::value_type> symbols(wt.sigma);
    std::vector<sdsl::wt_huff<>::size_type> ranki(wt.sigma);
    std::vector<sdsl::wt_huff<>::size_type> rankj(wt.sigma);
    sdsl::wt_huff<>::size_type k = wt.sigma;
    wt.interval_symbols(0, wt.size(), k, symbols, ranki, rankj);
    std::vector<size_t> C(wt.sigma,0);
    uint8_t inv_map[255]={0};
    for(size_t i=0;i<k;i++){
	inv_map[symbols[i]] = i;
	C[i] = rankj[i];
    }
    size_t tmp=C[0], tmp2;
    C[0] = 0;
    for(size_t i=1;i<wt.sigma;i++){
	tmp2 = C[i];
	C[i] = tmp;
	tmp+=tmp2;
    }

    size_t start_pos, next_pos;
    char sym=0;
    for(size_t i=0;i<10;i++){
	start_pos = i; 
	next_pos = start_pos;
	while(sym!='\n'){
	    auto pair = wt.inverse_select(next_pos);
	    sym = pair.second;
	    std::cout<<sym<<" ";
	    next_pos = C[inv_map[pair.second]] + pair.first;
	}
	std::cout<<" "<<std::endl;
    }

}
