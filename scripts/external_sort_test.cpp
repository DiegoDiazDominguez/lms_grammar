//
// Created by diego on 1/22/21.
//
#include<iostream>
#include <chrono>
#include <vector>
#include <sdsl/int_vector_buffer.hpp>
#include <cdt/hash_table.hpp>

struct node_data{
    uint8_t last_bwt_sym:4;
    size_t pos:60;
};

int main(int argc, char** argv){

    std::vector<std::string> files = { "/home/diego/data/rc_colapse_test/bwt_trip_7696_1.sdsl",
                                       "/home/diego/data/rc_colapse_test/rc_q1_0_36158116",
                                       "/home/diego/data/rc_colapse_test/rc_q1_36158117_72316233",
                                       "/home/diego/data/rc_colapse_test/rc_q1_72316234_108474350",
                                       "/home/diego/data/rc_colapse_test/rc_q1_108474351_144632467"};
    uint32_t node, bwt_sym, fr;
    void * buffer = malloc((1024*1024)/2);
    auto start = std::chrono::steady_clock::now();
    node_data data{};
    for (size_t i=1;i<files.size();i++) {
        sdsl::int_vector_buffer<> q1(files[i], std::ios::in);
        sdsl::int_vector_buffer<32> c_q1("c_q1", std::ios::out);
        bit_hash_table<node_data, 64> ht((1024*1024)/2, "", 0.8, buffer);
        std::cout<<q1.size()<<std::endl;
        size_t block_a, block_b, block_size= 262144;

        auto it = q1.begin();
        auto it_end = q1.end();
        while(it!=it_end) {
            fr = *it;
            ++it;
            bwt_sym = *it;
            ++it;
            node = *it;
            ++it;

            data.pos = c_q1.size()-1;
            data.last_bwt_sym = bwt_sym;
            auto res = ht.insert(&node, 32, data);
            if(res.second){
                c_q1.push_back(fr);
                c_q1.push_back(bwt_sym);
                c_q1.push_back(node);
            }else{
                ht.get_value_from(*res.first, data);
                block_a = (c_q1.size()-1)/block_size;
                block_b = (data.pos)/block_size;
                if(data.last_bwt_sym==bwt_sym && block_a==block_b){
                    c_q1[data.pos-2] += fr;
                }else{
                    c_q1.push_back(fr);
                    c_q1.push_back(bwt_sym);
                    c_q1.push_back(node);
                    data.pos = c_q1.size()-1;
                    data.last_bwt_sym = bwt_sym;
                    ht.insert_value_at(*res.first, data);
                }
            }
        }
        std::cout<<c_q1.size()<<" "<<q1.size()<<std::endl;
    }
    auto end = std::chrono::steady_clock::now();
    std::cout << "Elapsed time in seconds : " << std::chrono::duration_cast<std::chrono::seconds>(end - start).count() << " sec"<<std::endl;
    free(buffer);

    /*auto start = std::chrono::steady_clock::now();
    std::string file = "/home/diego/data/simple_q1";
    std::ifstream ifs(file, std::ios::in | std::ios::binary);
    ifs.seekg(0,std::ios_base::end);
    size_t size = ifs.tellg();
    ifs.seekg(0,std::ios_base::beg);
    size_t n_elms = size/sizeof(size_t);

    size_t buff = 20*131072;
    auto buffer = reinterpret_cast<size_t *>(malloc(buff*sizeof(size_t)));
    ifs.read((char *)buffer, buff*sizeof(size_t));

    size_t pos=0;
    for(size_t i=0;i<n_elms;i++){
        pos++;
        if((i % buff)==0){
            ifs.read((char *)buffer, buff*sizeof(size_t));
            pos=0;
        }
    }
    auto end = std::chrono::steady_clock::now();
    std::cout << "Elapsed time in seconds : " << std::chrono::duration_cast<std::chrono::seconds>(end - start).count() << " sec"<<std::endl;
    free(buffer);*/
}