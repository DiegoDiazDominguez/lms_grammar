//
// Created by Diego Diaz on 11/6/20.
//
#include <iostream>
#include <sdsl/wt_huff.hpp>
#include <sdsl/bit_vectors.hpp>
#include <sdsl/construct.hpp>
#include <chrono>
#include <cerrno>
#include <cstdlib>

void FMI(std::string& input_bwt, double samp_frac, size_t read_len){

    std::cout<<" Input file        : "<<input_bwt<<std::endl;
    std::cout<<" Sampling fraction : "<<samp_frac<<std::endl;
    std::cout<<" String len        : "<<read_len<<std::endl;
    std::cout<<" Encoding          : regular FM-index"<<std::endl;

    std::chrono::duration<double> elap_wt{0};
    auto start_t = std::chrono::high_resolution_clock::now();
    sdsl::wt_huff<> wt;
    sdsl::construct(wt, input_bwt, 1);
    auto end_t = std::chrono::high_resolution_clock::now();
    elap_wt = end_t-start_t;
    std::cout<<"Seconds for building the BWT "<<elap_wt.count()<<std::endl;

    std::vector<sdsl::wt_huff<>::value_type> symbols(wt.sigma);
    std::vector<sdsl::wt_huff<>::size_type> ranki(wt.sigma);
    std::vector<sdsl::wt_huff<>::size_type> rankj(wt.sigma);
    sdsl::wt_huff<>::size_type k = wt.sigma;

    wt.interval_symbols(0, wt.size(), k, symbols, ranki, rankj);
    std::vector<size_t> C(wt.sigma,0);
    uint8_t inv_map[255]={0};

    for(size_t i=0;i<k;i++){
        inv_map[symbols[i]] = i;
        C[i] = rankj[i];
    }

    size_t tmp=C[0], tmp2;
    C[0] = 0;
    for(size_t i=1;i<wt.sigma;i++){
        tmp2 = C[i];
        C[i] = tmp;
        tmp+=tmp2;
    }

    size_t start_pos, next_pos;
    double mc = 0;
    std::chrono::duration<double, std::micro> elapsed{0};

    for(size_t i=0;i<5000;i++){
        start_pos = rand() % wt.size();
        next_pos = start_pos;
        start_t = std::chrono::high_resolution_clock::now();
        for(size_t j=0;j<read_len;j++){
            auto pair = wt.inverse_select(next_pos);
            next_pos = C[inv_map[pair.second]] + pair.first;
        }
        end_t = std::chrono::high_resolution_clock::now();
        elapsed = end_t-start_t;
        mc += elapsed.count();
    }

    std::cout<<read_len<<" "<<wt.size()<<" "<<std::endl;
    assert((wt.size() % read_len)==0);
    size_t tot_str = wt.size()/read_len;
    auto s_str = size_t(samp_frac*tot_str);

    std::cout<<"Average time for accessing a read "<<(mc/5000)<< " usecs"<<std::endl;
    std::cout<<"Space usage of the encoding :"<< (sdsl::size_in_mega_bytes(wt) + ((double(wt.sigma*64)/8)/1024.0*1024.0))/1024.0 <<" GiB"<<std::endl;
    std::cout<<s_str<<" out of "<<tot_str<<" were sampled"<<std::endl;
    std::cout<<"Space overhead of the pointers: "<<(double(s_str*(sdsl::bits::hi(wt.size())+1))/8)/(1024.0*1024.0)<<" MiB"<<std::endl;
}

void RL_FMI(std::string& input_file, double samp_frac, size_t read_len){

    std::cout<<" Input file        : "<<input_file<<std::endl;
    std::cout<<" Sampling fraction : "<<samp_frac<<std::endl;
    std::cout<<" String len        : "<<read_len<<std::endl;
    std::cout<<" Encoding          : RLFM-index"<<std::endl;

    std::ifstream text_i(input_file, std::ios_base::binary);
    text_i.seekg (0, std::ifstream::end);
    size_t tot_bytes = text_i.tellg();
    text_i.seekg (0, std::ifstream::beg);
    auto buffer = reinterpret_cast<unsigned char *>(malloc(tot_bytes));
    text_i.read((char *)buffer, tot_bytes);

    std::chrono::duration<double> elap_wt{0};
    auto start_t = std::chrono::high_resolution_clock::now();

    sdsl::bit_vector run_len(tot_bytes, false);
    sdsl::bit_vector sym_sums(tot_bytes, false);
    sdsl::int_vector_buffer<8> run_heads("bwt_heads", std::ios::out);
    run_len[0] = true;
    size_t freqs[255]={0};
    size_t sym_maps[255];
    std::vector<uint8_t> inv_map;

    size_t sigma=0;
    for(size_t i=0;i<tot_bytes;i++){
        if(freqs[buffer[i]]==0) sigma++;
        freqs[buffer[i]]++;
    }
    std::cout<<"Computing the symbol frequencies"<<std::endl;
    std::vector<size_t> C(sigma,0);
    size_t k=0;
    for(size_t i=0;i<255;i++){
        if(freqs[i]>0){
            sym_maps[i] = k;
            C[k]=freqs[i];
            k++;
        }
    }

    size_t tmp=C[0], tmp2;
    C[0] = 0;
    for(size_t i=1;i<sigma;i++){
        tmp2 = C[i];
        C[i] = tmp;
        tmp+=tmp2;
    }

    std::vector<size_t> acc=C;
    C={0,0,0,0,0,0};

    size_t len=1, rank;
    run_len[0] = true;
    size_t i=1;
    while(i<tot_bytes){
        if(buffer[i]!=buffer[i-1]){
            run_heads.push_back(buffer[i-1]);
            rank = sym_maps[buffer[i-1]];
            C[rank]++;
            sym_sums[acc[rank]]=true;
            acc[rank]+=len;
            run_len[i]=true;
            len=1;
        }else{
            len++;
        }
        i++;
    }
    run_heads.push_back(buffer[i-1]);
    rank = sym_maps[buffer[i-1]];
    sym_sums[acc[rank]]=true;
    C[rank]++;

    tmp=C[0];
    C[0] = 0;
    for(size_t j=1;j<sigma;j++){
        tmp2 = C[j];
        C[j] = tmp;
        tmp+=tmp2;
    }
    run_heads.push_back(buffer[tot_bytes-1]);
    free(buffer);
    run_heads.close();
    std::cout<<"Building the representation"<<std::endl;
    sdsl::wt_huff<> wt;
    sdsl::construct(wt,"bwt_heads", 0);
    sdsl::sd_vector<> rl_sd = run_len;
    sdsl::sd_vector<>::rank_1_type rl_sd_rs(&rl_sd);
    sdsl::sd_vector<>::select_1_type rl_sd_ss(&rl_sd);
    sdsl::sd_vector<> sym_sums_sd = sym_sums;
    sdsl::sd_vector<>::select_1_type sym_sums_ss(&sym_sums_sd);
    auto end_t = std::chrono::high_resolution_clock::now();
    elap_wt = end_t-start_t;
    std::cout<<"Seconds for building the BWT "<<elap_wt.count()<<std::endl;

    assert((tot_bytes % read_len)==0);

    size_t n_strings = tot_bytes/read_len;
    double sampling = samp_frac*n_strings;//frac*100 % of the reads

    sampling *=(sdsl::bits::hi(tot_bytes)+1);//multiply by \log n
    sampling /= (8.0*1024.0*1024.0); //in MiB

    double sm = sdsl::size_in_mega_bytes(wt);
    sm += sdsl::size_in_mega_bytes(rl_sd);
    sm += sdsl::size_in_mega_bytes(rl_sd_rs);
    sm += sdsl::size_in_mega_bytes(rl_sd_ss);
    sm += sdsl::size_in_mega_bytes(sym_sums_sd);
    sm += sdsl::size_in_mega_bytes(sym_sums_ss);

    std::cout<<"Space usage of the RLFM-Index: "<<sm<<" (MiB)"<<std::endl;
    std::cout<<"Space overhead of the sampled pointers: "<<sampling<<" (MiB) "<<std::endl;

    size_t pos;
    bool head;
    auto start = std::chrono::high_resolution_clock::now();
    auto end = std::chrono::high_resolution_clock::now();
    double mc=0;

    std::chrono::duration<double, std::micro> elapsed = end-start;
    size_t diff, head_pos;
    for(i=0;i<1000;i++){
        pos = rand() % tot_bytes;
        start = std::chrono::high_resolution_clock::now();
        head = rl_sd[pos];
        head_pos = rl_sd_rs(pos)-!head;
        diff=0;
        if(!head){
            diff = pos-rl_sd_ss(head_pos+1);
        }
        auto symbol = wt.inverse_select(head_pos);
        sym_sums_ss(C[sym_maps[symbol.second]] + symbol.first+1) + diff;
        end = std::chrono::high_resolution_clock::now();
        elapsed=end-start;
        mc+=elapsed.count();
    }
    double av_lf = mc/1000;
    std::cout<<"Average time for LF-mapping: "<<(mc/1000)<<std::endl;

    double av_acc=0;
    auto str_per_samp = size_t(1/samp_frac);
    for(size_t j=1;j<=str_per_samp;j++){
        av_acc+=(double(read_len)*j*av_lf);
    }

    std::cout<<"Average string decomp: "<<av_acc/str_per_samp<<" microseconds "<<std::endl;
    std::string tmp_file = "bwt_heads";
    remove(tmp_file.c_str());
}

int main(int argc, char* argv[]) {

    if(argc!=5){
        std::cout<<"\nBuild and test the FM-index\n"<<std::endl;
        std::cout<<"Usage: ./fmi_exp input_bwt enc string_len samp_frac\n"<<std::endl;
        std::cout<<"input_bwt  = input BWT"<<std::endl;
        std::cout<<"enc        = encoding of the FM-index (1 or 2), 1 for a RL-compressed FM-index and 2 for a regular FM-index"<<std::endl;
        std::cout<<"string_len = length of the collection strings (it assumes all the elements have the same length)"<<std::endl;
        std::cout<<"samp_frac  = this fraction of strings will be sampled from the collection for random access\n"<<std::endl;
        std::cout<<"Note: we are not considering the SA"<<std::endl;
        exit(0);
    }

    char *p;
    size_t num;
    errno = 0;
    long conv = strtol(argv[2], &p, 10);
    if (errno != 0 || *p != '\0' || conv > 2) {
        std::cout<<"Invalid argument"<<std::endl;
    } else {
        num = conv;
        size_t read_len = strtol(argv[3], &p, 10);
        double samp_frac = strtod(argv[4], &p);
        assert(samp_frac>0 && samp_frac<=1);

        std::string input_file = std::string(argv[1]);
        if(num==1){
            RL_FMI(input_file, samp_frac, read_len);
        }else{
            FMI(input_file, samp_frac, read_len);
        }
    }
}
