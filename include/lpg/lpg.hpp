//
// Created by diediaz on 11-12-19.
//

#ifndef LMS_GRAMMAR_REP_HPP
#define LMS_GRAMMAR_REP_HPP

#include "lms_algo.hpp"
#include "lpg_iterator.hpp"
#include "cdt/louds_tree.hpp"
#include "cdt/se_int_queue.h"

#include "lpg/suffpair_algo_long.hpp"
#include "lpg/suffpair_algo.hpp"
#include "lpg/repair_algo.hpp"

#include "lpg2bwt/glex_algo.hpp"
#include "lpg2bwt/infbwt_algo.hpp"

#include <pthread.h>

template<class label_arr_type>
class lpg{

private:
    typedef typename lms_algo::rules_data               r_data_t;
    louds_tree                                          m_grammar_tree;
    label_arr_type                                      m_node_pointers;
    sdsl::int_vector<8>                                 symbols_map;
    uint8_t                                             m_sigma{};
    uint8_t                                             c_level{};
    size_t                                              m_max_degree{}; //only for decompression purposes
    friend struct grammar_iterator<lpg>;

    struct decomp_data{
        size_t start;
        size_t end;
        std::string tmp_file;
        lpg &grammar;
        decomp_data(size_t start_, size_t end_, std::string& tmp_file_, lpg& grammar_):
        start(start_), end(end_), tmp_file(tmp_file_), grammar(grammar_){};
    };

    //0 symbol doesn't exists
    //1 symbol exists but it is unique
    //2 symbol appears more than once
    static sdsl::int_vector<2> compute_alphabet(std::string &i_file){
        //TODO this can be done in parallel if the input is too big
        sdsl::int_vector<2> alphabet(256,0);
        for(size_t i=0;i<256;i++) alphabet[i]=0;

        i_file_stream<uint8_t> if_stream(i_file, BUFFER_SIZE);
        uint8_t tmp_symbol;

        for(size_t i=0;i<if_stream.tot_cells;i++){
            tmp_symbol = if_stream.read(i);
            //READ_SYMBOL(if_stream, i, tmp_symbol);
            if(alphabet[tmp_symbol]<2){
                alphabet[tmp_symbol]++;
            }
        }
        return alphabet;
    };

    static void build_grammar(std::string &i_file, std::string &g_file, uint8_t sep_symbol, uint8_t c_level,
                              sdsl::cache_config &config, size_t n_threads, size_t hbuff_size){
        std::cout<<"Computing the alphabet"<<std::endl;
        auto symbol_desc = compute_alphabet(i_file);

        std::string rules_file = sdsl::cache_file_name("rules", config);
        std::string rules_len_file = sdsl::cache_file_name("rules_len", config);
        std::string lmsg_as_sp_file = sdsl::cache_file_name("lmsg_as_sp", config);

        lms_algo::rules_data r_data(rules_file, rules_len_file, lmsg_as_sp_file);
        size_t max_symbol=0;
        for(size_t i=0;i<symbol_desc.size();i++){
            if(symbol_desc[i]!=0){
                r_data.symbols_map.push_back(i);
                r_data.sigma++;
                max_symbol = i;
                symbol_desc[i]--;
            }
        }
        r_data.n_rules = max_symbol+1;
        symbol_desc[sep_symbol]+=2;

        std::cout<<"Computing the LMSg grammar"<<std::endl;
        auto start = std::chrono::high_resolution_clock::now();
        lms_algo::compute_LMS_grammar(i_file, g_file, n_threads, symbol_desc, config, r_data, hbuff_size);
        auto end = std::chrono::high_resolution_clock::now();
        std::chrono::duration<double> elapsed = end - start;
        std::cout<<"  Elap. time (secs): "<<elapsed.count()<<std::endl;

        lms_algo::check_grammar(g_file, i_file);
        if(c_level==2){
            std::cout<<"Suffix-pairing nonterminals"<<std::endl;
            start = std::chrono::high_resolution_clock::now();
            //suffpair(g_file, config, n_threads, hbuff_size);
            suffpair_long(g_file, config);
            end = std::chrono::high_resolution_clock::now();
            elapsed = end - start;
            std::cout<<"  Elap. time (secs): "<<elapsed.count()<<std::endl;
            exit(0);
        }else if(c_level==3){
            std::cout<<"Re-pairing the compressed string"<<std::endl;
            start = std::chrono::high_resolution_clock::now();
            re_pair(g_file, config);
            end = std::chrono::high_resolution_clock::now();
            elapsed = end - start;
            std::cout<<"  Elap. time (secs): "<<elapsed.count()<<std::endl;
        }
    }

    void build_grammar_tree(r_data_t &r_data, sdsl::cache_config& config){

        sdsl::int_vector_buffer<1> tree_shape(sdsl::cache_file_name("lg_tree", config),
                                              std::ios::out);
        //TODO check the width of the g_pointers
        sdsl::int_vector_buffer<64> g_pointers(sdsl::cache_file_name("grammar_pointers", config),
                                               std::ios::out);
        m_max_degree=0;

        size_t tree_pos=0;
        tree_shape[tree_pos++] = true;
        tree_shape[tree_pos++] = false;


        {
            std::cout <<"  Visiting the grammar rules in level-order" << std::endl;
            bool int_node;
            size_t int_rank=1;
            sdsl::int_vector<> g_locus(r_data.n_rules, 0, sdsl::bits::hi(r_data.n_rules) + 1);
            std::string queue_file=sdsl::cache_file_name("queue_file", config);
            se_int_queue<size_t> g_queue(queue_file, BUFFER_SIZE);

            bv_t lmsg_as_sp_bv;
            bool lmsg_as_sp;
            sdsl::load_from_file(lmsg_as_sp_bv, r_data.lms_as_sp_file);
            g_queue.push_back((r_data.n_rules-1)<<1UL);

            //load the succinct (uncompressed) grammar
            sdsl::int_vector<> r;
            sdsl::bit_vector r_lim;
            sdsl::load_from_file(r, r_data.r_file);
            sdsl::load_from_file(r_lim, r_data.r_lim_file);
            sdsl::bit_vector::select_1_type r_lim_ss;
            sdsl::util::init_support(r_lim_ss, &r_lim);


            size_t start, gpos, len, tail;
            while (!g_queue.empty()) {

                auto tmp_sym = g_queue.front();
                g_queue.pop();

                lmsg_as_sp = (tmp_sym & 1UL);
                tmp_sym>>=1UL;

                if (tmp_sym < m_sigma) {//terminal
                    g_pointers.push_back(tmp_sym);
                } else {
                    if (g_locus[tmp_sym] == 0 && !lmsg_as_sp) {//first visit of a nonterminal rule
                        int_node = true;
                        g_locus[tmp_sym] = int_rank;
                    } else {//it is not the first visit of a nonterminal rule
                        if(g_locus[tmp_sym] == 0){
                            //the locus is still undetermined
                            g_pointers.push_back(r_data.n_rules+tmp_sym);
                        }else{
                            g_pointers.push_back(m_sigma + g_locus[tmp_sym]);
                        }
                        int_node = false;
                    }

                    if (int_node) {
                        start = r_lim_ss(tmp_sym)+1;

                        gpos = start;
                        while(!r_lim[gpos]){
                            g_queue.push_back(r[gpos]<<1UL);
                            tree_shape[tree_pos++] = true;
                            gpos++;
                        }
                        len = gpos-start+1;

                        //check if the last symbol is an LMSg nonterminal that occurs in a SP context
                        tail = r[gpos]<<1UL;
                        if(lmsg_as_sp_bv[tmp_sym]) tail |=1UL;
                        g_queue.push_back(tail);
                        tree_shape[tree_pos++] = true;
                        //

                        if (tmp_sym != r_data.n_rules && len > max_degree) {
                            m_max_degree = len;
                        }
                        int_rank++;
                    }
                }
                tree_shape[tree_pos++] = false;
            }

            //update missing loci
            for(auto && g_pointer : g_pointers){
                if(g_pointer>r_data.n_rules){
                    g_pointer = m_sigma + g_locus[g_pointer-r_data.n_rules];
                }
            }
            tree_shape.close();
            sdsl::util::clear(g_locus);
            sdsl::util::clear(r);
            sdsl::util::clear(r_lim);
            sdsl::util::clear(r_lim_ss);
            g_queue.close(true);
        }
        std::cout<<"  Compressing leaf labels"<<std::endl;
        m_node_pointers = label_arr_type(g_pointers, config);
        g_pointers.close();
        std::cout<<"  Building LOUDS rep. of the tree shape"<<std::endl;
        sdsl::bit_vector bv;
        sdsl::load_from_file(bv, sdsl::cache_file_name("lg_tree", config));
        m_grammar_tree = louds_tree(std::move(bv));

        sdsl::register_cache_file("lg_tree", config);
        sdsl::register_cache_file("grammar_pointers", config);
    };

    //static void check_uncomp_grammar(r_data_t &r_data, std::string uncom_seq);

    void check_grammar(std::string &uncom_seq){
        std::cout<<"Checking the grammar produces the exact input string"<<std::endl;

        std::vector<uint32_t > tmp_decomp;
        std::vector<uint32_t > prev_dc_step;

        std::ifstream uc_vec(uncom_seq, std::ifstream::binary);

        uc_vec.seekg(0, std::ifstream::end);
        size_t len = uc_vec.tellg();
        uc_vec.seekg(0, std::ifstream::beg);

        char *buffer = new char[len];
        uc_vec.read(buffer, len);
        uc_vec.close();

        size_t n_children = m_grammar_tree.n_children(root);
        size_t pos=0, tmp_node;
        std::vector<uint8_t> buff;
        for(size_t i=1;i<=n_children;i++){
            tmp_node = m_grammar_tree.child(2, i);
            decompress_node(buff, tmp_node);
            for(unsigned char j : buff){
                assert(j==buffer[pos]);
                pos++;
            }
            buff.clear();
        }
        uc_vec.close();

        delete [] buffer;
        std::cout<<"Everything ok!"<<std::endl;
    }

    void swap(lpg&& other){
        std::swap(m_sigma, other.m_sigma);
        std::swap(m_max_degree, other.m_max_degree);
        std::swap(symbols_map, other.symbols_map);
        std::swap(m_grammar_tree, other.m_grammar_tree);
        std::swap(m_node_pointers, other.m_node_pointers);
    }

public:
    typedef size_t                        size_type;
    static constexpr size_t root          = 2;
    const louds_tree&       tree          = m_grammar_tree;
    const label_arr_type&   l_labels      = m_node_pointers;
    const uint8_t&          sigma         = m_sigma;
    const size_t&           max_degree    = m_max_degree;

    lpg(std::string &input_file, std::string &tmp_folder,
        uint8_t sep_symbol, size_t n_threads, uint8_t c_level_, float hbuff_frac){

        std::cout<<"Input file: "<<input_file<<std::endl;
        std::cout<<"Compression level: "<<size_t(c_level_)<<std::endl;

        //create a temporary folder
        std::string tmp_path = tmp_folder+"/lpg.XXXXXX";
        char temp[200]={0};
        tmp_path.copy(temp, tmp_path.size() + 1);
        temp[tmp_path.size()+1] = '\0';
        auto res = mkdtemp(temp);
        if(res==nullptr){
            std::cout<<"Error trying to create a temporal folder"<<std::endl;
        }
        std::cout<<"Temporal folder: "<<std::string(temp)<<std::endl;
        //

        sdsl::cache_config config(false, temp);
        std::string lg_file = sdsl::cache_file_name("g_file", config);

        //build the uncompressed version of the grammar
        std::ifstream is (input_file, std::ifstream::binary);
        is.seekg (0, std::ifstream::end);
        size_t n_chars = is.tellg();
        is.close();

        //maximum amount of RAM allowed to spend in parallel for the hashing step
        auto hbuff_size = size_t(std::ceil(n_chars*hbuff_frac));

        std::cout<<"Input file contains "<<n_chars<<" symbols"<<std::endl;

        c_level = c_level_;
        auto start = std::chrono::high_resolution_clock::now();
        build_grammar(input_file, lg_file, sep_symbol, c_level, config, n_threads, hbuff_size);
        auto end = std::chrono::high_resolution_clock::now();
        std::chrono::duration<double> elapsed = end - start;

        //plain representation of the grammar
        r_data_t gram_inf;
        gram_inf.load_from_file(lg_file);

        std::cout <<"Resulting grammar stats: "<<std::endl;
        std::cout <<"  Terminals:                " << (size_t)gram_inf.sigma << std::endl;
        std::cout <<"  Nonterminals:             " << gram_inf.n_rules - gram_inf.sigma << std::endl;
        std::cout <<"  Size of the comp. string: " << gram_inf.comp_size << std::endl;
        std::cout <<"  Grammar size:             " << gram_inf.grammar_size - gram_inf.sigma << std::endl;
        std::cout <<"  Elap. time (secs):        "<< elapsed.count()<<std::endl;

        m_sigma = gram_inf.sigma;
        symbols_map.resize(gram_inf.symbols_map.size());
        for(size_t i=0; i < gram_inf.symbols_map.size(); i++){
            symbols_map[i] = gram_inf.symbols_map[i];
        }

        std::cout<<"Building the grammar tree"<<std::endl;
        start = std::chrono::high_resolution_clock::now();
        build_grammar_tree(gram_inf, config);
        end = std::chrono::high_resolution_clock::now();

        elapsed = end - start;
        std::cout<<"  Grammar tree stats:"<<std::endl;
        std::cout <<"    Tot. nodes:         "<<m_grammar_tree.nodes() << std::endl;
        std::cout <<"    Int. nodes:         "<<m_grammar_tree.int_nodes() << std::endl;
        std::cout <<"    Leaves:             "<<m_grammar_tree.nodes() - m_grammar_tree.int_nodes() << std::endl;
        std::cout <<"    Space usage (MB):   "<<sdsl::size_in_mega_bytes(*this)<<std::endl;
        std::cout <<"    Compression ratio:  "<<double(n_chars)/sdsl::size_in_bytes(*this)<<std::endl;
        std::cout <<"  Elap. time (secs):  "<<elapsed.count()<<std::endl;

        //TODO: this is just for testing if the grammar is ok! (it can be removed)
        //check_grammar(input_file);

        /*if(remove(gram_inf.r_file.c_str())){
            std::cout<<" Error trying to remove file "<<gram_inf.r_file<<std::endl;
        }
        if(remove(gram_inf.r_lim_file.c_str())){
            std::cout<<" Error trying to remove file "<<gram_inf.r_lim_file<<std::endl;
        }
        if(remove(gram_inf.lms_as_sp_file.c_str())){
            std::cout<<" Error trying to remove file "<<gram_inf.lms_as_sp_file<<std::endl;
        }
        if(remove(lg_file.c_str())){
            std::cout<<" Error trying to remove file "<<lg_file<<std::endl;
        }

        //delete temporary files
        std::cout<<"Deleting temporal data"<<std::endl;
        sdsl::util::delete_all_files(config.file_map);
        if(rmdir(temp)!=0){
            std::cout<<"Temporal folder couldn't be deleted properly"<<std::endl;
        }*/
    }

    lpg(): m_sigma(0), m_max_degree(0){};

    lpg(const lpg &other){
        m_sigma = other.m_sigma;
        m_max_degree = other.m_max_degree;
        symbols_map = other.symbols_map;
        m_grammar_tree = other.m_grammar_tree;
        m_node_pointers = other.m_node_pointers;
    }

    lpg(lpg &&other) noexcept {
        swap(std::forward<lpg>(other));
    }

    lpg& operator=(lpg&& other) noexcept {
        swap(std::forward<lpg>(other));
        return *this;
    }

    grammar_iterator<lpg> g_begin() const {
        return grammar_iterator<lpg>{*this, root};
    }

    grammar_iterator<lpg> g_end() const {
        return grammar_iterator<lpg>{*this, m_grammar_tree.shape.size()};
    }

    //get an level-order iterator that start from the input node
    grammar_iterator<lpg> node(size_t node) const{
        assert(node>=root && node<m_grammar_tree.shape.size());
        return grammar_iterator<lpg>{*this, node};
    }

    void subtree_shape_int(size_t node_id, std::string& shape) const{
        if(m_grammar_tree.is_leaf(node_id)){
            size_t p_val = m_node_pointers[m_grammar_tree.leaf_rank(node_id) - 1];
            if(p_val<=m_sigma){
                shape.push_back('(');
                shape.push_back(')');
                return;
            }else{
                node_id = m_grammar_tree.int_select(p_val - m_sigma);
            }
        }

        size_t n_children= m_grammar_tree.n_children(node_id);

        shape.push_back('(');
        for(size_t i=1;i<=n_children;i++){
            subtree_shape_int(m_grammar_tree.child(node_id, i), shape);
        }
        shape.push_back(')');
    }

    std::string parse_subtree_shape(size_t node_id) const{
        std::string shape;
        subtree_shape_int(node_id, shape);
        return shape;
    }

    std::string decompress_node(size_t node_id) const {
        std::string res;
        decompress_node<std::string>(res, node_id);
        return res;
    }

    template<class buff_t>
    void decompress_node(buff_t& buffer, size_t node_id) const {

        assert(node_id != 0);
        if(m_grammar_tree.is_leaf(node_id)){
            size_t lab = m_node_pointers[m_grammar_tree.leaf_rank(node_id) - 1];
            if(lab<sigma){
                buffer.push_back(symbols_map[lab]);
                return;
            }else{
                node_id = m_grammar_tree.int_select(lab-m_sigma);
            }
        }

        size_t n_children = m_grammar_tree.n_children(node_id);
        for(size_t r=1;r<=n_children;r++){
            decompress_node(buffer, m_grammar_tree.child(node_id, r));
        }
    }

    static void * decompress_range(void * data){

        size_t start = ((decomp_data*)data)->start;
        size_t end = ((decomp_data*)data)->end;
        lpg &grammar = ((decomp_data*)data)->grammar;

        size_t tmp_node;

        o_file_stream<uint8_t> of_s(((decomp_data*)data)->tmp_file, BUFFER_SIZE, std::ios::out);

        for(size_t i=start;i<=end;i++){
            tmp_node = grammar.m_grammar_tree.child(root, i);
            grammar.decompress_node<o_file_stream<uint8_t>>(of_s, tmp_node);
        }
        of_s.close();

        pthread_exit(nullptr);
    }

    void decompress_text(std::string& tmp_folder, std::string& o_file, size_t n_threads){
        std::cout<<"Decompressing the text"<<std::endl;

        if(tmp_folder.size()>1 && tmp_folder.back()=='/'){
            tmp_folder.pop_back();
        }
        //create a temporary folder
        std::string tmp_path = tmp_folder+"/lpg.XXXXXX";

        char *temp = (char *) malloc(tmp_path.size()+1);
        memcpy(temp, tmp_path.c_str(), tmp_path.size());
        temp[tmp_path.size()] = 0;

        if(mkdtemp(temp)== nullptr){
            std::cout<<"Error trying to set temporal folder : "<<errno<<std::endl;
            exit(1);
        }

        std::cout<<"Temporal folder: "<<std::string(temp)<<std::endl;
        //

        auto start_p = std::chrono::high_resolution_clock::now();

        size_t n_symbols = m_grammar_tree.n_children(2);
        size_t start, end;
        assert(n_symbols!=0 && n_threads>=1);

        size_t sym_per_thread = 1 + ((n_symbols - 1) / n_threads);

        std::vector<decomp_data> d_data;

        for(size_t i=0;i<n_threads;i++){

            start = (i * sym_per_thread) + 1;
            end = std::min<size_t>((i+1) * sym_per_thread, n_symbols);

            std::stringstream ss;
            ss<<temp<<"/chunk_"<<start<<"_"<<end<<".txt";
            std::string tmp_file = ss.str();

            d_data.emplace_back(start, end, tmp_file, *this);
        }

        std::vector<pthread_t> threads(n_threads);
        for(size_t i=0;i<n_threads;i++){

            int ret =  pthread_create(&threads[i],
                                      nullptr,
                                      &lpg::decompress_range,
                                      (void*)&d_data[i]);
            if(ret != 0) {
                printf("Error: pthread_create() failed\n");
                exit(EXIT_FAILURE);
            }
        }

        for(size_t i=0;i<n_threads;i++) {
            pthread_join(threads[i], nullptr);
        }

        //concatenate elements into one file!!
        std::ofstream o_f(o_file, std::ios::out | std::ofstream::binary);
        assert(o_f.good());

        size_t len, rem, to_read;
        char *buffer = new char[BUFFER_SIZE];

        for(auto & chunk : d_data){

            std::ifstream i_f(chunk.tmp_file, std::ifstream::binary);

            i_f.seekg (0, std::ifstream::end);
            len = i_f.tellg();
            i_f.seekg (0, std::ifstream::beg);

            rem = len;
            to_read = std::min<size_t>(BUFFER_SIZE, len);

            while(true){

                i_f.read(buffer, to_read);
                assert(i_f.good());

                o_f.write(buffer, to_read);
                assert(o_f.good());
                //TODO if file not ok, then remove it

                rem -= i_f.gcount();
                to_read = std::min<size_t>(BUFFER_SIZE, rem);
                if(to_read == 0) break;
            }
            i_f.close();

            if(remove(chunk.tmp_file.c_str())){
                std::cout<<"Error trying to remove temporal file"<<std::endl;
                std::cout<<"Aborting"<<std::endl;
                exit(1);
            }
        }

        delete[] buffer;
        o_f.close();

        //delete temporary files
        std::cout<<"Deleting temporal data"<<std::endl;
        if(rmdir(temp)!=0){
            std::cout<<"temporary files couldn't be deleted properly"<<std::endl;
        }
        std::cout<<"Decompression completed"<<std::endl;
        auto end_p = std::chrono::high_resolution_clock::now();

        std::chrono::duration<double> elapsed = end_p - start_p;
        std::cout<<"Output file: "<<o_file<<std::endl;
        std::cout<<"Elap. time: "<<elapsed.count()<<" seconds"<<std::endl;
        free(temp);
    };

    void compute_bwt(std::string& output_file, uint8_t& bwt_of, size_t& n_threads, std::string& tmp_folder) const{

        //create a temporary folder
        std::string tmp_path = tmp_folder+"/lpg.XXXXXX";
        auto *temp = reinterpret_cast<char *>(malloc(tmp_path.size()+1));
        memcpy(temp, tmp_path.c_str(), tmp_path.size());
        temp[tmp_path.size()] = '\0';

        if(mkdtemp(temp)== nullptr){
            std::cout<<"error trying to set temporal file"<<std::endl;
            exit(1);
        }
        std::cout<<"Temporal folder: "<<std::string(temp)<<std::endl;
        //
        sdsl::cache_config config(false, temp);

        auto start = std::chrono::high_resolution_clock::now();
        std::cout<<"Running GLex"<<std::endl;

        auto start1 = std::chrono::steady_clock::now();
        size_t steps = glex<lpg>::infer_ranks(*this, n_threads, config);
        auto end1 = std::chrono::steady_clock::now();
        std::cout<<"Time for GLex: " << std::chrono::duration_cast<std::chrono::microseconds>(end1 - start1).count() << " µs" << std::endl;

        std::cout<<"Running InfBWT"<<std::endl;
        infbwt_algo<lpg>::infer(*this, n_threads, config, steps);

        std::string r_heads_file = sdsl::cache_file_name("new_rl_bwt_heads", config);
        std::string r_len_file = sdsl::cache_file_name("new_rl_bwt_len", config);
        std::string bck_acc_file = sdsl::cache_file_name("new_alph_bck_acc", config);

        size_t bwt_size=0, bwt_runs=0;
        if(bwt_of==0){
            int dest = open(output_file.c_str(), O_WRONLY | O_CREAT, 0644);
            sdsl::int_vector_buffer<> heads_buff(r_heads_file, std::ios::in);
            sdsl::int_vector_buffer<> len_buff(r_len_file, std::ios::in);

            char output_buff[BUFSIZ];
            size_t len, bc=0;
            char sym;
            bwt_runs = heads_buff.size();

            for(size_t i=0;i<len_buff.size();i++){
                len = len_buff[i];
                sym = symbols_map[heads_buff[i]];
                bwt_size+=len;
                for(size_t j=0;j<len;j++){
                    output_buff[bc++] = sym;
                    if(bc==BUFSIZ){
                        if(write(dest, output_buff, BUFSIZ)<0){
                            std::cout<<"Error trying to write the output"<<std::endl;
                            exit(1);
                        };
                        bc=0;
                    }
                }
            }
            if(bc>0){
                if(write(dest, output_buff, bc)<0){
                    std::cout<<"Error trying to write the output"<<std::endl;
                    exit(1);
                };
            }
            close(dest);
        }else if(bwt_of==1){
            std::cout<<"This output format is not implemented yet"<<std::endl;
            exit(1);
        }else if(bwt_of==2){
            rlfm_index<sdsl::wt_int<>> rlfmi(r_heads_file, r_len_file, bck_acc_file);
            sdsl::store_to_file(rlfmi, output_file);
            bwt_runs = rlfmi.runs();
            bwt_size = rlfmi.size();
            assert(rlfmi.alphabet_size()==sigma);
        }
        auto end = std::chrono::high_resolution_clock::now();
        std::chrono::duration<double> elapsed = end - start;

        std::cout<<"  Resulting eBWT:"<<std::endl;
        std::cout<<"    Size:       "<<bwt_size<<std::endl;
        std::cout<<"    Runs:       "<<bwt_runs<<std::endl;
        std::cout<<"    Alphabet:   "<<size_t(sigma)<<std::endl;
        std::cout<<"    Elap. time: "<<elapsed.count()<<" seconds"<<std::endl;

        std::cout<<"Deleting temporal data .."<<std::endl;
        sdsl::util::delete_all_files(config.file_map);
        if (remove(r_len_file.c_str())) {
            std::cout << "Error trying to remove " << r_len_file << std::endl;
        }
        if (remove(r_heads_file.c_str())) {
            std::cout << "Error trying to remove " << r_heads_file << std::endl;
        }
        if (remove(bck_acc_file.c_str())) {
            std::cout << "Error trying to remove " << bck_acc_file << std::endl;
        }
        if(rmdir(temp)!=0){
            std::cout<<"Temporal folder couldn't be deleted properly"<<std::endl;
        }
        free(temp);
    };

    //label of a node
    inline size_t label(size_t node_id) const {
        if(m_grammar_tree.is_leaf(node_id)){
            return m_node_pointers[m_grammar_tree.leaf_rank(node_id) - 1];
        }else{
            return m_grammar_tree.int_rank(node_id)+m_sigma;
        }
    }

    //rth child in the parse tree
    inline size_t pt_child(size_t node_id, size_t r) const{
        assert(node_id>=2);
        if(m_grammar_tree.is_leaf(node_id)){
            size_t label = m_node_pointers[m_grammar_tree.leaf_rank(node_id) - 1];
            assert(label>m_sigma);
            node_id = m_grammar_tree.int_select(label-m_sigma);
        }
        return m_grammar_tree.child(node_id, r);
    }

    //rightmost child in the parse tree
    inline size_t pt_rm_child(size_t node_id) const{
        assert(node_id>=2);
        if(m_grammar_tree.is_leaf(node_id)){
            size_t label = m_node_pointers[m_grammar_tree.leaf_rank(node_id) - 1];
            assert(label>m_sigma);
            node_id = m_grammar_tree.int_select(label-m_sigma);
        }
        return m_grammar_tree.child(node_id, m_grammar_tree.n_children(node_id));
    }

    //leftmost child in the parse tree
    inline size_t pt_lm_child(size_t node_id) const{
        assert(node_id>=2);
        if(m_grammar_tree.is_leaf(node_id)){
            size_t label = m_node_pointers[m_grammar_tree.leaf_rank(node_id) - 1];
            assert(label>m_sigma);
            node_id = m_grammar_tree.int_select(label-m_sigma);
        }
        return m_grammar_tree.child(node_id, 1);
    }

    void load(std::istream &in){
        m_grammar_tree.load(in);
        m_node_pointers.load(in);
        symbols_map.load(in);
        sdsl::read_member(m_max_degree, in);
        sdsl::read_member(m_sigma, in);
        sdsl::read_member(c_level, in);
    }

    size_type serialize(std::ostream &out, sdsl::structure_tree_node *v, std::string name) const{
        sdsl::structure_tree_node* child = sdsl::structure_tree::add_child(v, name, sdsl::util::class_name(*this));
        size_t written_bytes = 0;
        written_bytes += m_grammar_tree.serialize(out, child, "lg_tree");
        written_bytes += m_node_pointers.serialize(out, child, "lg_pointers");
        written_bytes += symbols_map.serialize(out, child, "symbols_map");
        written_bytes += sdsl::write_member(m_max_degree, out, child, "max_degree");
        written_bytes += sdsl::write_member(m_sigma, out, child, "sigma");
        written_bytes += sdsl::write_member(c_level, out, child, "c_level");
        return written_bytes;
    }
};

template <>
void lpg<sdsl::int_vector<>>::load(std::istream &in){

    m_grammar_tree.load(in);

    sdsl::cache_config config;
    std::string lab_file = sdsl::cache_file_name("labels", config);
    size_t lab_width = sdsl::bits::hi(m_grammar_tree.int_nodes())+1;
    size_t lab_space = m_grammar_tree.leaves()*lab_width;
    size_t buff_size = std::min<size_t>(BUFFER_SIZE, INT_CEIL(lab_space,8));
    sdsl::int_vector_buffer<> tmp(lab_file, std::ios::out, buff_size, lab_width);

    {
        huff_vector<> lab_data;
        lab_data.load(in);
        auto it = lab_data.begin();
        auto it_end = lab_data.end();
        while (it != it_end) {
            tmp.push_back(*it);
            ++it;
        }
        tmp.close();
    }

    sdsl::load_from_file(m_node_pointers, lab_file);

    if(remove(lab_file.c_str())){
        std::cout<<"Error trying to remove file "<<lab_file<<std::endl;
    }

    symbols_map.load(in);
    sdsl::read_member(m_max_degree, in);
    sdsl::read_member(m_sigma, in);
    sdsl::read_member(c_level, in);
}



#endif //LMS_GRAMMAR_REP_HPP
