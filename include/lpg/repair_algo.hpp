//
// Created by diego on 22-02-20.
//

#ifndef LMS_COMPRESSOR_PAIRING_ALGORITHMS_H
#define LMS_COMPRESSOR_PAIRING_ALGORITHMS_H
#include <cassert>
#include <iostream>
#include <vector>

#include <sdsl/bit_vectors.hpp>
#include "cdt/hash_table.hpp"
#include "cdt/int_array.h"
#include "cdt/si_int_array.h"

//using pt_vector_t = vlc_int_array<elias_delta>;
//using pt_vector_t = si_int_array;
using pt_vector_t = std::vector<size_t>;

struct pair_data_t {
    size_t freq;
    size_t q_locus;
    size_t id;
    pt_vector_t *occ;
    pair_data_t(size_t freq_, size_t q_locus_, size_t id_, pt_vector_t *occ_) : freq(freq_), q_locus(q_locus_), id(id_), occ(occ_) {};
    pair_data_t() : freq(0), q_locus(0), id(0), occ(nullptr) {};
};

using repair_ht_t =  bit_hash_table<pair_data_t>;

struct repair_data{

    gram_t&              gram_info;
    sdsl::cache_config & config;
    size_t               s_width;
    int_array<size_t>    seq;
    bv_t                 seq_lim;
    bv_t::rank_1_type    seq_lim_rs;
    bv_t                 rep_syms;
    bv_t::rank_1_type    rep_syms_rs;
    int_array<size_t>    sym_dists;
    repair_ht_t          ht;
    size_t               next_av_rule;
    ivb                  new_rules;
    size_t               dummy_sym;

    explicit repair_data(gram_t& gram_info_, sdsl::cache_config& config_): gram_info(gram_info_),
                                                                           config(config_){

        ivb r(gram_info.r_file, std::ios::in);
        s_width = sdsl::bits::hi(r.size()*2) + 1;
        seq = int_array<size_t>(r.size(), s_width);
        dummy_sym = seq.bits.masks[s_width];
        sdsl::load_from_file(seq_lim, gram_info.r_lim_file);

        sdsl::int_vector<2> rep_record(r.size(), 0);
        size_t sym;
        for(auto && i : r){
            sym = i;
            if(rep_record[sym] < 2){
                rep_record[sym]++;
            }
            seq.push_back(sym);
        }
        r.close();

        rep_syms = bv_t(r.size(), false);
        size_t max_rep_len=0;
        size_t tmp_rep_len=0;
        for(size_t i=gram_info.sigma; i < seq.size(); i++){
            if(rep_record[seq[i]] > 1){
                rep_syms[i] = true;
                if(!seq_lim[i-1]){
                    tmp_rep_len++;
                }else{
                    if(tmp_rep_len>max_rep_len){
                        max_rep_len = tmp_rep_len;
                    }
                    tmp_rep_len = 1;
                }
            }else{
                if(tmp_rep_len>max_rep_len){
                    max_rep_len = tmp_rep_len;
                }
                tmp_rep_len = 0;
            }
        }
        if(tmp_rep_len>max_rep_len){
            max_rep_len = tmp_rep_len;
        }
        sdsl::util::init_support(rep_syms_rs, &rep_syms);

        size_t d_width = sdsl::bits::hi(max_rep_len)+1;
        sym_dists = int_array<size_t>(rep_syms_rs(rep_syms.size())*2, d_width);

        size_t mask = (1UL<<d_width)-1UL;
        size_t dist;
        bool prev_is_rep=false;
        for(size_t i=gram_info.sigma; i < seq.size(); i++){

            if(rep_syms[i]){
                dist=0;
                if(!seq_lim[i-1]){
                    dist = prev_is_rep? 1 : mask;
                }
                sym_dists.write(rep_syms_rs(i)*2, dist);

                dist=0;
                if(!seq_lim[i]){
                    dist = rep_syms[i+1] ? 1 : mask;
                }
                sym_dists.write(rep_syms_rs(i)*2+1, dist);
            }
            prev_is_rep = rep_syms[i];
        }
        sym_dists.write(sym_dists.size()-1, mask);

        next_av_rule=gram_info.n_rules-1;
        std::string nr_file=sdsl::cache_file_name("new_rp_rules", config);
        new_rules = ivb(nr_file, std::ios::out, BUFFER_SIZE);
        sdsl::util::init_support(seq_lim_rs, &seq_lim);
    };

    ~repair_data(){
        if(new_rules.is_open()){
            new_rules.close(true);
        }
    }
};

struct pair_greater {
    const bitstream<ht_t::buff_t>& ht_data;
    size_t dist_bits;//distance from the pointer to the position where the pair_value starts
    size_t pt_bits=sizeof(size_t)*8;//number of bits used for the frequency

    pair_greater(const bitstream<ht_t::buff_t>& ht_str, size_t db): ht_data(ht_str), dist_bits(db) {};

    bool operator()(const size_t& pt1, const size_t& pt2) const {
        size_t freq1 = ht_data.read(pt1+dist_bits, pt1+dist_bits+pt_bits-1);
        size_t freq2 = ht_data.read(pt2+dist_bits, pt2+dist_bits+pt_bits-1);
        return freq1>freq2;
    }

    inline size_t get_value(size_t pt)const{
        return ht_data.read(pt+dist_bits, pt+dist_bits+pt_bits-1);
    }
};

struct priority_queue{

    repair_ht_t& ht;
    pair_greater comp;
    std::vector<size_t> arr;
    typedef long long int size_type;

    inline void swap(size_t l, size_t r){
        pair_data_t pd_l, pd_r;
        ht.get_value_from(arr[l], pd_l);
        ht.get_value_from(arr[r], pd_r);

        std::swap(arr[l], arr[r]);

        pd_l.q_locus = r;
        pd_r.q_locus = l;
        ht.insert_value_at(arr[r], pd_l);
        ht.insert_value_at(arr[l], pd_r);
    }

    inline static size_type parent(size_type idx) {
        return (idx-1)/2;
    }

    inline static size_type l_child(size_type idx) {
        return (2*idx + 1);
    }

    inline static size_type r_child(size_type idx) {
        return (2*idx + 2);
    }

    inline void trim_leaves() {
        auto pt = arr.back();
        while(!arr.empty() && comp.get_value(pt)<2){

            pair_data_t pd;
            ht.get_value_from(pt, pd);
            delete pd.occ;

            arr.pop_back();
            if(!arr.empty()) pt = arr.back();
        }
    }

    void sift_down(size_t idx){

        size_t largest, left, right;

        while(true){

            largest = idx;
            left = l_child(idx);
            right = r_child(idx);

            if(left<arr.size() && comp(arr[left], arr[idx])){
                largest = left;
            }

            if(right<arr.size() && comp(arr[right], arr[largest])){
                largest = right;
            }

            if(largest!=idx){
                swap(idx, largest);
                idx = largest;
            }else{
                break;
            }
        }
    }

    void sift_up(size_t idx){
        while (idx != 0 && comp(arr[idx], arr[parent(idx)]) ){
            swap(idx, parent(idx));
            idx = parent(idx);
        }
    }

    explicit priority_queue(pair_greater& comp_, repair_ht_t& ht_): ht(ht_), comp(comp_) {};

    inline bool empty() const{
        return arr.empty();
    }

    inline size_t size() const{
        return arr.size();
    }

    size_type extract_max(){
        assert(!empty());
        // get the maximum value, and remove it from heap
        size_t root = arr[0];
        swap(0, arr.size()-1);
        arr.pop_back();
        sift_down(0);
        return root;
    }

    void insert(size_t elm) {
        arr.push_back(elm);

        //insert the queue locus in
        // the hash table
        pair_data_t pd;
        ht.get_value_from(elm, pd);
        pd.q_locus = arr.size()-1;
        ht.insert_value_at(elm, pd);
        //

        sift_up(arr.size()-1);
    }
};

void update_grammar(repair_data& rp_data){

    size_t new_gsyms = rp_data.new_rules.size();

    size_t new_g_size=0;
    for(size_t i=0;i<rp_data.seq.size();i++){
        if(rp_data.seq[i]!=rp_data.dummy_sym) new_g_size++;
    }
    new_g_size += new_gsyms;

    sdsl::int_vector_buffer<1> r_lim(rp_data.gram_info.r_lim_file, std::ios::out, BUFFER_SIZE);
    ivb r(rp_data.gram_info.r_file, std::ios::out, BUFFER_SIZE, rp_data.s_width);

    for(size_t i=0;i<rp_data.gram_info.sigma;i++){
        r.push_back(rp_data.seq[i]);
        r_lim.push_back(true);
    }

    size_t new_c_size=0;
    size_t gsyms = rp_data.gram_info.grammar_size-rp_data.gram_info.comp_size;
    for(size_t i=rp_data.gram_info.sigma;i<gsyms;i++){
        if(rp_data.seq[i]!=rp_data.dummy_sym){
            r.push_back(rp_data.seq[i]);
        }
        if(rp_data.seq_lim[i]){
            r_lim[r.size()-1] = true;
        }
    }

    for(size_t i=0;i<rp_data.new_rules.size();i++){
        r.push_back(rp_data.new_rules[i]);
        r_lim.push_back(i & 1UL);
    }

    for(size_t i=gsyms;i<rp_data.seq.size();i++){
        if(rp_data.seq[i]!=rp_data.dummy_sym){
            r.push_back(rp_data.seq[i]);
            new_c_size++;
        }
    }
    r_lim[r.size()-1] = true;

    r.close();
    r_lim.close();

    std::cout<<"  Re-Pair stats:"<<std::endl;
    std::cout<<"    Grammar size before:         "<<rp_data.gram_info.grammar_size-rp_data.gram_info.sigma<<std::endl;
    std::cout<<"    Grammar size after:          "<<new_g_size-rp_data.gram_info.sigma<<std::endl;
    std::cout<<"    Number of new nonterminals:  "<<new_gsyms/2<<std::endl;
    std::cout<<"    Compression ratio:           "<<double(new_g_size)/rp_data.gram_info.grammar_size<<std::endl;

    rp_data.gram_info.grammar_size = new_g_size;
    rp_data.gram_info.n_rules += new_gsyms/2;
    rp_data.gram_info.comp_size = new_c_size;
}

static void repair_prim_pairs(repair_data& rp_data, priority_queue& p_queue){

    int_array<size_t> pair(2, rp_data.s_width);
    size_t rep, lim;

    {//get the frequency of the first pairs

        rep = rp_data.rep_syms[rp_data.gram_info.sigma]<<1UL;
        lim = 4;// 100

        bit_hash_table<size_t, 44> rep_map;

        size_t val=0;
        for (size_t i = rp_data.gram_info.sigma; i < rp_data.seq.size()-1; i++) {

            rep |= rp_data.rep_syms[i+1];
            lim |= rp_data.seq_lim[i+1];

            //11: both symbols are repeated
            //!10: pair is not a junction of two rules
            if ((rep & 3UL) == 3UL && (lim & 3UL) != 2U) {
                pair.write(0, rp_data.seq[i]);
                pair.write(1, rp_data.seq[i + 1]);
                auto res = rep_map.insert(pair.data(), pair.n_bits(), 1);
                if (!res.second) {
                    rep_map.get_value_from(*res.first, val);
                    rep_map.insert_value_at(*res.first, val + 1);
                }
            }
            rep <<= 1U;
            lim <<= 1U;
        }

        size_t freq=0;
        auto key = reinterpret_cast<char*>(malloc(sizeof(size_t)*2));
        memset(key, 0, sizeof(size_t)*2);

        for(auto pt : rep_map){
            rep_map.get_value_from(pt, freq);
            if(freq>1){
                size_t k_bits = rep_map.get_key(pt, key);
                assert(k_bits==rp_data.s_width*2);
                auto occ = new pt_vector_t();
                pair_data_t new_pair{freq,0,0,occ};
                rp_data.ht.insert(key, k_bits, new_pair);
            }
        }
        free(key);
    }

    //store the positions of the repeated pairs
    rep = rp_data.rep_syms[rp_data.gram_info.sigma]<<1UL;
    lim = 4;// 100
    for (size_t i = rp_data.gram_info.sigma; i < rp_data.seq.size()-1; i++) {
        rep |= rp_data.rep_syms[i+1];
        lim |= rp_data.seq_lim[i+1];

        if((rep & 3UL)==3UL && (lim & 3UL) != 2U){ //==11 repeated symbol

            pair.write(0, rp_data.seq[i]);
            pair.write(1, rp_data.seq[i+1]);

            auto res = rp_data.ht.find(pair.data(), pair.n_bits());

            if(res.second){
                pair_data_t tmp;
                rp_data.ht.get_value_from(*res.first, tmp);
                if(rp_data.seq_lim[i-1] && !rp_data.seq_lim[i] && rp_data.seq_lim[i+1]){
                    tmp.id = rp_data.seq_lim_rs(i);
                }
                tmp.occ->push_back(i);
                rp_data.ht.insert_value_at(*res.first, tmp);
            }
        }
        rep <<= 1U;
        lim <<= 1U;
    }

    for(auto pt : rp_data.ht){
        p_queue.insert(pt);
    }
}

static void re_pair_int(repair_data& rp_data){

    pair_greater pg{rp_data.ht.get_data(), rp_data.ht.description_bits()+(2*rp_data.s_width)};
    priority_queue p_queue(pg, rp_data.ht);

    repair_prim_pairs(rp_data, p_queue);

    size_t l_dist, r_dist, n_r_dist;
    size_t t_pos, t_r_pos;

    size_t curr_pt;
    pair_data_t p_data;

    int_array<size_t> tmp_pair(2, rp_data.s_width);
    int_array<size_t> curr_pair(2, rp_data.s_width);

    key_wrapper key_w{rp_data.s_width,
                      rp_data.ht.description_bits(),
                      rp_data.ht.get_data()};

    size_t d_mask = (1UL<<rp_data.sym_dists.width())-1UL;
    bool is_rule;

    while(!p_queue.empty()) {

        curr_pt = p_queue.extract_max();

        rp_data.ht.get_value_from(curr_pt, p_data);
        curr_pair.write(0, key_w.read(curr_pt, 0));
        curr_pair.write(1, key_w.read(curr_pt, 1));

        if(p_data.freq>1){

            if(p_data.id==0){
                p_data.id = rp_data.next_av_rule++;
                rp_data.new_rules.push_back(curr_pair[0]);
                rp_data.new_rules.push_back(curr_pair[1]);
            }

            for (auto const &pos : *(p_data.occ)) {

                //do the pair replacement
                t_pos = rp_data.rep_syms_rs(pos);

                l_dist = rp_data.sym_dists[t_pos*2];
                r_dist = rp_data.sym_dists[t_pos*2+1];

                //the pair was modified and the neighbor is unique
                if (r_dist == d_mask) continue;

                //right distance of the right symbol of the pair
                t_r_pos = rp_data.rep_syms_rs(pos + r_dist);
                n_r_dist = rp_data.sym_dists[(t_r_pos*2)+1];

                //check if the current occurrence of
                // the pair covers an entire rule
                is_rule = (l_dist == 0) && (n_r_dist == 0);

                tmp_pair.write(0, rp_data.seq[pos]);
                tmp_pair.write(1, rp_data.seq[pos+r_dist]);

                if (tmp_pair == curr_pair && !is_rule) {

                    //replace the pair with its id
                    rp_data.seq.write(pos, p_data.id);
                    rp_data.seq.write(pos+r_dist, rp_data.dummy_sym);

                    //invalidate the distances of the right symbol
                    rp_data.sym_dists.write((t_r_pos*2), d_mask);
                    rp_data.sym_dists.write((t_r_pos*2)+1, d_mask);

                    if (n_r_dist != d_mask && n_r_dist!=0) {
                        r_dist += n_r_dist;
                        t_r_pos = rp_data.rep_syms_rs(pos + r_dist);
                        //left distance of the symbol next to the right pair symbol
                        rp_data.sym_dists.write(t_r_pos*2, r_dist);
                    } else {
                        r_dist = n_r_dist;
                    }

                    rp_data.sym_dists.write((t_pos*2)+1, r_dist);

                    //hash the new pairs
                    if (l_dist!=0 && l_dist != d_mask) {

                        //decrease the frequency of the pair to the left
                        pair_data_t pd1;
                        tmp_pair.write(0, rp_data.seq[pos-l_dist]);
                        tmp_pair.write(1, curr_pair[0]);
                        auto res1 = rp_data.ht.find(tmp_pair.data(), tmp_pair.n_bits());
                        if(res1.second){
                            rp_data.ht.get_value_from(*res1.first, pd1);
                            if(pd1.freq>0){
                                pd1.freq--;
                            }
                            rp_data.ht.insert_value_at(*res1.first, pd1);
                            p_queue.sift_down(pd1.q_locus);
                        }
                        //

                        //increase the frequency of the new pair to the right
                        pair_data_t pd2;
                        tmp_pair.write(0, rp_data.seq[pos-l_dist]);
                        tmp_pair.write(1, rp_data.seq[pos]);

                        //check if the new pair covers an entire rule
                        size_t t_l_pos = rp_data.rep_syms_rs(pos - l_dist);
                        size_t n_l_dist = rp_data.sym_dists[(t_l_pos*2)];
                        if((n_l_dist == 0) && (r_dist == 0)){
                            pd2.id = rp_data.seq_lim_rs(pos-l_dist);
                        }
                        //

                        auto res2 = rp_data.ht.find(tmp_pair.data(), tmp_pair.n_bits());
                        if(!res2.second) {
                            pd2.occ = new pt_vector_t();
                            pd2.freq=1;
                            auto ires = rp_data.ht.insert(tmp_pair.data(), tmp_pair.n_bits(), pd2);
                            p_queue.insert(*ires.first);
                        }else{
                            rp_data.ht.get_value_from(*res2.first, pd2);
                            pd2.freq++;
                            rp_data.ht.insert_value_at(*res2.first, pd2);
                            p_queue.sift_up(pd2.q_locus);
                        }
                        pd2.occ->push_back(pos - l_dist);
                    }

                    if (r_dist!=0 && r_dist != d_mask) {

                        //decrease the frequency of the pair to the right
                        pair_data_t pd1;
                        tmp_pair.write(0, curr_pair[1]);
                        tmp_pair.write(1, rp_data.seq[pos+r_dist]);
                        auto res1 = rp_data.ht.find(tmp_pair.data(), tmp_pair.n_bits());
                        if(res1.second){
                            rp_data.ht.get_value_from(*res1.first, pd1);
                            if(pd1.freq>0){
                                pd1.freq--;
                            }
                            rp_data.ht.insert_value_at(*res1.first, pd1);
                            p_queue.sift_down(pd1.q_locus);
                        }
                        //

                        //increase the frequency of the new right pair
                        pair_data_t pd2;
                        tmp_pair.write(0, rp_data.seq[pos]);
                        tmp_pair.write(1, rp_data.seq[pos+r_dist]);

                        //check if the current occurrence covers an entire rule
                        t_r_pos = rp_data.rep_syms_rs(pos + r_dist);
                        n_r_dist = rp_data.sym_dists[(t_r_pos*2)+1];

                        if((l_dist==0) && (n_r_dist==0)){
                            pd2.id = rp_data.seq_lim_rs(pos);
                        }

                        auto res = rp_data.ht.find(tmp_pair.data(), tmp_pair.n_bits());
                        if(!res.second) {
                            pd2.occ = new pt_vector_t();
                            pd2.freq = 1;
                            auto ires = rp_data.ht.insert(tmp_pair.data(), tmp_pair.n_bits(), pd2);
                            p_queue.insert(*ires.first);
                        }else{
                            rp_data.ht.get_value_from(*res.first, pd2);
                            pd2.freq++;
                            rp_data.ht.insert_value_at(*res.first, pd2);
                            p_queue.sift_up(pd2.q_locus);
                        }
                        pd2.occ->push_back(pos);
                    }
                }
            }
            p_queue.trim_leaves();
        }
        //release memory
        delete p_data.occ;
    }

    rp_data.ht.reset();
    sdsl::util::clear(rp_data.rep_syms);
    sdsl::util::clear(rp_data.rep_syms_rs);

    update_grammar(rp_data);
}

static void re_pair(std::string &g_file, sdsl::cache_config &config) {
    //Load the grammar from file
    gram_t gram_info;
    gram_info.load_from_file(g_file);
    repair_data rp_data{gram_info, config};
    re_pair_int(rp_data);
    gram_info.save_to_file(g_file);
}

/*
class pairing_algo {
    struct succ_gram {
        size_type n_rules{};
        size_type grammar_size{};
        uint8_t sigma{};
        size_type max_rule_len{};
        size_t tot_nt_syms{};
        size_type *pairs_seq;

        sdsl::bit_vector limits;
        sdsl::bit_vector::rank_1_type limits_rs;

        ~succ_gram() {
            allocator::deallocate(pairs_seq);
        }
    };



    template<class pair_type>
    static void collect_and_store_new_pairs(succ_gram &gram, pair_data_t<pair_type>* pairs_list){


        uint8_t width = (sizeof(pair_type) * 8) / 2;

        //collect and sort the new pairs
        std::vector<std::tuple<size_type, size_type, size_type>> new_rules;

        pair_data_t<pair_type>* curr_pair  = pairs_list;
        pair_data_t<pair_type>* tmp_pair;

        //TODO Repair: to reduce even further, I can delete those new rules with one occurrence.
        // This happens for instance with a pair occurs several times, but all those occurrences
        // are followed or preceded by the same symbol. In the grammar, this traduces to a
        // non-terminal with one occurrence. This space won't be reduced, but the decompression
        // time might be better.
        size_type left, right;
        while(curr_pair!= nullptr){
            if(curr_pair->id>=gram.n_rules){
                left = size_type((curr_pair->pair)>>width);
                right = (curr_pair->pair & ((1UL<<width)-1UL));
                new_rules.emplace_back(left, right, curr_pair->id);
            }
            tmp_pair = curr_pair;
            curr_pair = curr_pair->next;
            delete tmp_pair;
        }

        std::sort(new_rules.begin(), new_rules.end(), [](const auto& left, const auto &right){
            return std::get<2>(left)<std::get<2>(right);
        });

        size_t npos_av=0;
        for(size_t i=0;i<gram.tot_nt_syms; i++){
            if(gram.pairs_seq[i] != 0){

                gram.pairs_seq[npos_av] = gram.pairs_seq[i];

                if(gram.limits[i]){
                    gram.limits[npos_av] = true;
                    if(npos_av != i) gram.limits[i] = false;
                }
                npos_av++;

            }else if(gram.limits[i]){
                gram.limits[npos_av] = true;
                if(npos_av != i) gram.limits[i] = false;
            }
        }

        size_t tot_syms = npos_av + (new_rules.size() * 2);
        gram.pairs_seq = allocator::reallocate<size_type>(gram.pairs_seq, gram.tot_nt_syms, tot_syms, 0);
        gram.limits.resize(tot_syms+1);

        for(auto const& tuple : new_rules){
            gram.pairs_seq[npos_av] = std::get<0>(tuple);
            gram.limits[npos_av] = true;
            gram.pairs_seq[npos_av + 1] = std::get<1>(tuple);
            gram.limits[npos_av + 1] = false;
            npos_av+=2;
        }
        gram.limits[npos_av] = true;

        size_t new_gram_size = gram.grammar_size-gram.tot_nt_syms+tot_syms;
        std::cout<<"    Pairing stats:"<<std::endl;
        std::cout<<"      Grammar size before:         "<<gram.grammar_size<<std::endl;
        std::cout<<"      Grammar size after:          "<<new_gram_size<<std::endl;
        std::cout<<"      Number of new nonterminals:  "<<new_rules.size()<<std::endl;
        std::cout<<"      Compression ratio:           "<<double(tot_syms)/gram.grammar_size<<std::endl;

        gram.n_rules = gram.n_rules+new_rules.size();
        gram.grammar_size = new_gram_size;
        gram.tot_nt_syms = tot_syms;
        std::vector<std::tuple<size_type, size_type, size_type>>().swap(new_rules);
    }


    static void compute_symbol_dists(sdsl::int_vector<>& sym_dists, gram_t& gram,
                                     bv_t& sym_rep, bv_t::rank_1_type& sym_rep_rs){

        size_t d_width = sdsl::bits::hi(gram.comp_size);

        sym_dists.width(d_width);
        sym_dists.resize(sym_rep_rs(gram.tot_nt_syms));
        size_t null_val= (1UL<<d_width)-1UL;

        size_t dist;
        bool prev_is_rep = false;

        for (size_t i = 0; i < gram.tot_nt_syms; i++) {

            if(sym_rep[i]){
                dist=0;
                if(!gram.limits[i]){
                    if(prev_is_rep){
                        dist =1U<<d_width;
                    }else{
                        dist = null_val<<d_width;
                    }
                }

                if(i<(gram.tot_nt_syms-1) && !gram.limits[i+1]){
                    if(sym_rep[i+1]){
                        dist|= 1U;
                    }else{
                        dist |=null_val;
                    }
                }
                sym_dists[sym_rep_rs(i)] = dist;
            }
            prev_is_rep = sym_rep[i];
        }
    }

public:

        static void prefix_pairing(size_type ** rules, size_type* rules_len, size_type& n_rules, uint8_t& sigma) {

            //some aliases to make the code more readable
            typedef std::pair<size_type, size_type>      value_t; //(freq, m_id)

            //TODO remove this in the future (it is just for debugging)
            size_t cont=0;
            for(size_t i=(sigma+1);i<n_rules;i++){
                assert(rules_len[i]>1);
                cont+=rules_len[i];
            }
            //LOG(DEBUG) << "There are "<<cont<<" symbols before prefix pairing ";
            //

            size_t g_orig_size = n_rules;
            auto buffer_size = (size_t)ceil(n_rules*1.1);

            rules = allocator::reallocate<size_type*>(rules, n_rules, buffer_size, nullptr);
            rules_len = allocator::reallocate<size_type>(rules_len, n_rules, buffer_size, 0);

            std::unordered_map<std::pair<size_type, size_type>, value_t, pair_hash> prefixes;
            bool rule_is_pair;

            std::queue<size_type> active_rules;
            std::pair<size_type, size_type> tmp_pair;
            size_t idx;

            active_rules.push(0);

            for(size_t i=(sigma+1);i<g_orig_size;i++){
                rule_is_pair = rules_len[i]==2;
                tmp_pair.first = rules[i][0];
                tmp_pair.second = rules[i][1];
                auto res = prefixes.insert({tmp_pair, {1, rule_is_pair ? i : 0}});
                if(!res.second){
                    res.first->second.first++;
                    if(rule_is_pair) res.first->second.second = i;
                }
                if(!rule_is_pair) active_rules.push(i);
            }

            while(!active_rules.empty()){

                idx = active_rules.front();
                active_rules.pop();

                if(idx==0){
                    for (auto it = prefixes.begin(), next_it = it; it != prefixes.end(); it = next_it){

                        ++next_it;
                        if(it->second.first<=1){
                            prefixes.erase(it);
                        }else if(it->second.second==0){

                            it->second.second = n_rules;
                            rules[n_rules] = allocator::allocate<size_type>(2,0);

                            rules[n_rules][0] = it->first.first;
                            rules[n_rules][1] = it->first.second;
                            rules_len[n_rules] = 2;
                            n_rules++;

                            if(n_rules==buffer_size){
                                buffer_size = (size_t)ceil(n_rules*1.1);
                                //TODO Here I should use my allocator!!
                                rules = (size_type**)realloc(rules, sizeof(size_type*)*buffer_size);
                                rules_len = (size_type*)realloc(rules_len, sizeof(size_type)*buffer_size);
                                for(size_t i=n_rules; i < buffer_size; i++){
                                    rules[i] = nullptr;
                                    rules_len[i] = 0;
                                }
                                //
                            }
                        }
                    }

                    //LOG(DEBUG) << prefixes.size() << " remaining repeated prefixes";
                    if(!prefixes.empty()) active_rules.push(0);

                }else{

                    if(rules_len[idx]==2) continue;

                    tmp_pair.first = rules[idx][0];
                    tmp_pair.second = rules[idx][1];

                    auto f_res = prefixes.find(tmp_pair);

                    if(f_res != prefixes.end()){

                        f_res->second.first--;
                        rules[idx][0] = f_res->second.second;
                        for(size_t i=1;i<(rules_len[idx]-1);i++){
                            rules[idx][i] = rules[idx][i+1];
                        }
                        rules_len[idx]--;

                        rule_is_pair = rules_len[idx]==2;
                        tmp_pair.first = rules[idx][0];
                        tmp_pair.second = rules[idx][1];

                        auto i_res = prefixes.insert({tmp_pair, {1, rule_is_pair ? idx : 0}});

                        if(!i_res.second){
                            i_res.first->second.first++;
                            if(rule_is_pair) i_res.first->second.second = idx;
                        }

                        if(!rule_is_pair) active_rules.push(idx);
                    }
                }
            }

            //shrink the array!
            rules = allocator::reallocate<size_type *>(rules, buffer_size, n_rules, nullptr);
            rules_len = allocator::reallocate<size_type>(rules_len, buffer_size, n_rules, 0);
            for(size_t i=(sigma+1);i<n_rules;i++){
                rules[i] = allocator::reallocate<size_type>(rules[i], rules_len[i], rules_len[i], 0);
            }
            //LOG(DEBUG) << "The prefix pairing step produced "<<n_rules-g_orig_size<<" extra non-terminals";

            //TODO: remove in the future
            cont=0;
            for(size_t i=6;i<n_rules;i++){
                assert(rules_len[i]>1);
                cont+=rules_len[i];
            }
            //LOG(DEBUG) << "There are "<<cont<<" symbols after prefix pairing ";
            //

        }

    static void load_succ_grammar(std::string &lg_file, succ_gram &gram) {

        size_type buffer[255];
        std::ifstream fp(lg_file, std::ifstream::binary);

        fp.seekg(0, std::ifstream::beg);

        //get number of rules and alphabet size
        fp.read((char *) buffer, sizeof(size_type) * 3);
        gram.n_rules = buffer[0];
        gram.sigma = (uint8_t) buffer[1];
        gram.grammar_size = buffer[2];

        //skip symbol_maps and grammar_size
        fp.seekg(sizeof(size_type) * (gram.sigma + 1), std::ifstream::cur);

        //skip compressed sequence
        size_t tmp_len;
        fp.read((char *) buffer, sizeof(size_type));
        tmp_len = buffer[0];
        size_t n_symbols = gram.grammar_size-tmp_len;
        fp.seekg(sizeof(size_type) * tmp_len, std::ifstream::cur);

        gram.tot_nt_syms = n_symbols;
        gram.limits.resize(n_symbols + 1);
        gram.pairs_seq = allocator::allocate<size_type>(n_symbols, 0);

        for (size_t i = 0; i < n_symbols; i++) gram.limits[i] = false;
        gram.limits[n_symbols] = true;

        size_t curr_pos = 0, max_len = 0;
        for (size_t i = 1; i < gram.n_rules; i++) {

            fp.read((char *) buffer, sizeof(size_type));
            tmp_len = buffer[0];
            if (tmp_len > max_len) max_len = tmp_len;

            if (tmp_len > 1) {
                gram.limits[curr_pos] = true;
                fp.read((char *) &gram.pairs_seq[curr_pos], sizeof(size_type) * tmp_len);
                curr_pos += tmp_len;
            }
        }

        gram.max_rule_len = max_len;
        sdsl::util::init_support(gram.limits_rs, &gram.limits);
    }

    static void store_succ_grammar(std::string &lg_file, succ_gram &gram) {

        size_type *symbols_map;
        size_type *seq;

        std::ifstream fp(lg_file, std::ifstream::binary);

        //skip alphabet size, number of rules and grammar size as they are already known
        fp.seekg(sizeof(size_type) * 3, std::ifstream::cur);

        //read alphabet m_map
        symbols_map = allocator::allocate<size_type>(gram.sigma + 1, 0);
        fp.read((char *) symbols_map, sizeof(size_type) * (gram.sigma + 1));

        //read compressed sequence
        size_type seq_len;
        fp.read((char *) &seq_len, sizeof(size_type));
        seq = allocator::allocate<size_type>(seq_len, 0);
        fp.read((char *) seq, sizeof(size_type) * seq_len);
        fp.close();

        //write down the new version of the grammar
        std::ofstream of(lg_file, std::ofstream::out | std::ofstream::binary);

        //the new number of rules
        of.write((char *) &gram.n_rules, sizeof(size_type));
        //the alphabet of terminals
        of.write((char *) &gram.sigma, sizeof(size_type));
        //the size of the grammar
        of.write((char *) &gram.grammar_size, sizeof(size_type));

        //the symbols m_map
        of.write((char *) symbols_map, sizeof(size_type) * (gram.sigma + 1));
        allocator::deallocate(symbols_map);

        //compressed sequence
        of.write((char *) &seq_len, sizeof(size_type));
        of.write((char *) seq, sizeof(size_type) * seq_len);
        allocator::deallocate(seq);

        //terminals
        size_type rule_len = 1;
        for (size_t i = 0; i < gram.sigma; i++) {
            of.write((char *) &rule_len, sizeof(size_type));
        }
        assert(of.good());

        //write the new rules
        size_t pos = 0;
        while (pos < gram.tot_nt_syms) {
            while (!gram.limits[pos + 1]) {
                pos++;
                rule_len++;
            }
            of.write((char *) &rule_len, sizeof(size_type));
            of.write((char *) &gram.pairs_seq[pos - (rule_len - 1)], sizeof(size_type) * rule_len);
            assert(of.good());
            rule_len = 1;
            pos++;
        }
        of.write((char *) &gram.tot_nt_syms, sizeof(size_t));
        of.close();
    }
};
*/
#endif //LMS_COMPRESSOR_PAIRING_ALGORITHMS_H
