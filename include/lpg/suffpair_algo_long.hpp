//
// Created by Diego Diaz on 5/7/21.
//

#ifndef LPG_COMPRESSOR_SUFFIX_PAIR_LONG_HPP
#define LPG_COMPRESSOR_SUFFIX_PAIR_LONG_HPP

#include<unordered_map>
#include "lms_algo.hpp"

typedef lms_algo::rules_data      plain_gram_t;
typedef sdsl::bit_vector          bv_t;
typedef sdsl::int_vector_buffer<> ivb;

struct suffix_trie_node{
    std::unordered_map<uint32_t, suffix_trie_node*> children;
    uint32_t id{};
};

struct suffix_trie{
    suffix_trie_node root;
};

void gather_new_nonterminals(suffix_trie_node* node, std::vector<size_t> seq, size_t& new_id,
                             ivb& new_rules, sdsl::int_vector_buffer<1>& new_rules_lim){

    if(node->children.size()>1 && seq.size()>1){
        node->id = node->id==0? new_id++ : node->id;
        //std::reverse(seq.begin(), seq.end());
        //std::cout<<node->id<<" : ";
        for(auto const sym : seq){
            //std::cout<<sym<<" ";
            new_rules.push_back(sym);
        }
        //std::cout<<""<<std::endl;
        new_rules_lim[new_rules.size()-1] = true;

        seq = {node->id};
    }

    for(auto const& child : node->children){
        auto new_seq(seq);
        new_seq.push_back(child.first);
        gather_new_nonterminals(child.second,new_seq ,
                                new_id, new_rules,
                                new_rules_lim);
    }
}

//change the width of R and compute the repeated symbols
void prep_input(plain_gram_t& p_gram, bv_t& rep_syms, sdsl::cache_config& config) {

    ivb r(p_gram.r_file, std::ios::in);

    std::string tmp_string = sdsl::cache_file_name("tmp_rules", config);
    o_file_stream<size_t> tmp_r(tmp_string, BUFFER_SIZE, std::ios::out);
    sdsl::int_vector<2> tmp_rep(p_gram.n_rules - 1, false);

    for (auto const &sym : r) {
        tmp_r.push_back(sym);
        if (tmp_rep[sym] < 2) tmp_rep[sym]++;
    }
    r.close(true);
    tmp_r.close();

    size_t j = 0;
    for (auto &&i : tmp_rep) {
        assert(i == 1 || i == 2);
        rep_syms[j++] = i - 1;
    }
    rename(tmp_string.c_str(), p_gram.r_file.c_str());
}

void build_trie(plain_gram_t& p_gram, bv_t& rep_syms, sdsl::cache_config& config){

    i_file_stream<size_t> rules(p_gram.r_file, BUFFER_SIZE);

    bv_t r_lim;
    sdsl::load_from_file(r_lim, p_gram.r_lim_file);
    bv_t::select_1_type r_lim_ss(&r_lim);

    size_t curr_rule = p_gram.sigma, pos, start;
    suffix_trie trie;
    suffix_trie_node *node;

    while(curr_rule<p_gram.n_rules){
        start = r_lim_ss(curr_rule)+1;
        pos = r_lim_ss(curr_rule+1);
        node = &trie.root;

        while(pos>=start && rep_syms[rules.read(pos)]){
            auto res = node->children.find(rules.read(pos));
            if(res != node->children.end()){
                node = res->second;
            }else{
                auto new_child = new suffix_trie_node();
                new_child->id = pos==start? curr_rule : 0;

                node->children.insert({rules.read(pos), new_child});
                node = new_child;
            }
            pos--;
        }
        curr_rule++;
    }

    size_t id= p_gram.n_rules;

    std::string new_rules_file = sdsl::cache_file_name("new_rules_file", config);
    std::string new_rules_lim_file = sdsl::cache_file_name("new_rules_file_lim", config);

    ivb new_rules(new_rules_file, std::ios::out);
    sdsl::int_vector_buffer<1> new_rules_lim(new_rules_lim_file, std::ios::out);

    gather_new_nonterminals(&trie.root, {}, id, new_rules, new_rules_lim);


    curr_rule = p_gram.sigma;
    size_t lpos, nt;
    while(curr_rule<p_gram.n_rules){

        start = r_lim_ss(curr_rule)+1;
        pos = r_lim_ss(curr_rule+1);
        node = &trie.root;

        auto res = node->children.find(rules.read(pos--));
        while(pos>start && res!=node->children.end()){
            if(node->id!=0){
                lpos = pos+1;
                nt = node->id;
                assert(nt!=0);
            }
            node = res->second;
            res = node->children.find(rules.read(pos--));
        }
        curr_rule++;
    }
}


void suffpair_long(std::string& g_file, sdsl::cache_config& config){

    plain_gram_t p_gram;
    p_gram.load_from_file(g_file);

    bv_t rep_syms(p_gram.n_rules-1, false);
    prep_input(p_gram, rep_syms, config);
    build_trie(p_gram, rep_syms, config);
}

#endif //LPG_COMPRESSOR_SUFFIX_PAIR_LONG_HPP
