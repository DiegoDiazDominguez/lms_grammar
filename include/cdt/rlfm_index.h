//
// Created by diegodiaz on 19-11-20.
//

#ifndef LPG_COMPRESSOR_RLFM_INDEX_H
#define LPG_COMPRESSOR_RLFM_INDEX_H
#include <iostream>
#include <sdsl/bit_vectors.hpp>
#include <sdsl/wavelet_trees.hpp>

template<typename seq_t=sdsl::int_vector<>>
struct rlfm_index{

    typedef size_t                    size_type;
    typedef sdsl::bit_vector          bv_t;
    typedef sdsl::int_vector<>        vector_t;
    typedef sdsl::int_vector_buffer<> vector_buff_t;

    sdsl::enc_vector<>  C;
    bv_t                bwt_run_len;
    bv_t::rank_1_type   bwt_run_len_rs;
    bv_t::select_1_type bwt_run_len_ss;

    bv_t                bwt_run_sums;
    bv_t::rank_1_type   bwt_run_sums_rs;
    bv_t::select_1_type bwt_run_sums_ss;

    seq_t               bwt_run_heads;

    struct head_iterator{
        const rlfm_index& rlfmi;

        std::vector<std::pair<size_t, size_t>> bucket_list;
        size_t head_pos, cb, first, last, next_b_freq, head_sym;

        head_iterator(const rlfm_index& rlfmi_, size_t head_pos_): rlfmi(rlfmi_), head_pos(head_pos_){
            if(head_pos<rlfmi.bwt_run_heads.size()){

                first = rlfmi.bwt_run_len_ss(head_pos+1);
                last = rlfmi.bwt_run_len_ss(head_pos+2)-1;

                head_sym = rlfmi.bwt_run_heads[head_pos];

                cb = rlfmi.bucket(first);
                next_b_freq = rlfmi.C[++cb];
                get_bucket_list();
            }else{
                head_pos = rlfmi.bwt_run_heads.size();
            }
        }

        head_iterator& operator++(){
            if(head_pos<(rlfmi.bwt_run_heads.size()-1)){
                head_sym = rlfmi.bwt_run_heads[++head_pos];
                first = last+1;
                last = rlfmi.bwt_run_len_ss(head_pos+2)-1;
                get_bucket_list();
            }else{
                head_pos = rlfmi.bwt_run_heads.size();
            }
            return *this;
        }

        void get_bucket_list(){
            bucket_list.clear();
            bool is_sum_head = rlfmi.bwt_run_sums[first];
            size_t b_s = rlfmi.bwt_run_sums_rs(first) + is_sum_head;
            size_t freq=0, prev_pos=first, next_pos=0, l_boundary;
            while(next_pos<=last){
                l_boundary = rlfmi.bwt_run_sums_ss(b_s+1);
                next_pos = std::min(last+1, l_boundary);
                freq += next_pos-prev_pos;
                if(next_pos==l_boundary) b_s++;

                prev_pos = next_pos;
                if(b_s>next_b_freq || next_pos> last){
                    bucket_list.emplace_back(freq, (cb-1));
                    if(b_s>next_b_freq && next_b_freq<rlfmi.bwt_run_heads.size()){
                        next_b_freq = rlfmi.C[++cb];
                    }
                    freq=0;
                }
            }
        }

        bool operator!=(const head_iterator& other) const{
            return (head_pos!=other.head_pos);
        }
    };

    struct range_iterator{
        const rlfm_index& rlfmi;
        size_t f,l,j, h_start, h_end, first, last, head, freq;

        range_iterator(const rlfm_index& rlfmi_, size_t first_, size_t last_): rlfmi(rlfmi_),
                                                                               first(first_),
                                                                               last(last_) {
            if(first<=last && last<(rlfmi.bwt_run_len.size()-1)){
                bool is_head = rlfmi.bwt_run_len[first];
                bool is_head2 = rlfmi.bwt_run_len[last];

                h_start = rlfmi.bwt_run_len_rs(first)+1-!is_head;
                h_end = rlfmi.bwt_run_len_rs(last)+1-!is_head2;
                f = first;
                j = h_start;

                l = std::min<size_t>(rlfmi.bwt_run_len_ss(j+1)-1, last);
                freq = l-f+1;
                head = rlfmi.bwt_run_heads[j-1];
                f = l+1;
                if(j<h_end) assert(rlfmi.bwt_run_len[f]);
                j++;
            }else{
                first = rlfmi.bwt_run_len.size();
                last = rlfmi.bwt_run_len.size();
            }
        }

        range_iterator& operator++(){
            if(j<=h_end){
                l = std::min<size_t>(rlfmi.bwt_run_len_ss(j+1)-1, last);
                freq = l-f+1;
                head = rlfmi.bwt_run_heads[j-1];
                f = l+1;
                if(j<h_end) assert(rlfmi.bwt_run_len[f]);
                j++;
            }else{
                first = rlfmi.bwt_run_len.size();
                last = rlfmi.bwt_run_len.size();
            }
            return *this;
        }

        bool operator!=(const range_iterator& other) const{
            return (other.first!=first || other.last!=last);
        }
    };

    rlfm_index()=default;

    rlfm_index(std::string& r_heads_file, std::string& r_len_file, std::string& acc_bck_file){

        {
            vector_buff_t r_heads(r_heads_file, std::ios::in);
            vector_buff_t r_len(r_len_file, std::ios::in);
            vector_t acc_bck;
            sdsl::load_from_file(acc_bck, acc_bck_file);

            assert(r_heads.size()==r_len.size());

            size_t max_freq = 0, freq;
            for (size_t i = 1; i < acc_bck.size(); i++) {
                freq = acc_bck[i] - acc_bck[i - 1];
                if (freq > max_freq) {
                    max_freq = freq;
                }
            }

            sdsl::int_vector<> sums(acc_bck.size(), 0, sdsl::bits::hi(max_freq) + 1);
            bwt_run_sums = bv_t(acc_bck[acc_bck.size() - 1] + 1, false);
            bwt_run_len = bv_t(acc_bck[acc_bck.size() - 1] + 1, false);

            size_t sym, len, pos = 0;
            for (size_t i = 0; i < r_heads.size(); i++) {
                bwt_run_len[pos] = true;
                sym = r_heads[i];
                bwt_run_sums[acc_bck[sym] + sums[sym]] = true;
                len = r_len[i];
                sums[sym] += len;
                pos += len;
            }
            bwt_run_len[bwt_run_len.size() - 1] = true;
            bwt_run_sums[bwt_run_sums.size() - 1] = true;

            //remove head copies from the accumulative array
            for (size_t i = 0; i < acc_bck.size()-1; i++) {
                acc_bck[i] = acc_bck[i+1]-acc_bck[i];
            }
            for(size_t i=0;i< r_heads.size();i++){
                acc_bck[r_heads[i]]-= (r_len[i]-1);
            }

            size_t tmp=acc_bck[0], tmp2;
            acc_bck[0] = 0;
            for(size_t j=1;j<acc_bck.size();j++){
                tmp2 = acc_bck[j];
                acc_bck[j] = tmp;
                tmp+=tmp2;
            }
            //

            assert(acc_bck[acc_bck.size()-1]==r_heads.size());
            sdsl::util::init_support(bwt_run_sums_ss, &bwt_run_sums);
            sdsl::util::init_support(bwt_run_sums_rs, &bwt_run_sums);
            sdsl::util::init_support(bwt_run_len_ss, &bwt_run_len);
            sdsl::util::init_support(bwt_run_len_rs, &bwt_run_len);

            C = sdsl::enc_vector<>(acc_bck);
        }
        build_seq(r_heads_file);
    };

    inline size_t runs() const{
        return bwt_run_heads.size();
    }

    inline std::pair<size_t, size_t> head_inv_select(size_t head_pos) const {
        return bwt_run_heads.inverse_select(head_pos);
    }

    inline size_t sym_freq(size_t sym) const{
        size_t sym_bf = C[sym];
        size_t rl_rank = C[sym+1] - sym_bf;
        return bwt_run_sums_ss(sym_bf+ rl_rank+1)-bwt_run_sums_ss(sym_bf+1);
    }

    void build_seq(std::string &seq_file);

    template <typename s_t = seq_t>
    inline typename std::enable_if<std::is_same<s_t, sdsl::wt_int<>>::value==true, size_t>::type
    rank(size_t idx, size_t sym) const {
        bool is_head = bwt_run_len[idx];
        size_t head_pos = bwt_run_len_rs(idx) - !is_head;
        size_t rl_rank = bwt_run_heads.rank(head_pos, sym);

        size_t rank = bwt_run_sums_ss(C[sym]+ rl_rank+1)-bwt_run_sums_ss(C[sym]+1);
        if(!is_head && bwt_run_heads[head_pos]==sym){
            rank += idx-bwt_run_len_ss(head_pos+1);
        }
        return rank;
    }

    inline size_t operator [] (size_t idx) const {
        bool is_head = bwt_run_len[idx];
        size_t head_pos = bwt_run_len_rs(idx) - !is_head;
        return bwt_run_heads[head_pos];
    }

    inline range_iterator range_iterator_end() const {
        return range_iterator{*this, bwt_run_len.size(), bwt_run_len.size()};
    }

    inline size_t bucket(size_t pos) const {
        bool is_head = bwt_run_sums[pos];
        size_t n_runs = bwt_run_sums_rs(pos)+is_head;
        size_t lb, rb;
        long long l=0, r=(long long)(C.size()-2L), mid;

        while(r>=l) {
            mid = l + (r - l) / 2;
            lb = C[mid], rb = C[mid+1];
            if (rb >= n_runs && lb < n_runs) {
                return mid;
            } else {
                if(n_runs<=lb){
                    r = mid - 1;
                }else{
                    l = mid + 1;
                }
            }
        }
        assert(r>=l);
        return 0;
    }

    inline std::pair<size_t, size_t> head_range(size_t head_pos) const{
        return {bwt_run_len_ss(head_pos+1), bwt_run_len_ss(head_pos+2)-1};
    }

    inline std::vector<std::tuple<size_t, size_t, size_t>> run_buckets(size_t head_pos) const{

        std::vector<std::tuple<size_t, size_t, size_t>> output;
        auto res = head_range(head_pos);
        bool is_head = bwt_run_sums[res.first];
        size_t b_s = bwt_run_sums_rs(res.first) + is_head;
        size_t bk = bucket(res.first);
        size_t bk_freq = C[++bk];
        long long prev_pos=res.first, next_pos = -1;

        size_t run = b_s, freq=0;
        while(next_pos<=(long long)res.second){
            next_pos = std::min<size_t>(res.second+1, bwt_run_sums_ss(++run));
            freq += next_pos-prev_pos;
            prev_pos = next_pos;
            if(run>bk_freq || next_pos>(long long)res.second){
                output.emplace_back(bwt_run_heads[head_pos], freq, (bk-1));
                if(bk<(C.size()-1)){
                    bk_freq = C[++bk];
                    freq=0;
                }
            }
        }
        return output;
    }

    inline range_iterator interval(size_t first, size_t last) const {
        return range_iterator{*this, first, last};
    }

    //TODO still unfinished
    /*
    inline void interval_symbols(size_t i, size_t j, size_t& k,
                                 std::vector<size_t>& cs,
                                 std::vector<size_t>& rank_c_i,
                                 std::vector<size_t>& rank_c_j) const{
        size_t f_sym=0, l_sym=0, f_diff=0, l_diff=0, prev;
        bool is_head = bwt_run_len[i];
        bool is_tail = bwt_run_len[j];

        size_t start = bwt_run_len_rs(i)-!is_head;
        size_t end = bwt_run_len_rs(j)+1;


        if(!is_head){
            f_sym = bwt_run_heads[start];
            f_diff = i - bwt_run_len_ss(start);
        }

        if(!is_tail){
            l_sym = bwt_run_heads[end-1];
            l_diff = bwt_run_len_ss(end)-j;
        }

        bwt_run_heads.interval_symbols(start, end , k,cs, rank_c_i, rank_c_j);
        for(size_t u=0;u<k;u++){
            prev = C[cs[u]];

            rank_c_i[u] = bwt_run_sums_ss(prev + 1 + rank_c_i[u])- bwt_run_sums_ss(prev+1);
            rank_c_j[u] = bwt_run_sums_ss(prev + 1 + rank_c_j[u]) - bwt_run_sums_ss(prev+1);

            if(!is_head && cs[u]==f_sym){
                rank_c_i[u]+=f_diff;
            }
            if(!is_tail && cs[u]==l_sym){
                rank_c_j[u]-=l_diff;
            }
        }
    }*/


    inline size_t alphabet_size() const {
        return C.size()-1;
    }

    inline size_t size() const{
        return bwt_run_len.size()-1;
    };

    inline head_iterator begin() const {
        return head_iterator(*this, 0);
    };

    inline head_iterator end() const {
        return head_iterator(*this, bwt_run_heads.size());
    };

    //returns a pair {lf, sym_pos}, where lf is the LF value and
    // sym_pos is the symbol at position pos
    inline std::pair<size_t,size_t> lf(size_t pos) const{
        bool is_head = bwt_run_len[pos];
        size_t head_pos = bwt_run_len_rs(pos) - !is_head;
        auto is = bwt_run_heads.inverse_select(head_pos);
        size_t new_pos = bwt_run_sums_ss(C[is.second]+ is.first+1);
        if(!is_head){
            new_pos += pos-bwt_run_len_ss(head_pos+1);
        }
        return {new_pos, is.second};
    }

    //returns a pair {freq, BWT[pos]}
    inline std::pair<size_t, size_t> inv_select(size_t pos) const {
        bool is_head = bwt_run_len[pos];
        size_t head_pos = bwt_run_len_rs(pos) - !is_head;
        auto is = bwt_run_heads.inverse_select(head_pos);
        size_t freq = bwt_run_sums_ss(C[is.second]+ is.first+1)-bwt_run_sums_ss(C[is.second]+1);
        if(!is_head) freq += pos-bwt_run_len_ss(head_pos+1);
        return {freq, is.second};
    }

    size_type serialize(std::ostream &out, sdsl::structure_tree_node *v, std::string name) const {
        sdsl::structure_tree_node *child = sdsl::structure_tree::add_child(v, name, sdsl::util::class_name(*this));
        size_t written_bytes = 0;
        written_bytes += C.serialize(out, child, "C");
        written_bytes += bwt_run_len.serialize(out, child, "len");
        written_bytes += bwt_run_len_rs.serialize(out, child, "len_rs");
        written_bytes += bwt_run_len_ss.serialize(out, child, "len_ss");
        written_bytes += bwt_run_sums.serialize(out, child, "sums");
        written_bytes += bwt_run_sums_rs.serialize(out, child, "sums_rs");
        written_bytes += bwt_run_sums_ss.serialize(out, child, "sums_ss");
        written_bytes += bwt_run_heads.serialize(out, child, "heads");
        return written_bytes;
    }

    void load(std::istream &in) {
        C.load(in);
        bwt_run_len.load(in);
        bwt_run_len_rs.load(in);
        bwt_run_len_ss.load(in);
        bwt_run_sums.load(in);
        bwt_run_sums_rs.load(in);
        bwt_run_sums_ss.load(in);
        bwt_run_heads.load(in);

        bwt_run_len_rs.set_vector(&bwt_run_len);
        bwt_run_len_ss.set_vector(&bwt_run_len);

        bwt_run_sums_rs.set_vector(&bwt_run_sums);
        bwt_run_sums_ss.set_vector(&bwt_run_sums);
    }
};

template <>
void rlfm_index<sdsl::wt_int<>>::build_seq(std::string &seq_file) {
    sdsl::construct(bwt_run_heads, seq_file, 0);
}

template <>
void rlfm_index<sdsl::int_vector<>>::build_seq(std::string &seq_file) {
    sdsl::load_from_file(bwt_run_heads, seq_file);
}
#endif //LPG_COMPRESSOR_RLFM_INDEX_H
