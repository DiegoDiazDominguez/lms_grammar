//
// Created by diego on 12-07-20.
//
#include <sdsl/int_vector.hpp>

#ifndef LPG_COMPRESSOR_SIMPLE_QUEUE_HPP
#define LPG_COMPRESSOR_SIMPLE_QUEUE_HPP

class simple_int_queue{
private:
    long head,tail,max_size;
    sdsl::int_vector<> container;

public:
    simple_int_queue(size_t size, size_t max_value): head(0), tail(-1){
        max_size = size;
        container = sdsl::int_vector<>(max_size, 0, sdsl::bits::hi(max_value)+1);
    }

    inline bool empty() const{
        return tail<head;
    }

    inline size_t size() const{
        if(tail<head){
            return 0;
        }else{
            return (tail-head+1);
        }
    }

    inline void push(size_t val){
        assert((tail-head+1)<max_size);
        if(tail==(max_size-1) && head<=tail){//shift the elements to the beginning
            long pos =0;
            for(long i=head;i<=tail;i++){
                container[pos++] = container[i];
            }
            head = 0;
            tail = pos-1;
        }
        container[++tail]=val;
    }

    inline size_t front() const{
        assert(head<=tail);
        return container[head];
    }

    inline void pop(){
        if(head<(tail+1)){
            head++;
        }
    }

    inline void clear(){
        sdsl::util::clear(container);
    }
};

#endif //LPG_COMPRESSOR_SIMPLE_QUEUE_HPP
