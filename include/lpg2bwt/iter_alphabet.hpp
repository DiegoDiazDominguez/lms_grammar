//
// Created by diegodiaz on 03-12-20.
//

#ifndef LPG_COMPRESSOR_ITER_ALPHABET_H
#define LPG_COMPRESSOR_ITER_ALPHABET_H

struct iter_alph {

    struct ldc_buffer{
        size_t label;
        size_t node;
    };

    typedef sdsl::bit_vector bit_vector_t;
    typedef sdsl::int_vector_buffer<> buffer_t;
    typedef sdsl::int_vector<> vector_t;
    typedef size_t size_type;

    vector_t ranks;
    bit_vector_t alph;
    bit_vector_t::rank_1_type alph_rs;
    bit_vector_t::select_1_type alph_ss;
    bool inverted{};
    size_t miss=0;
    size_t hit=0;

    void move(iter_alph&& other){
        ranks.swap(other.ranks);
        alph.swap(other.alph);
        std::swap(inverted, other.inverted);
        alph_rs.swap(other.alph_rs);
        alph_ss.swap(other.alph_ss);
        if(!inverted){
            alph_rs.set_vector(&alph);
        }else{
            alph_ss.set_vector(&alph);
        }
    }

    void copy(const iter_alph& other){
        ranks = other.ranks;
        alph = other.alph;
        inverted = other.inverted;
        alph_rs = other.alph_rs;
        alph_ss = other.alph_ss;
        if(!inverted){
            alph_rs.set_vector(&alph);
        }else{
            alph_ss.set_vector(&alph);
        }
    }

    iter_alph() = default;

    iter_alph(size_t sigma, size_t tot_labels) : alph(tot_labels+1, false) {
        ranks = sdsl::int_vector<>(sigma, 0, sdsl::bits::hi(sigma) + 1);
        for (size_t i = 0; i < sigma; i++) {
            alph[i] = true;
            ranks[i] = i;
        }
        sdsl::util::init_support(alph_rs, &alph);
        inverted = false;
    };

    //move constructor
    iter_alph(iter_alph &&other) noexcept {
        move(std::forward<iter_alph>(other));
    }

    //copy constructor
    iter_alph(const iter_alph &other) noexcept {
        copy(other);
    }

    void invert_alphabet(sdsl::cache_config& config){
        assert(!inverted);
        std::string tmp_file = sdsl::cache_file_name("alph_ranks", config);
        sdsl::store_to_file(ranks, tmp_file);
        sdsl::util::set_to_value(ranks, 0);
        sdsl::int_vector_buffer<> ranks_buff(tmp_file, std::ios::in);
        size_t k=0;
        for(auto const& elm : ranks_buff){
            ranks[elm] = k++;
        }
        ranks_buff.close(true);
        sdsl::util::init_support(alph_ss, &alph);
        inverted = true;
    }

    inline size_t in_alph(size_t g_symbol) const {
        return alph[g_symbol];
    }

    inline size_t label2rank(size_t g_symbol) const {
        assert(!inverted && alph[g_symbol]);
        return ranks[alph_rs(g_symbol)];
    }

    inline size_t rank2label(size_t rank) const {
        assert(inverted);
        return alph_ss(ranks[rank]+1);
    }

    inline size_t size() const {
        return ranks.size();
    }

    iter_alph &operator=(iter_alph &&other) noexcept {
        move(std::forward<iter_alph>(other));
        return *this;
    }

    template<typename grammar_t>
    inline void p_decomp(const grammar_t& g, size_t node_map, size_t res[], size_t& len) const {

        size_t node = g.tree.node_select(node_map);
        size_t label = g.label(node);
        len = 0;

        if(!in_alph(label)){
            if(g.tree.is_leaf(node)){
                node = g.tree.int_select(label-g.sigma);
            }
            node = g.tree.child(node, 1);
            label = g.label(node);
            assert(in_alph(label));
        }else if(g.tree.parent(node)==grammar_t::root){
            res[len++] = label2rank(label);
            return;
        }

        size_t n_rsibs = g.tree.n_rsibs(node);

        while(true){
            res[len++] = label2rank(label);
            if(n_rsibs==0) return;

            node = g.tree.succ_0(node)+1;//next sibling
            label = g.label(node);
            n_rsibs--;

            if(!in_alph(label)){
                if(g.tree.is_leaf(node)){
                    node = g.tree.int_select(label-g.sigma);
                }
                node = g.tree.child(node, 1);
                label = g.label(node);
                assert(in_alph(label));
                n_rsibs = g.tree.n_rsibs(node);
            }
        }
    }

    template<typename grammar_t>
    inline size_t access_sym(const grammar_t& g, size_t node_map, size_t idx) const {

        size_t node = g.tree.node_select(node_map);
        size_t label = g.label(node);
        size_t pos=0;

        if(!in_alph(label)){
            if(g.tree.is_leaf(node)){
                node = g.tree.int_select(label-g.sigma);
            }
            node = g.tree.child(node, 1);
            label = g.label(node);
            assert(in_alph(label));
        }

        while(true){
            if(pos==idx){
                return label2rank(label);
            }else{
                node = g.tree.n_sibling(node);
                assert(node!=0);
                label = g.label(node);
                if(!in_alph(label)){
                    if(g.tree.is_leaf(node)){
                        node = g.tree.int_select(label-g.sigma);
                    }
                    node = g.tree.child(node, 1);
                    label = g.label(node);
                    assert(in_alph(label));
                }
                pos++;
            }
        }
    }

    template<typename grammar_t>
    inline bool compare_phrases(const grammar_t &g,  const size_t l, const size_t r) const {
        if (l == r) return false;

        bool l_unf = true, r_unf = true;
        size_t lab_l, lab_r, len_l = 1, len_r = 1, l_node, r_node;

        l_node = g.tree.node_select(l);
        r_node = g.tree.node_select(r);

        lab_l = g.label(l_node);
        lab_r = g.label(r_node);

        while (l_unf && r_unf) {
            if (!in_alph(lab_l)) {
                if(g.tree.is_leaf(l_node)){
                    l_node = g.tree.int_select(lab_l-g.sigma);
                }
                l_node = g.tree.child(l_node, 1);
                lab_l = g.label(l_node);
                assert(in_alph(lab_l));
            }
            //std::cout<<l_node<<","<<lab_l<<" -- ";
            len_l++;

            if (!in_alph(lab_r)) {
                if(g.tree.is_leaf(r_node)){
                    r_node = g.tree.int_select(lab_r - g.sigma);
                }
                r_node = g.tree.child(r_node, 1);
                lab_r = g.label(r_node);
                assert(in_alph(lab_r));
            }
            len_r++;
            //std::cout<<r_node<<","<<lab_r<<std::endl;

            if (lab_l != lab_r) {
                return label2rank(lab_l) < label2rank(lab_r);
            } else {
                l_node = g.tree.n_sibling(l_node);
                if (l_node == 0) {
                    l_unf = false;
                    len_l--;
                } else {
                    lab_l = g.label(l_node);
                }

                r_node = g.tree.n_sibling(r_node);
                if (r_node == 0) {
                    r_unf = false;
                    len_r--;
                } else {
                    lab_r = g.label(r_node);
                }
            }
        }
        return len_l > len_r;
    }

    template<typename grammar_t>
    inline bool buff_comp_phrases(const grammar_t &g,
                                  const size_t l, const size_t r,
                                  ldc_buffer l_ldc_data[], size_t& l_size,
                                  ldc_buffer r_ldc_data[], size_t& r_size) const {
        if (l == r) return false;

        size_t lab_l, lab_r, len_l = 1, len_r = 1, l_node, r_node;

        if(l_size==0){
            l_node = g.tree.node_select(l);
            lab_l = g.label(l_node);
            l_ldc_data[l_size++] = {lab_l, l_node};
        }else{
            l_node = l_ldc_data[0].node;
            lab_l = l_ldc_data[0].label;
        }

        if(r_size==0){
            r_node = g.tree.node_select(r);
            lab_r = g.label(r_node);
            r_ldc_data[r_size++] = {lab_r, r_node};
        }else{
            r_node = r_ldc_data[0].node;
            lab_r = r_ldc_data[0].label;
        }

        while (true) {

            if (!in_alph(lab_l)) {
                if(g.tree.is_leaf(l_node)){
                    l_node = g.tree.int_select(lab_l-g.sigma);
                }
                l_node = g.tree.child(l_node, 1);
                lab_l = g.label(l_node);

                l_ldc_data[l_size-1] = {lab_l, l_node};
                assert(in_alph(lab_l));
            }
            len_l++;

            if (!in_alph(lab_r)) {
                if(g.tree.is_leaf(r_node)){
                    r_node = g.tree.int_select(lab_r - g.sigma);
                }
                r_node = g.tree.child(r_node, 1);
                lab_r = g.label(r_node);

                r_ldc_data[r_size-1] = {lab_r, r_node};
                assert(in_alph(lab_r));
            }
            len_r++;

            if (lab_l != lab_r) {
                return label2rank(lab_l) < label2rank(lab_r);
            } else {

                if(len_l<=l_size){
                    l_node = l_ldc_data[len_l-1].node;
                    lab_l = l_ldc_data[len_l-1].label;
                }else{
                    l_node = g.tree.n_sibling(l_node);
                    if (l_node == 0) {
                        len_l--;
                        break;
                    } else {
                        lab_l = g.label(l_node);
                        l_ldc_data[len_l-1] = {lab_l, l_node};
                        l_size = len_l;
                    }
                }

                if(len_r<=r_size){
                    r_node = r_ldc_data[len_r-1].node;
                    lab_r = r_ldc_data[len_r-1].label;
                }else{
                    r_node = g.tree.n_sibling(r_node);
                    if (r_node == 0) {
                        len_r--;
                        break;
                    } else {
                        lab_r = g.label(r_node);
                        r_ldc_data[len_r-1] = {lab_r, r_node};
                        r_size = len_r;
                    }
                }
            }
        }
        return len_l > len_r;
    }

    size_type serialize(std::ostream &out, sdsl::structure_tree_node *v, std::string name) const {
        sdsl::structure_tree_node *child = sdsl::structure_tree::add_child(v, name, sdsl::util::class_name(*this));
        size_t written_bytes = 0;
        written_bytes += alph.serialize(out, child, "alph");
        written_bytes += ranks.serialize(out, child, "ranks");
        written_bytes += sdsl::write_member(inverted, out, child, "inverted");
        if(!inverted){
            written_bytes += alph_rs.serialize(out, child, "alph_rs");
        }else{
            written_bytes += alph_ss.serialize(out, child, "alph_ss");
        }
        return written_bytes;
    }

    void load(std::istream &in) {
        alph.load(in);
        ranks.load(in);
        sdsl::read_member(inverted, in);
        if(!inverted){
            alph_rs.load(in);
            alph_rs.set_vector(&alph);
        }else{
            alph_ss.load(in);
            alph_ss.set_vector(&alph);
        }
    }
};

#endif //LPG_COMPRESSOR_ITER_ALPHABET_H
