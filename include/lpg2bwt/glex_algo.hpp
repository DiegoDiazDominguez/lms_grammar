//
// Created by diego on 13-07-20.
//

#ifndef LPG_COMPRESSOR_GLEX_ALGO_HPP
#define LPG_COMPRESSOR_GLEX_ALGO_HPP

#include <cdt/parallel_string_sort.hpp>
#include "cdt/macros.h"
#include "iter_alphabet.hpp"

template<class grammar_t>
class glex {
    typedef sdsl::bit_vector bit_vector_t;
    typedef sdsl::int_vector_buffer<> buffer_t;

private:

    struct infer_nodes_thread{
        const size_t start;
        const size_t end;
        const grammar_t &g;
        const iter_alph &alph;
        const bit_vector_t& phrases_loci;

        sdsl::int_vector_buffer<> sorted_suff;
        std::string inf_nodes_file;
        sdsl::int_vector_buffer<> inf_nodes;

        infer_nodes_thread(size_t start_, size_t end_,
                           const grammar_t& g_, const iter_alph& alph_,
                           std::string& sorted_suff_file, std::string& pref,
                           const bit_vector_t& phrases_loci_): start(start_),
                                                               end(end_),
                                                               g(g_),
                                                               alph(alph_),
                                                               phrases_loci(phrases_loci_){
            sorted_suff = sdsl::int_vector_buffer<>(sorted_suff_file, std::ios::in);
            inf_nodes_file = pref+"/inf_nodes_"+std::to_string(start)+"_"+std::to_string(end);
            inf_nodes = sdsl::int_vector_buffer<>(inf_nodes_file, std::ios::out);
        }

        void operator()(){
            size_t node, label;
            for(size_t i=start;i<=end;i++){
                node = g.tree.node_select(sorted_suff[i]);
                label = g.label(node);
                assert(alph.in_alph(label));
                if(phrases_loci[label]){
                    inf_nodes.push_back(label);
                }else if(g.tree.is_lm_child(node)){
                    label = g.label(g.tree.parent(node));
                    if(phrases_loci[label]){
                        inf_nodes.push_back(label);
                    }
                }
            }
            inf_nodes.close();
        }
    };

    static void *infer_p(void *data){
        auto t_data = reinterpret_cast<infer_nodes_thread*>(data);
        (*t_data)();
        pthread_exit(nullptr);
    };

    struct glex_thread_data {
        const size_t start;
        const size_t end;
        const grammar_t &g;
        const iter_alph &alph;
        const bit_vector_t &masked_nt;

        size_t inf_phrases = 0;
        bit_vector_t phrase_loci;
        bit_vector_t new_masked_nt;

        std::string tr_syms_file;
        sdsl::int_vector_buffer<> tr_sym_buff;
        std::string ssym_file;
        sdsl::int_vector_buffer<> ssym_buff;
        std::string fsym_file;
        sdsl::int_vector_buffer<> fsym_buff;

        glex_thread_data(size_t start_, size_t end_,
                         const grammar_t &g_, iter_alph &alph_,
                         bit_vector_t &masked_nt_,
                         std::string dir ) : start(start_),
                                             end(end_),
                                             g(g_),
                                             alph(alph_),
                                             masked_nt(masked_nt_),
                                             phrase_loci(end - start + 1, false),
                                             new_masked_nt(end - start + 1, false) {

            size_t buff_size = std::min<size_t>((end - start + 1) * (sdsl::bits::hi(end) + 1), BUFFER_SIZE);

            tr_syms_file = dir+"/tr_syms_range_"+std::to_string(start)+"_"+std::to_string(end);
            tr_sym_buff = sdsl::int_vector_buffer<>(tr_syms_file, std::ios::out, buff_size);

            ssym_file = dir+"/ssyms_range_"+std::to_string(start)+"_"+std::to_string(end);
            ssym_buff = sdsl::int_vector_buffer<>(ssym_file, std::ios::out);
            fsym_file = dir+"/fsyms_range_"+std::to_string(start)+"_"+std::to_string(end);
            fsym_buff = sdsl::int_vector_buffer<>(fsym_file, std::ios::out);
        };

        void operator()(){

            bool is_lmsg, is_sp;
            size_t tmp_node, child, child_lab, psib, psib_lab;
            size_t top_int_nodes = g.tree.n_ints(g.tree.child(grammar_t::root, g.tree.n_children(grammar_t::root)));

            size_t n_rsib=0, node_map;

            for(size_t i=start, k=0;i<=end;i++,k++){
                if(masked_nt[i]) continue;

                tmp_node = g.tree.int_select(i);
                is_lmsg = false;
                is_sp = false;

                //check if the node is the locus
                // of an LMSg phrase
                child = g.tree.child(tmp_node, 1);
                child_lab = g.label(child);

                if(alph.in_alph(child_lab)){
                    if(i<=top_int_nodes){//child of the root
                        is_lmsg = true;
                    }else{
                        if(!g.tree.is_lm_child(tmp_node)){
                            psib = g.tree.p_sibling(tmp_node);
                            psib_lab = g.label(psib);
                            if(!alph.in_alph(psib_lab)){
                                is_lmsg = true;
                            }else{
                                is_sp = true;
                                new_masked_nt[k] = true;
                                ssym_buff.push_back(g.tree.node_map(child));
                                fsym_buff.push_back(alph.label2rank(child_lab));
                            }
                        }else{
                            is_lmsg = true;
                        }
                    }
                }

                if(is_lmsg){
                    phrase_loci[k] = true;
                    new_masked_nt[k] = true;
                    inf_phrases++;

                    n_rsib=g.tree.n_rsibs(child);

                    node_map = g.tree.node_map(child);
                    ssym_buff.push_back(node_map);
                    fsym_buff.push_back(alph.label2rank(child_lab));

                    for(size_t j=0;j<n_rsib-1;j++){
                        child = g.tree.succ_0(child)+1;//right sibling
                        child_lab = g.label(child);
                        ssym_buff.push_back(++node_map);
                        fsym_buff.push_back(alph.label2rank(child_lab));
                    }

                }else if(!is_sp){
                    size_t n_children = g.tree.n_children(tmp_node);
                    for(size_t j=n_children;j>=1;j--){
                        child = g.tree.child(tmp_node, j);
                        child_lab = g.label(child);
                        if(alph.in_alph(child_lab)){
                            tr_sym_buff.push_back(child_lab);
                        }else{
                            break;
                        }
                    }
                }
            }

            tr_sym_buff.close(false);

            ssym_buff.close(false);
            fsym_buff.close(false);
        }
    };


    static void *glex_p(void *data){
        auto t_data = reinterpret_cast<glex_thread_data*>(data);
        (*t_data)();
        pthread_exit(nullptr);
    };

    /*static bool compare_phrases(const grammar_t &g, const iter_alph& alph, size_t l, size_t r) {

        if (l == r) return false;

        bool l_unf = true;
        bool r_unf = true;
        size_t lab_l, lab_r, len_l = 1, len_r = 1, l_node, r_node;

        l_node = g.tree.child(g.tree.int_select(l), 2);
        r_node = g.tree.child(g.tree.int_select(r), 2);

        lab_l = g.label(l_node);
        lab_r = g.label(r_node);

        while (l_unf && r_unf) {
            if (alph.in_alph(lab_l)) {
                if (lab_l > g.sigma && (lab_l - g.sigma) == l) l_unf = false;
            } else {
                l_node = g.tree.child(g.tree.int_select(lab_l-g.sigma), 1);
                lab_l = g.label(l_node);
            }
            assert(alph.in_alph(lab_l));
            len_l++;

            if (alph.in_alph(lab_r)) {
                if (lab_r > g.sigma && (lab_r - g.sigma) == r) r_unf = false;
            } else {
                r_node = g.tree.child(g.tree.int_select(lab_r - g.sigma), 1);
                lab_r = g.label(r_node);
            }
            assert(alph.in_alph(lab_r));
            len_r++;
            if (lab_l != lab_r) {
                return alph.label2rank(lab_l) < alph.label2rank(lab_r);
            } else {
                l_node = g.tree.n_sibling(l_node);
                if (l_node == 0) {
                    l_unf = false;
                    len_l--;
                } else {
                    lab_l = g.label(l_node);
                }
                r_node = g.tree.n_sibling(r_node);
                if (r_node == 0) {
                    r_unf = false;
                    len_r--;
                } else {
                    lab_r = g.label(r_node);
                }
            }
        }
        return len_l > len_r;
    }*/

    static void compute_ranks(const grammar_t& g,
                              bit_vector_t& phrases_loci,
                              iter_alph& alph,
                              size_t n_threads,
                              size_t iter,
                              sdsl::cache_config& config){

        std::cout<<"  Computing the ranks of the inferred phrases"<<std::endl;

        std::string ssym_file = sdsl::cache_file_name("ssyms_iter_"+std::to_string(iter), config);
        std::string fsym_file = sdsl::cache_file_name("fsyms_iter_"+std::to_string(iter), config);

        auto compare = [&](const size_t &l, const size_t &r) -> bool {
            return alph.compare_phrases(g, l+1, r+1);
        };
        auto access = [&](const size_t &node_map, size_t idx) -> size_t {
            return alph.access_sym(g, node_map, idx);
        };
        parallel_str_sort(ssym_file, compare, access, alph.size(), n_threads, config, fsym_file);

        size_t tot_suff;
        {
            sdsl::int_vector_buffer<> tmp(ssym_file, std::ios::in);
            tot_suff = tmp.size();
        }
        size_t suff_per_thread = INT_CEIL(tot_suff, n_threads);
        std::vector<infer_nodes_thread> threads_data;

        for (size_t i = 0; i < n_threads; i++) {
            size_t start = i * suff_per_thread;
            size_t end = std::min((i + 1) * suff_per_thread, tot_suff)-1;
            threads_data.emplace_back(start, end, g, alph, ssym_file, config.dir, phrases_loci);
        }

        std::vector<pthread_t> threads(threads_data.size());
        for (size_t i = 0; i < threads_data.size(); i++) {
            int ret = pthread_create(&threads[i],
                                     nullptr,
                                     &infer_p,
                                     (void *) &threads_data[i]);
            if (ret != 0) {
                printf("Error: pthread_create() failed\n");
                exit(EXIT_FAILURE);
            }
        }
        for (size_t i = 0; i < threads_data.size(); i++) {
            pthread_join(threads[i], nullptr);
        }

        /*size_t node, label;
        std::string nodes_file = sdsl::cache_file_name("inferred_nodes", config);
        sdsl::int_vector_buffer<> sorted_nodes(nodes_file, std::ios::out);
        for(auto const& sym : sorted_buff){
            node = g.tree.node_select(sym);
            label = g.label(node);
            assert(alph.in_alph(label));
            if(phrases_loci[label]){
                sorted_nodes.push_back(label);
            }else if(g.tree.is_lm_child(node)){
                label = g.label(g.tree.parent(node));
                if(phrases_loci[label]){
                    sorted_nodes.push_back(label);
                }
            }
        }*/

        std::string alph_file= sdsl::cache_file_name("alph_iter_"+std::to_string(iter), config);
        sdsl::store_to_file(alph, alph_file);
        sdsl::util::clear(alph.ranks);
        sdsl::util::clear(alph.alph_rs);

        bit_vector_t::rank_1_type new_map;
        sdsl::util::init_support(new_map, &phrases_loci);
        size_t tot_inf = new_map(phrases_loci.size());
        sdsl::int_vector<> new_ranks(tot_inf, 0, sdsl::bits::hi(tot_inf) + 1);

        size_t rank=0;
        for(auto const& data: threads_data){
            sdsl::int_vector_buffer<> inf_nodes(data.inf_nodes_file, std::ios::in);
            for(auto const& lab : inf_nodes){
                new_ranks[new_map(lab)]=rank++;
            }
            inf_nodes.close(true);
        }

        alph.alph.swap(phrases_loci);
        alph.alph_rs.swap(new_map);
        alph.ranks.swap(new_ranks);
        sdsl::util::set_to_value(phrases_loci, 0);
    }

public:
    static size_t infer_ranks(const grammar_t &g, size_t &n_threads, sdsl::cache_config &config){

        size_t iter=1;
        bit_vector_t phrase_loci(g.tree.int_nodes() + g.sigma + 1, false);
        iter_alph alph(g.sigma, g.tree.int_nodes()+g.sigma);

        size_t n_phrases = g.sigma;
        bit_vector_t masked_nt(g.tree.int_nodes()+1, false);
        size_t top_int_nodes = g.tree.n_ints(g.tree.child(grammar_t::root, g.tree.n_children(grammar_t::root)));
        size_t r = g.tree.int_nodes();
        size_t rem_nodes=r;//remaining nodes to infer (we do not consider the root)

        size_t start, end, nodes_per_thread, alph_size=g.sigma;
        std::string sym_freq_file = sdsl::cache_file_name("sym_freq_file", config);

        while(n_phrases>0){
            std::cout<<"  Inferring the phrases of iteration "<<iter<<" of LMSg"<<std::endl;
            nodes_per_thread = INT_CEIL(rem_nodes, n_threads);
            std::vector<glex_thread_data> threads_data;
            if(n_phrases==g.sigma){
                for (size_t i = 0; i < n_threads; i++) {
                    start = i * nodes_per_thread + 1;
                    end = std::min((i + 1) * nodes_per_thread, r);
                    if (i == 0) start++;//skip the root node
                    threads_data.emplace_back(start, end, g, alph, masked_nt, config.dir);
                }
            }else{
                size_t k =0, u=2;
                start = 2;
                while(u<masked_nt.size()){
                    if(!masked_nt[u]) k++;
                    if(k==nodes_per_thread || (u==masked_nt.size()-1)){
                        end = u;
                        k=0;
                        threads_data.emplace_back(start, end, g, alph, masked_nt, config.dir+"/");
                        start = u+1;
                    }
                    u++;
                }
            }

            std::vector<pthread_t> threads(threads_data.size());
            for (size_t i = 0; i < threads_data.size(); i++) {
                int ret = pthread_create(&threads[i],
                                         nullptr,
                                         &glex_p,
                                         (void *) &threads_data[i]);
                if (ret != 0) {
                    printf("Error: pthread_create() failed\n");
                    exit(EXIT_FAILURE);
                }
            }

            for (size_t i = 0; i < threads_data.size(); i++) {
                pthread_join(threads[i], nullptr);
            }

            //TODO this is new
            std::string ssym_file = sdsl::cache_file_name("ssyms_iter_"+std::to_string(iter), config);
            std::string fsym_file = sdsl::cache_file_name("fsyms_iter_"+std::to_string(iter), config);
            sdsl::int_vector_buffer<> ssym_buff(ssym_file, std::ios::out);
            //

            size_t n_tr = 0;
            {
                sdsl::int_vector<> fsym_freqs(alph_size+1, 0, sdsl::bits::hi(g.tree.nodes()) + 1);

                //treat nodes at depth one as transferred
                for (size_t i = g.sigma; i <= top_int_nodes + g.sigma; i++) {
                    if (alph.in_alph(i)) {
                        phrase_loci[i] = true;
                        n_tr++;
                        assert(fsym_freqs[alph.label2rank(i)] == 0);
                        fsym_freqs[alph.label2rank(i)]++;
                        ssym_buff.push_back(g.tree.node_map(g.tree.int_select(i-g.sigma)));
                    }
                }

                //gather thread data
                size_t k = g.sigma + 2, k2 = 2;
                n_phrases = 0;
                for (auto const &data : threads_data) {

                    //transferred symbols
                    size_t buff_size = std::min<size_t>((data.end - data.start + 1) * (sdsl::bits::hi(end) + 1),
                                                        BUFFER_SIZE);
                    sdsl::int_vector_buffer<> tr_symbols(data.tr_syms_file, std::ios::in, buff_size);
                    for (auto const &sym : tr_symbols) {
                        if (!phrase_loci[sym]){
                            size_t rank = alph.label2rank(sym);
                            assert(fsym_freqs[rank]==0);
                            fsym_freqs[rank]++;

                            n_tr++;
                            ssym_buff.push_back(g.tree.node_map(g.tree.int_select(sym-g.sigma)));
                            phrase_loci[sym] = true;
                        }
                    }
                    tr_symbols.close(true);

                    //new phrases
                    for (unsigned long long bit : data.phrase_loci) {
                        if (bit)  phrase_loci[k] = true;
                        k++;
                    }

                    //new masked symbols
                    for (unsigned long long bit : data.new_masked_nt) {
                        if (bit) {
                            masked_nt[k2] = true;
                            rem_nodes--;
                        }
                        k2++;
                    }
                    n_phrases += data.inf_phrases;

                    //suffix phrases
                    sdsl::int_vector_buffer<> tmp_ssym(data.ssym_file, std::ios::in);
                    for(auto const& ssym : tmp_ssym) ssym_buff.push_back(ssym);
                    tmp_ssym.close(true);
                    sdsl::int_vector_buffer<> tmp_fsym(data.fsym_file, std::ios::in);
                    for(auto const& fsym : tmp_fsym) fsym_freqs[fsym]++;
                    tmp_fsym.close(true);
                    //
                }
                sdsl::store_to_file(fsym_freqs, fsym_file);
                ssym_buff.close(false);
            }
            compute_ranks(g, phrase_loci, alph, n_threads, iter, config);

            std::cout<<"  Iter stats: "<<std::endl;
            std::cout<<"    LMSg phrases:        "<<n_phrases<<std::endl;
            std::cout<<"    Transferred symbols: "<<n_tr<<std::endl;
            alph_size = n_phrases+n_tr;
            iter++;
        }
        return iter;
    };
};

//helper functions just for debugging purposes
/*
static void decompress_nt_node_int(const lpg& g, size_t node_id, alphabet& alph, std::vector<int>& res){

    if(g.tree.is_leaf(node_id)){
        size_t p_val = g.l_labels[g.tree.leaf_rank(node_id) - 1];
        assert(p_val>g.sigma);
        node_id = g.tree.node_select(p_val - g.sigma+1);
    }

    size_t int_rank = g.tree.int_rank(node_id)-1;
    if(alph.alph[int_rank]){
        std::cout << "this is leaf : id=" << node_id << " - int_rank=" <<int_rank+1 << " map=" << g.tree.node_map(node_id+1) << " ->" << alph.ranks[alph.alph_rs(int_rank)] << " " << alph.is_tr(
                int_rank + 1) << std::endl;
        res.push_back(alph.ranks[alph.alph_rs(int_rank)]);
    }else{
        size_t n_children = g.tree.n_children(node_id);

        std::cout<<"this is int : "<<node_id<<" - "<<int_rank+1<<std::endl;
        for(size_t i=1;i<=n_children;i++){
            decompress_nt_node_int(g, g.tree.child(node_id, i), alph, res);
        }
    }
}
static std::vector<int> decompress_nt_node(const lpg& g, size_t node_id, alphabet& alph){
    std::vector<int> res;
    decompress_nt_node_int(g, node_id, alph, res);
    return res;
}
static void print_nt_sa(lpg& g, ind_table& table, alphabet& alph){

    size_t curr_bucket = 1;
    size_t next_bucket_start = table.bck_limit(curr_bucket + 1);

    for(size_t i=0;i<table.sa.size();i++){

        if(i == next_bucket_start){
            curr_bucket++;
            next_bucket_start = table.bck_limit(curr_bucket + 1);
        }

        std::cout<<curr_bucket<<" - "<<i<<" - "<<table.sa[i]<<"\t:\t";

        if(table.sa[i]!=0){
            auto seq = decompress_nt_node(g, g.tree.node_select(table.sa[i]), alph);
            for(auto const& sym : seq){
                std::cout<<sym<<" ";
            }
            if(seq.size()==1 && g.tree.parent(g.tree.node_select(table.sa[i]))!=lpg::root){
                size_t node = g.tree.n_sibling(g.tree.node_select(table.sa[i]));
                while(node!=0){
                    seq = decompress_nt_node(g, node, alph);
                    for(auto const& sym : seq){
                        std::cout<<sym<<" ";
                    }
                    node = g.tree.n_sibling(node);
                }
            }
            std::cout<<" "<<std::endl;
        }else{
            std::cout<<""<<std::endl;
        }
    }
}
static void print_suffix_array(lpg& g, ind_table& table){

    size_t curr_bucket = 1;
    size_t next_bucket_start = table.bck_limit(curr_bucket + 1);

    for(size_t i=0;i<table.sa.size();i++){
        if(i == next_bucket_start){
            curr_bucket++;
            next_bucket_start = table.bck_limit(curr_bucket + 1);
        }

        std::cout<<curr_bucket<<" - "<<i<<" - "<<table.sa[i]<<"\t:\t";
        if(table.sa[i]!=0){
            std::string seq = g.decompress_node(g.tree.node_select(table.sa[i]));
            std::cout<<seq;
            if(seq.size()==1){
                size_t node = g.tree.n_sibling(g.tree.node_select(table.sa[i]));
                while(node!=0){
                    std::cout<<g.decompress_node(node);
                    node = g.tree.n_sibling(node);
                }
            }
            std::cout<<""<<std::endl;
        }else{
            std::cout<<""<<std::endl;
        }
    }
}
static void get_nt_shape_int(const lpg& g, size_t node_id, alphabet& alph, std::string& res){

    if(g.tree.is_leaf(node_id)){
        size_t p_val = g.l_labels[g.tree.leaf_rank(node_id) - 1];
        assert(p_val>g.sigma);
        node_id = g.tree.node_select(p_val - g.sigma);
    }

    size_t int_rank = g.tree.int_rank(node_id)-1;
    if(alph.alph[int_rank]){
        res.push_back('(');
        res.push_back(')');
    }else{
        size_t n_children = g.tree.n_children(node_id);
        res.push_back('(');
        for(size_t i=1;i<=n_children;i++){
            get_nt_shape_int(g, g.tree.child(node_id, i), alph, res);
        }
        res.push_back(')');
    }
}
static std::string get_nt_shape(const lpg& g, size_t node_id, alphabet& alph){
    std::string res;
    get_nt_shape_int(g, node_id, alph, res);
    return res;
}*/

#endif //LPG_COMPRESSOR_GLEX_ALGO_HPP
