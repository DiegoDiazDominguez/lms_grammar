//
// Created by diego on 22-04-20.
//

#ifndef LPG_COMPRESSOR_PROTBWT_ALGO_HPP
#define LPG_COMPRESSOR_PROTBWT_ALGO_HPP

#include "iter_alphabet.hpp"
#include "cdt/rlfm_index.h"

template<class grammar_t>
class protbwt_algo {

    typedef sdsl::bit_vector                           bit_vector_t;
    typedef sdsl::int_vector_buffer<>                  buffer_t;
    typedef sdsl::int_vector<>                         vector_t;
    typedef struct iter_alph                           iter_alph_t;

public:

    static bit_vector_t get_suffix_nodes(const grammar_t &g){
        size_t tmp_node, label, int_rank;
        std::ptrdiff_t root_children = g.tree.n_children(grammar_t::root);

        //!note: we need to do this to make circular references of the strings in the BWT
        //0: node has not been visited
        //1: node is not suffix
        //2: node is suffix
        sdsl::int_vector<2> sep_nt(g.tree.int_nodes()+1, 0);
        sdsl::bit_vector suffix_nodes(root_children+2, false);

        bool continue_trav, is_suffix;
        std::stack<size_t> stack;

        size_t n_children = g.tree.n_children(grammar_t::root);

        for(size_t i=2;i<=n_children+1;i++){

            tmp_node = g.tree.node_select(i);

            if(!g.tree.is_leaf(tmp_node)){

                //TODO: can we have a terminal as a child of the root?
                continue_trav = g.label(tmp_node) > g.sigma;

                while(continue_trav){
                    stack.push(tmp_node);

                    //get the right-most pt_child
                    tmp_node = g.tree.child(tmp_node, g.tree.n_children(tmp_node));

                    if(g.tree.is_leaf(tmp_node)){
                        label = g.label(tmp_node);
                        if(label<=g.sigma){//it points to a terminal symbol
                            is_suffix = (label==0); //is the pointed terminal a sep symbol?
                            while(!stack.empty()){ //mark all the ancestors
                                tmp_node = stack.top();
                                stack.pop();
                                int_rank = g.tree.int_rank(tmp_node);
                                sep_nt[int_rank] = (is_suffix + 1);
                            }
                            continue_trav = false;
                            suffix_nodes[i] = is_suffix;
                        }else{
                            int_rank = label-g.sigma;
                            tmp_node = g.tree.int_select(int_rank);
                            if(sep_nt[int_rank]){
                                is_suffix = (sep_nt[int_rank] == 2);
                                while(!stack.empty()){
                                    tmp_node = stack.top();
                                    stack.pop();
                                    int_rank = g.tree.int_rank(tmp_node);
                                    sep_nt[int_rank] = is_suffix + 1;
                                }
                                continue_trav = false;
                                suffix_nodes[i] = is_suffix;
                            }
                        }
                    }
                }
            }else{
                label = g.label(tmp_node);
                suffix_nodes[i] = (label==0) ||
                                  (label>g.sigma && sep_nt[label-g.sigma]==2);
            }
        }
        return suffix_nodes;
    }

    static bool smaller_nt(const grammar_t &g, size_t node_map_a, size_t node_map_b,
                           const iter_alph_t &alph, const bit_vector_t &suffix_nt){

        size_t rank_a, rank_b, next_a, next_b;

        rank_a = alph.label2rank(g.label(g.tree.node_select(node_map_a)));
        rank_b = alph.label2rank(g.label(g.tree.node_select(node_map_b)));
        next_a = circular_next(node_map_a, suffix_nt);
        next_b = circular_next(node_map_b, suffix_nt);

        while(rank_a==rank_b && next_a != node_map_a && next_b != node_map_b){
            rank_a = alph.label2rank(g.label(g.tree.node_select(next_a)));
            rank_b = alph.label2rank(g.label(g.tree.node_select(next_b)));

            next_a = circular_next(next_a, suffix_nt);
            next_b = circular_next(next_b, suffix_nt);
        }
        return rank_a<rank_b;
    }

    /*
    static long quick_sort_par(const grammar_t &g, const iter_alph_t &alph, vector_t& nodes,
                               const bit_vector_t &suffix_nt, long low_idx, long high_idx){

        long pivot = nodes[high_idx], tmp;
        long i = low_idx-1;
        for(long j=low_idx;j<=high_idx-1;j++){
            if(smaller_nt(g, nodes[j], pivot, alph, suffix_nt)){
                i++;
                tmp = nodes[i];
                nodes[i] = nodes[j];
                nodes[j] = tmp;
            }
        }

        tmp = nodes[i+1];
        nodes[i+1] = nodes[high_idx];
        nodes[high_idx] = tmp;
        return i+1;
    }

    static void quick_sort(const grammar_t &g, const iter_alph_t &alph, vector_t& nodes,
                           const bit_vector_t &suffix_nt, long low_idx, long high_idx){
        if(low_idx<high_idx){
            long pi = quick_sort_par(g, alph, nodes, suffix_nt, low_idx, high_idx);
            quick_sort(g, alph, nodes, suffix_nt, low_idx, pi - 1);
            quick_sort(g, alph, nodes, suffix_nt, pi + 1, high_idx);
        }
    }
*/

    static void sort_nt(const grammar_t &g, const iter_alph_t &alph, vector_t& nodes,
                        const bit_vector_t::select_1_type& buckets_ss,
                        const bit_vector_t &suff_nodes){
        size_t start, end, len;
        for(size_t i=1;i<=alph.size();i++){
            start = buckets_ss(i);
            end = buckets_ss(i+1)-1;
            len = end-start+1;
            auto it = nodes.begin()+start;
            if((end-start)>0){
                std::sort(it, it+len,
                          [&](size_t l, size_t r){
                              return smaller_nt(g, l , r, alph, suff_nodes);
                          });
            }
        }
    }

    //only nonterminals at depth one: returns the node map of its circular next sibling
    inline static size_t circular_next(size_t node_map, const bit_vector_t &suffix_nt)  {
        if (suffix_nt[node_map]) {
            if (node_map > 2 && !suffix_nt[node_map - 1]) {
                node_map--;
                while (node_map > 1 && !suffix_nt[node_map]) node_map--;
                node_map++;
            }
        }else{
            node_map++;
        }
        return node_map;
    }

    //only nonterminals at depth one: returns the node map of its circular prev sibling
    inline static size_t circular_prev(size_t node_map, const bit_vector_t &suffix_nt) {
        assert(node_map>1);
        if (node_map == 2 || suffix_nt[node_map - 1]) {
            while (!suffix_nt[node_map]){
                node_map++;
            }
        } else {
            node_map--;
        }
        return node_map;
    }

    //for debugging: we can delete it in the future
    inline static void circular_print(const grammar_t &g, size_t node_map,
                                      const iter_alph_t &alph, const bit_vector_t &suffix_nt) {
        std::cout << alph.label2rank(g.label(g.tree.node_select(node_map))) << " ";
        size_t next = circular_next(node_map, suffix_nt);
        while (next != node_map) {
            std::cout << alph.label2rank(g.label(g.tree.node_select(next))) << " ";
            next = circular_next(next, suffix_nt);
        }
        std::cout << " " << std::endl;
    }

    static void light_sort(const grammar_t& g, const iter_alph_t& alph, vector_t& nodes,
                           bit_vector_t& b_limits, bit_vector_t::select_1_type& b_limits_ss, size_t& max_freq){

        size_t n_children = g.tree.n_children(grammar_t::root);

        //compute label frequencies
        vector_t sym_freqs(alph.size()+2, 0, sdsl::bits::hi(n_children)+1);
        for(size_t i=1;i<=n_children;i++){
            sym_freqs[alph.label2rank(g.label(g.tree.node_select(i+1)))]++;
        }
        b_limits = bit_vector_t(n_children+1, false);
        max_freq=0;
        size_t acc=0;
        for (auto const &freq : sym_freqs) {
            b_limits[acc] = true;
            acc+=freq;
            if(freq>max_freq) max_freq = freq;
        }
        b_limits[acc] = true;
        sdsl::util::clear(sym_freqs);
        sdsl::util::init_support(b_limits_ss, &b_limits);
        nodes = vector_t(n_children, 0, sdsl::bits::hi(n_children+2)+1);

        //place the nodes in the array to be sorted
        {
            vector_t bucket_pos(alph.size() + 1, 0, sdsl::bits::hi(max_freq) + 1);
            size_t bucket;
            for (size_t i = 1; i <= n_children; i++) {
                bucket = alph.label2rank(g.label(g.tree.node_select(i + 1))) + 1;
                nodes[b_limits_ss(bucket) + bucket_pos[bucket]] = i + 1;
                bucket_pos[bucket]++;
            }
        }
    }

public:
    static void build(const grammar_t&g, size_t n_threads, sdsl::cache_config &config, size_t& iter){

        std::cout<<"  Building the eBWT of the compressed string"<<std::endl;
        iter_alph_t alph;
        sdsl::load_from_file(alph, sdsl::cache_file_name("alph_iter_"+std::to_string(--iter), config));

        //circular sort of the nonterminals at depth one
        std::cout<<"    Computing suffix nonterminals"<<std::endl;
        bv_t suff_nodes = get_suffix_nodes(g);

        auto comparator = [&](const size_t &l, const size_t &r) {
            return smaller_nt(g, l, r, alph, suff_nodes);
        };

        auto accessor = [&](const size_t &node_map, const size_t& idx) {
            assert(idx==0);
            return alph.label2rank(g.label(g.tree.node_select(node_map)));
        };

        std::cout<<"    Sorting circular suffixes"<<std::endl;
        //compute symbol frequencies
        std::string nodes_file = sdsl::cache_file_name("cstring_nodes", config);
        std::string buckets_file = sdsl::cache_file_name("cstring_bck", config);
        {
            size_t n_children = g.tree.n_children(grammar_t::root);
            vector_t sym_freqs(alph.size()+1, 0, sdsl::bits::hi(n_children)+1);
            sdsl::int_vector_buffer<> nodes(nodes_file, std::ios::out);
            for(size_t i=1;i<=n_children;i++){
                nodes.push_back(i+1);
                sym_freqs[alph.label2rank(g.label(g.tree.node_select(i+1)))]++;
            }
            sdsl::store_to_file(sym_freqs, buckets_file);
            nodes.close();
        }
        //
        //parallel sort
        parallel_str_sort(nodes_file, comparator, accessor, alph.size(), n_threads, config, buckets_file);

        //run-len compress the eBWT and get the accumulative sums of its symbol frequencies
        std::string r_heads_file = sdsl::cache_file_name("r_heads_file", config);
        std::string r_len_file = sdsl::cache_file_name("r_len_file", config);
        {
            sdsl::int_vector_buffer<> nodes(nodes_file, std::ios::in);
            sdsl::int_vector_buffer<> r_heads_buff(r_heads_file, std::ios::out);
            sdsl::int_vector_buffer<> r_len_buff(r_len_file, std::ios::out);
            size_t head, node, len = 1;

            head = g.label(g.tree.node_select(circular_prev(nodes[0], suff_nodes)));
            head = alph.label2rank(head);

            for (size_t i = 1; i < nodes.size(); i++) {

                node = g.label(g.tree.node_select(circular_prev(nodes[i], suff_nodes)));
                node = alph.label2rank(node);

                if (head != node) {
                    r_heads_buff.push_back(head);
                    r_len_buff.push_back(len);
                    head = node;
                    len = 0;
                }
                len++;
            }
            r_heads_buff.push_back(head);
            r_len_buff.push_back(len);
            nodes.close(true);

            sdsl::int_vector_buffer<> acc_bck_counts(buckets_file, std::ios::in);
            size_t tmp=acc_bck_counts[0], tmp2;
            acc_bck_counts[0] = 0;
            for(size_t j=1;j<acc_bck_counts.size();j++){
                tmp2 = acc_bck_counts[j];
                acc_bck_counts[j] = tmp;
                tmp+=tmp2;
            }
        }

        std::cout<<"    Building the eBWT of the compressed string"<<std::endl;
        //compute the fm-index from the run-len compressed eBWT
        rlfm_index<> rlfmi(r_heads_file, r_len_file,  buckets_file);

        {
            sdsl::int_vector<> acc_sums(alph.size(), 0);
            std::string p_ranks_file = sdsl::cache_file_name("p_ranks", config);
            sdsl::int_vector_buffer<> p_ranks(p_ranks_file, std::ios::out);
            size_t i=0;
            for(auto const& sym : rlfmi.bwt_run_heads){
                p_ranks[i++] = acc_sums[sym]++;
            }
        }

        //invert the alphabet for the next iteration
        alph.invert_alphabet(config);

        sdsl::store_to_file(alph, sdsl::cache_file_name("alph_iter_"+std::to_string(iter), config));
        sdsl::store_to_file(rlfmi, sdsl::cache_file_name("rlfmi_iter_"+std::to_string(iter), config));

        if(remove(buckets_file.c_str())){
            std::cout<<"Error trying to remove "<<buckets_file<<std::endl;
        }
        if(remove(r_heads_file.c_str())){
            std::cout<<"Error trying to remove "<<r_heads_file<<std::endl;
        }
        if(remove(r_len_file.c_str())){
            std::cout<<"Error trying to remove "<<r_len_file<<std::endl;
        }

        std::string ssym_file = sdsl::cache_file_name("ssyms_iter_"+std::to_string(iter), config);
        std::string fsym_file = sdsl::cache_file_name("fsyms_iter_"+std::to_string(iter), config);
        if(remove(ssym_file.c_str())){
            std::cout<<"Error trying to remove "<<ssym_file<<std::endl;
        }
        if(remove(fsym_file.c_str())){
            std::cout<<"Error trying to remove "<<fsym_file<<std::endl;
        }
    }
};
#endif //LPG_COMPRESSOR_PROTBWT_ALGO_HPP
