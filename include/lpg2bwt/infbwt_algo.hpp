//
// Created by diego on 22-04-20.
//

#ifndef LPG_COMPRESSOR_INFBWT_ALGO_HPP
#define LPG_COMPRESSOR_INFBWT_ALGO_HPP

#include "iter_alphabet.hpp"
#include "protbwt_algo.hpp"
#include "cdt/rlfm_index.h"
#include "cdt/parallel_string_sort.hpp"
#include "cdt/hash_table.hpp"
#include "cdt/int_array.h"

template<class grammar_t>
class infbwt_algo{

    typedef sdsl::int_vector_buffer<>     buff_t;
    typedef sdsl::int_vector<>            vector_t;
    typedef sdsl::bit_vector              bit_vector_t;


    struct preproc_thread{
        const size_t                      start;
        const size_t                      end;
        o_file_stream<size_t>             q1_buff;
        buff_t                            sec_occ_buff;
        const rlfm_index<>&               prev_bwt;
        const iter_alph&                  prev_alph;
        const iter_alph&                  alph;
        const grammar_t&                  g;

        //the range [start, end[ is exclusive for the end
        preproc_thread(size_t start_, size_t end_, std::string prefix, const grammar_t& g_,
                       const iter_alph& prev_alph_, const iter_alph& alph_, const rlfm_index<>& prev_bwt_):
                       start(start_),
                       end(end_),
                       prev_bwt(prev_bwt_),
                       prev_alph(prev_alph_),
                       alph(alph_),
                       g(g_){
            std::string file1 = prefix+"_q1_"+std::to_string(start)+"_"+std::to_string(end-1);
            std::string file2 = prefix+"_seqc_occ_"+std::to_string(start)+"_"+std::to_string(end-1);
            q1_buff = o_file_stream<size_t>(file1, 1024*1024, std::ios::out);
            sec_occ_buff = buff_t(file2, std::ios::out);
        }

        void operator()(){

            size_t l_label, r_label, node, p_sib, left, right, parent, n_children, sym_rank, node_map, r_rank;
            bool hashed;

            for(size_t i=start;i<end;i++){

                r_label = prev_alph.rank2label(i);

                if(!alph.in_alph(r_label)){

                    sym_rank = prev_bwt.sym_freq(i);

                    while(!alph.in_alph(r_label)){

                        parent = g.tree.int_select(r_label-g.sigma);
                        n_children = g.tree.n_children(parent);

                        //right most child
                        node = g.tree.child(parent, n_children);
                        node_map = g.tree.node_map(node);
                        r_label = g.label(node);

                        //left sibling of the right most child
                        p_sib = g.tree.pred_0(node-1)+1;
                        l_label = g.label(p_sib);

                        hashed = false;

                        //right most child also belongs to the
                        // previous alphabet
                        if(prev_alph.in_alph(r_label)){

                            left = alph.label2rank(l_label);
                            right = g.tree.int_select(r_label-g.sigma);

                            if(alph.in_alph(r_label)){
                                r_rank = alph.label2rank(r_label);
                            }else{
                                r_rank = alph.label2rank(g.label(g.tree.child(right,1)));
                            }

                            //we hash only if the rank symbol of the left sibling
                            // is less or equal to the rank symbol of the first child
                            // of the right most node: this indicates that the left
                            // sibling has S-type
                            if(left<=r_rank){
                                right = g.tree.node_map(right);
                                /*if(g.tree.is_lm_child(p_sib)){
                                    val = g.tree.node_map(parent);
                                }else{
                                    val = node_map-1;
                                }*/
                                sec_occ_buff.push_back(left);
                                sec_occ_buff.push_back(right);
                                //sec_occ_buff.push_back(val);
                                sec_occ_buff.push_back(node_map-1);
                                hashed = true;
                            }
                        }

                        // we record all the unique suffixes that appear
                        // only inside the current phrase
                        for(size_t j=n_children-2;j>0;j--){
                            //new right node
                            node = p_sib;
                            node_map--;

                            //new left node
                            p_sib = g.tree.pred_0(node-1)+1;

                            //we skip the last suffix if it was hashed.
                            // We need to get the relative order among
                            // the secondary occurrences
                            if(j==(n_children-2) && hashed) continue;

                            q1_buff.push_back(sym_rank);
                            q1_buff.push_back(alph.label2rank(g.label(p_sib)));
                            q1_buff.push_back(node_map);
                        }
                        //assert(g.tree.is_lm_child(p_sib));
                    }
                }
            }
        }
    };

    struct run_buff_data{
        size_t lpos;
        uint32_t lbwt_sym;
    };

    struct lf_buff_data{
        uint32_t gt_lab;
        uint32_t bwt_sym;
        size_t node;
    };

    struct split_data{
        std::vector<std::string> q1_chunks;
        std::vector<std::string> q2_chunks;
        size_t l_q1_run{};
        size_t l_q2_run{};
        size_t alphabet_size{};
    };

    struct split_bwt_thread{

        const size_t                      start;
        const size_t                      end;
        o_file_stream<size_t>             q1;
        o_file_stream<size_t>             q2;
        sdsl::int_vector_buffer<>         p_ranks;
        const rlfm_index<>&               prev_bwt;
        const iter_alph&                  prev_alph;
        const iter_alph&                  alph;
        const grammar_t&                  g;

        const bit_hash_table<size_t, 44>& sec_occ;

        bit_hash_table<run_buff_data,96,size_t,7> run_bwt_ht;
        run_buff_data                             rbwt_data;

        bit_hash_table<lf_buff_data,128,size_t,6> lf_ht;
        lf_buff_data                              lf_data;

        //the range [start, end[ is exclusive for the end
        split_bwt_thread(size_t start_, size_t end_,
                         std::string prefix, const grammar_t& g_,
                         const iter_alph& prev_alph_, const iter_alph& alph_,
                         const rlfm_index<>& prev_bwt_,
                         const std::string& p_ranks_file,
                         const bit_hash_table<size_t,44>& sec_occ_,
                         void * run_bwt_addr,
                         void * lf_addr): start(start_),
                                          end(end_),
                                          prev_bwt(prev_bwt_),
                                          prev_alph(prev_alph_),
                                          alph(alph_),
                                          g(g_),
                                          sec_occ(sec_occ_),
                                          run_bwt_ht(524288, "", 0.8, run_bwt_addr),
                                          lf_ht(524288, "", 0.8, lf_addr){
            p_ranks = sdsl::int_vector_buffer<>(p_ranks_file, std::ios::in);
            std::string file1 = prefix+"_q1_"+std::to_string(start)+"_"+std::to_string(end-1);
            std::string file2 = prefix+"_q2_"+std::to_string(start)+"_"+std::to_string(end-1);
            q1 = o_file_stream<size_t>(file1, 1024*1024, std::ios::out);
            q2 = o_file_stream<size_t>(file2, 1024*1024, std::ios::out);
        }

        inline void insert_triplet(size_t fr, size_t bwt_sym, size_t rc) {
            rbwt_data = {q1.size() + 2, (uint32_t)bwt_sym};

            auto res = run_bwt_ht.insert(&rc, 64, rbwt_data);
            if(res.second){
                q1.push_back(fr);
                q1.push_back(bwt_sym);
                q1.push_back(rc);
            }else{
                run_bwt_ht.get_value_from(*res.first, rbwt_data);
                size_t block_a = (q1.size()-1)>>17UL; //block size 1024^2/sizeof(size_t)
                size_t block_b = (rbwt_data.lpos - 2) >> 17UL;
                if(rbwt_data.lbwt_sym == bwt_sym && block_a == block_b){
                    fr+=q1.read(rbwt_data.lpos - 2);
                    q1.write(rbwt_data.lpos - 2, fr);
                }else{
                    q1.push_back(fr);
                    q1.push_back(bwt_sym);
                    q1.push_back(rc);
                    rbwt_data = {q1.size() - 1, (uint32_t)bwt_sym};
                    run_bwt_ht.insert_value_at(*res.first, rbwt_data);
                }
            }
        }

        void operator()(){
            auto inter_end = prev_bwt.range_iterator_end();
            auto head_it = rlfm_index<>::head_iterator(prev_bwt, start);
            auto it_end = rlfm_index<>::head_iterator(prev_bwt, end);

            size_t l_label, node, f_node, r_label, bwt_sym, bwt_sym2, node_map, parent, p_sib, rc;
            int_array<size_t> tmp_pair(2, sdsl::bits::hi(g.tree.nodes())+1);
            bit_vector_t vs_nodes(g.tree.int_nodes()+g.sigma+1, false);

            while(head_it!=it_end){

                r_label = prev_alph.rank2label(head_it.head_sym);
                f_node = g.tree.int_select(r_label-g.sigma);
                node_map = g.tree.node_map(f_node);
                rc = alph.in_alph(r_label) ? node_map : g.tree.node_map(g.tree.child(f_node, 1));

                //LF mapping
                size_t head_r = p_ranks[head_it.head_pos];
                size_t lf = prev_bwt.bwt_run_sums_ss(prev_bwt.C[head_it.head_sym]+ head_r+1);
                auto lf_inter = prev_bwt.interval(lf, lf + (head_it.last-head_it.first));

                while(lf_inter!=inter_end){

                    auto res = lf_ht.insert(&lf_inter.head, 44, lf_data);
                    if(res.second){
                        l_label = prev_alph.rank2label(lf_inter.head);
                        node = g.tree.int_select(l_label-g.sigma);
                        while(!alph.in_alph(l_label)){
                            if(g.tree.is_leaf(node)){
                                node = g.tree.int_select(l_label-g.sigma);
                            }
                            node = g.tree.child(node, g.tree.n_children(node));
                            l_label = g.label(node);
                        }
                        bwt_sym = alph.label2rank(l_label);
                        lf_data.node = node;
                        lf_data.bwt_sym = bwt_sym;
                        lf_data.gt_lab = l_label;
                        lf_ht.insert_value_at(*res.first, lf_data);
                    }else{
                        lf_ht.get_value_from(*res.first, lf_data);
                        node = lf_data.node;
                        bwt_sym = lf_data.bwt_sym;
                        l_label = lf_data.gt_lab;
                    }

                    if(!prev_alph.in_alph(l_label)){
                        bwt_sym2 = alph.label2rank(g.label(g.tree.pred_0(node-1)+1));
                        tmp_pair.write(0, bwt_sym);
                        tmp_pair.write(1, node_map);
                        auto res2 = sec_occ.find(tmp_pair.data(), tmp_pair.n_bits());
                        if(res2.second){
                            size_t val = res2.first.value();
                            insert_triplet(lf_inter.freq, bwt_sym2, val);
                        }
                    }
                    insert_triplet(lf_inter.freq, bwt_sym, rc);
                    ++lf_inter;
                }
                //

                //decompress grammar symbols
                if(!alph.in_alph(r_label)){

                    size_t freq = head_it.last - head_it.first + 1;
                    parent = f_node;

                    while(!alph.in_alph(r_label)) {

                        if(g.tree.is_leaf(parent)){
                            parent = g.tree.int_select(r_label - g.sigma);
                        }
                        size_t n_children = g.tree.n_children(parent);

                        //right most child
                        node = g.tree.child(parent, n_children);
                        r_label = g.label(node);

                        //left sibling of the right most child
                        p_sib = g.tree.pred_0(node-1)+1;
                        l_label = g.label(p_sib);

                        //for the next iteration
                        parent = node;

                        if(!alph.in_alph(r_label) || prev_alph.in_alph(r_label)){

                            node = g.tree.int_select(r_label - g.sigma);
                            node_map = g.tree.node_map(node);

                            //a unique suffix with secondary occurrences, i.e,
                            // it appears only in one nonterminal, but it has
                            // secondary occurrences
                            if(n_children>2){
                                size_t pp_sib = g.tree.pred_0(p_sib-1)+1;
                                tmp_pair.write(0, alph.label2rank(l_label));
                                tmp_pair.write(1, node_map);
                                auto res = sec_occ.find(tmp_pair.data(), tmp_pair.n_bits());

                                if(res.second){
                                    size_t ll_label = g.label(pp_sib);
                                    insert_triplet(freq, alph.label2rank(ll_label), g.tree.node_map(p_sib));
                                }
                            }

                            rc = alph.in_alph(r_label) ? node_map : g.tree.node_map(g.tree.child(node, 1));

                            insert_triplet(freq, alph.label2rank(l_label), rc);
                        }else{
                            size_t rc_label, rc_node;
                            bwt_sym2 = alph.label2rank(l_label);
                            bwt_sym = alph.label2rank(r_label);
                            for (auto const &pair : head_it.bucket_list) {

                                rc_label = prev_alph.rank2label(pair.second);
                                rc_node = g.tree.int_select(rc_label-g.sigma);
                                node_map = g.tree.node_map(rc_node);

                                tmp_pair.write(0, bwt_sym);
                                tmp_pair.write(1, node_map);
                                auto res = sec_occ.find(tmp_pair.data(), tmp_pair.n_bits());
                                if(!res.second){
                                    q2.push_back(pair.first);
                                    q2.push_back(bwt_sym2);
                                    q2.push_back(bwt_sym);
                                    if(!alph.in_alph(rc_label)){
                                        q2.push_back(g.tree.node_map(g.tree.child(rc_node,1)));
                                    }else{
                                        q2.push_back(node_map);
                                    }
                                }
                            }
                        }
                    }
                }
                ++head_it;
            }
        }
    };

private:


    static void sort_tuples(const grammar_t& g, split_data& sp_data, sdsl::cache_config& config){

        //compute the distinct symbols that occur as a prefix in the distinct right context pairs
        bit_vector_t syms(sp_data.alphabet_size+1, false);
        size_t k, max_val=0,val, pos, bck, max_bwt_run=0, tot_syms=0;
        for(auto const& chunk : sp_data.q2_chunks){
            i_file_stream<size_t> q2(chunk, 1024*1024);
            tot_syms+=q2.size();
            k=1;
            for(size_t i=0;i<q2.size();i++){
                val = q2.read(i);
                if(val>max_val) max_val = val;
                if(k%4==0){
                    syms[q2.read(k-2)]=true;
                }
                k++;
            }
        }
        bit_vector_t::rank_1_type syms_rs(&syms);
        //

        //get the frequency of every prefix symbol
        vector_t bucket_counts(syms_rs(syms_rs.size())+1, 0, sdsl::bits::hi(tot_syms/4)+1);
        for(auto const& chunk : sp_data.q2_chunks){
            i_file_stream<size_t> q2(chunk, 1024*1024);
            for(size_t i=0;i<q2.size();i+=4){
                bucket_counts[syms_rs(q2.read(i+2))]++;
                if(q2.read(i)>max_bwt_run) max_bwt_run = q2.read(i);
            }
        }
        //

        //accumulative sums of the prefix symbols
        size_t tmp=bucket_counts[0], tmp2;
        bucket_counts[0] = 0;
        for(size_t j=1;j<bucket_counts.size();j++){
            tmp2 = bucket_counts[j];
            bucket_counts[j] = tmp;
            tmp+=tmp2;
        }
        //

        //stable sort of the tuples according to the prefix symbol in the right context pairs
        sdsl::int_vector<> bwt_rcon(tot_syms/2, 0, sdsl::bits::hi(g.tree.nodes())+1);
        vector_t sorted_q2(tot_syms,0,sdsl::bits::hi(max_val)+1);

        size_t f_width = sdsl::bits::hi(max_bwt_run)+1;
        size_t s_width = sdsl::bits::hi(sp_data.alphabet_size)+1;
        size_t width = f_width+s_width;
        sp_data.l_q2_run = max_bwt_run;

        bitstream<size_t> bwt_data;
        size_t n_bytes = INT_CEIL(width*(tot_syms/4), 8);
        bwt_data.stream_size = INT_CEIL(n_bytes, sizeof(size_t));
        bwt_data.stream = new size_t[bwt_data.stream_size];

        bitstream<size_t> tmp_data;
        tmp_data.stream_size = 2;
        tmp_data.stream  = new size_t[2];

        for(auto const& chunk : sp_data.q2_chunks){
            i_file_stream<size_t> q2(chunk, 1024*1024);

            for(size_t i=0;i<q2.size();i+=4){
                bck = syms_rs(q2.read(i+2));
                pos = bucket_counts[bck];

                tmp_data.write(0, f_width-1, q2.read(i));//frequency
                tmp_data.write(f_width, f_width+s_width-1, q2.read(i+1));//bwt_symbol
                bwt_data.write_chunk(tmp_data.stream, pos*width, (pos+1)*width-1);

                bwt_rcon[pos*2] = q2.read(i+2);
                bwt_rcon[pos*2+1] = q2.read(i+3);

                bucket_counts[bck]++;
            }
            q2.close(true);
        }

        std::ofstream ofs(sdsl::cache_file_name("bwt_data2", config), std::ios::out | std::ios::binary);
        ofs.write((char *)bwt_data.stream, bwt_data.stream_size*sizeof(size_t));
        delete [] bwt_data.stream;
        delete [] tmp_data.stream;

        sdsl::util::clear(bucket_counts);
        //

        //get the distinct right context pairs and their frequencies in q2
        std::string rc_sorted_file = sdsl::cache_file_name("rc_sorted_2", config);
        std::string rc_freq_file = sdsl::cache_file_name("rc_freq_2", config);
        sdsl::int_vector_buffer<> rc_sorted_buff(rc_sorted_file, std::ios::out);
        sdsl::int_vector_buffer<> rc_freq_buff(rc_freq_file, std::ios::out);
        std::pair<size_t, size_t> head = {bwt_rcon[0], bwt_rcon[1]};
        size_t len=1;
        for(size_t i=2;i<bwt_rcon.size();i+=2){
            if(bwt_rcon[i]!=head.first || bwt_rcon[i+1]!=head.second){
                rc_sorted_buff.push_back(head.first);
                rc_sorted_buff.push_back(head.second);
                rc_freq_buff.push_back(len);
                head.first = bwt_rcon[i];
                head.second = bwt_rcon[i+1];
                len=0;
            }
            len++;
        }
        rc_sorted_buff.push_back(head.first);
        rc_sorted_buff.push_back(head.second);
        rc_freq_buff.push_back(len);

        rc_freq_buff.close();
        rc_freq_buff.close();
        //
    }

    /*
    static void s_triplets(const grammar_t& g, split_data& sp_data, sdsl::cache_config& config, const size_t& iter){

        //mark the distinct nodes of Q1
        std::string rc_sorted_file = sdsl::cache_file_name("ssyms_iter_"+std::to_string(iter-1), config);
        sdsl::int_vector_buffer<> rc_sorted(rc_sorted_file, std::ios::in);
        bit_vector_t rc_bv(g.tree.nodes(), false);
        for (auto &&i : rc_sorted) rc_bv[i] = true;
        bit_vector_t::rank_1_type rc_bv_rs(&rc_bv);

        //run-length compress Q1
        std::string rl_q1_file = sdsl::cache_file_name("rl_q1_" + std::to_string(iter - 1), config);
        {
            auto start1 = std::chrono::steady_clock::now();
            std::string tmp_rl_q1_file = sdsl::cache_file_name("tmp_rl_q1_" + std::to_string(iter - 1), config);
            sdsl::int_vector_buffer<> tmp_rl_q1(tmp_rl_q1_file, std::ios::out);
            std::vector<std::pair<size_t, size_t>> run_data(rc_sorted.size());
            std::vector<uint16_t> freq;
            freq.reserve(sp_data.q1_size);

            size_t node, bwt_sym, fr;
            for (auto const &chunk : sp_data.q1_chunks) {
                sdsl::int_vector_buffer<> q1(chunk, std::ios::in);
                auto it = q1.begin();
                auto it_end = q1.end();
                while(it!=it_end) {
                    fr = *it;
                    ++it;
                    bwt_sym = *it;
                    ++it;
                    node = rc_bv_rs(*it);
                    ++it;

                    if (run_data[node].first == 0 || run_data[node].second != bwt_sym) {
                        freq.push_back(fr); //bwt_sym frequency
                        //tmp_rl_q1.push_back(bwt_sym); //bwt_sym
                        //tmp_rl_q1.push_back(node); //right context
                        run_data[node] = {freq.size() - 1, bwt_sym};
                    } else if (run_data[node].second == bwt_sym) {
                        freq[run_data[node].first] += fr;
                    }
                }
            }

        }

        //global ranks of the context strings.
        // That is, considering the extended context strings
        sdsl::int_vector<> ranks(rc_sorted.size(),0,sdsl::bits::hi(rc_sorted.size())+1);
        size_t rank = 0;
        for (auto &&i : rc_sorted){
            ranks[rc_bv_rs(i)] = rank++;
        }
    }
    */

    static void sort_triplets(const grammar_t& g, split_data& sp_data, sdsl::cache_config& config, const size_t& iter){

        size_t max_bwt_run=0, tot_trip=0, rc, bwt_sym, fr;
        sdsl::int_vector<> b_counts;
        std::string c_q1_file = sdsl::cache_file_name("collapsed_q1"+std::to_string(iter-1), config);

        //compute the frequency and bucket of every distinct
        // right context symbol
        auto start = std::chrono::steady_clock::now();
        {
            o_file_stream<size_t> q1(c_q1_file, 1024*1024, std::ios::out);
            size_t rank=0, block_a, block_b;
            std::string rc_sorted_file = sdsl::cache_file_name("ssyms_iter_"+std::to_string(iter-1), config);
            sdsl::int_vector_buffer<> rc_sorted(rc_sorted_file, std::ios::in);
            bit_vector_t rc_bv(g.tree.nodes()+1, false);
            for (auto &&i : rc_sorted) rc_bv[i] = true;
            bit_vector_t::rank_1_type rc_bv_rs(&rc_bv);

            sdsl::int_vector<> ranks(rc_sorted.size(),0,sdsl::bits::hi(rc_sorted.size())+1);
            for (auto &&i : rc_sorted)  ranks[rc_bv_rs(i)] = rank++;

            std::string rc_ranks_file = sdsl::cache_file_name("ranks_"+std::to_string(iter-1), config);
            std::string rc_bv_file = sdsl::cache_file_name("rc_bv_"+std::to_string(iter-1), config);
            std::string rc_bv_rs_file = sdsl::cache_file_name("rc_bv_rs_"+std::to_string(iter-1), config);
            sdsl::store_to_file(ranks, rc_ranks_file);
            sdsl::store_to_file(rc_bv, rc_bv_file);
            sdsl::store_to_file(rc_bv_rs, rc_bv_rs_file);

            int_array<size_t> rc_fr(rc_sorted.size() + 1, sdsl::bits::hi(g.tree.nodes()) + 1);
            rc_fr.set(0, rc_sorted.size() + 1);

            run_buff_data data;
            void* buff = malloc(524288);
            bit_hash_table<run_buff_data,96,size_t,7> ht(524288, "", 0.8, buff);

            for(size_t j=0; j<sp_data.q1_chunks.size();j++){
                i_file_stream<size_t> tmp_q1(sp_data.q1_chunks[j], 1024*1024);
                if(j>0){
                    for(size_t i=0;i<tmp_q1.size();i+=3) {
                        fr = tmp_q1.read(i);
                        bwt_sym = tmp_q1.read(i+1);
                        rc =  tmp_q1.read(i+2);

                        data.lpos = q1.size()+2;
                        data.lbwt_sym = bwt_sym;
                        auto res = ht.insert(&rc, 64, data);
                        if(res.second){
                            if (fr > max_bwt_run) max_bwt_run = fr;
                            rc = ranks[rc_bv_rs(rc)];
                            rc_fr.write(rc, rc_fr[rc]+1);
                            q1.push_back(fr);
                            q1.push_back(bwt_sym);
                            q1.push_back(rc);
                        }else{
                            ht.get_value_from(*res.first, data);
                            block_a = (q1.size()-1)>>17UL; //block size 1024^2/sizeof(size_t)
                            block_b = (data.lpos-2)>>17UL;
                            if(data.lbwt_sym==bwt_sym && block_a==block_b){
                                fr += q1.read(data.lpos-2);
                                if (fr > max_bwt_run) max_bwt_run = fr;
                                q1.write(data.lpos-2, fr);
                            }else{
                                if (fr > max_bwt_run) max_bwt_run = fr;
                                rc = ranks[rc_bv_rs(rc)];
                                rc_fr.write(rc, rc_fr[rc]+1);
                                q1.push_back(fr);
                                q1.push_back(bwt_sym);
                                q1.push_back(rc);
                                data = {q1.size()-1, (uint32_t)bwt_sym};
                                ht.insert_value_at(*res.first, data);
                            }
                        }
                    }
                }else{
                    for(size_t i=0;i<tmp_q1.size();i+=3) {
                        fr = tmp_q1.read(i);
                        bwt_sym = tmp_q1.read(i+1);
                        rc = ranks[rc_bv_rs(tmp_q1.read(i+2))];
                        rc_fr.write(rc, rc_fr[rc]+1);
                        if (fr > max_bwt_run) max_bwt_run = fr;
                        q1.push_back(fr);
                        q1.push_back(bwt_sym);
                        q1.push_back(rc);
                    }
                }
                tmp_q1.close(true);
            }
            free(buff);
            tot_trip=q1.size()/3;

            //we require this information for the next step
            std::string rc_freq_file = sdsl::cache_file_name("rc_freq_1", config);
            std::ofstream ofs(rc_freq_file, std::ios::out | std::ios::binary);
            ofs.write((char *)rc_fr.bits.stream, rc_fr.bits.stream_size*sizeof(size_t));
            //

            std::string tmp_bck_file = sdsl::cache_file_name("tmp_bck_file", config);
            sdsl::int_vector_buffer<> acc_bck_buff(tmp_bck_file,
                                                 std::ios::out,
                                                 1024*1024,
                                                 sdsl::bits::hi((tot_trip))+1);
            size_t tmp = rc_fr[0], tmp2;
            acc_bck_buff.push_back(0);
            for (size_t i = 1; i < rc_fr.size(); i++) {
                tmp2 = rc_fr[i];
                acc_bck_buff.push_back(tmp);
                tmp += tmp2;
            }
            //
            acc_bck_buff.close(false);
            sdsl::load_from_file(b_counts, tmp_bck_file);
            if(remove(tmp_bck_file.c_str())){
                std::cout<<"Error trying to remove file: "<<tmp_bck_file<<std::endl;
            }
            q1.close();
        }
        auto end = std::chrono::steady_clock::now();
        std::cout<<"Bck freqs :          " << std::chrono::duration_cast<std::chrono::microseconds>(end - start).count() << " µs" << std::endl;
        //

        //sort the BWT symbols in q1 according the buckets
        start = std::chrono::steady_clock::now();
        size_t pos;
        size_t f_width = sdsl::bits::hi(max_bwt_run)+1;
        size_t s_width = sdsl::bits::hi(sp_data.alphabet_size)+1;
        size_t width = f_width+s_width;
        sp_data.l_q1_run = max_bwt_run;
        bitstream<size_t> bwt_data;//BWT symbols and their runs
        size_t n_bytes = INT_CEIL(width*tot_trip, 8);
        bwt_data.stream_size = INT_CEIL(n_bytes, sizeof(size_t));
        bwt_data.stream = reinterpret_cast<size_t*>(malloc(bwt_data.stream_size*sizeof(size_t)));
        memset(bwt_data.stream, 0, bwt_data.stream_size*sizeof(size_t));

        bitstream<size_t> tmp_data;
        tmp_data.stream_size = 2;
        tmp_data.stream  = new size_t[2];

        i_file_stream<size_t> q1(c_q1_file, 1024*1024);
        for(size_t i=0;i<q1.size();i+=3) {
            fr =  q1.read(i);
            bwt_sym = q1.read(i+1);
            rc = q1.read(i+2);
            pos = b_counts[rc]++;
            tmp_data.write(0, f_width - 1, fr);//frequency
            tmp_data.write(f_width, f_width + s_width - 1, bwt_sym);//bwt_symbol
            bwt_data.write_chunk(tmp_data.stream, pos * width, (pos + 1) * width - 1);
        }
        q1.close(true);

        std::ofstream ofs(sdsl::cache_file_name("bwt_data1", config), std::ios::out | std::ios::binary);
        ofs.write((char *)bwt_data.stream, bwt_data.stream_size*sizeof(size_t));

        free(bwt_data.stream);
        delete [] tmp_data.stream;

        end = std::chrono::steady_clock::now();
        std::cout<<"Reorder BWT symbols: " << std::chrono::duration_cast<std::chrono::microseconds>(end - start).count() << " µs" << std::endl;
        //
    }

    static void preproc_data(const grammar_t& g, const rlfm_index<>& prev_bwt,
                             const iter_alph& prev_alph, const iter_alph& alph,
                             size_t n_threads, bit_hash_table<size_t, 44>& sec_occ,
                             sdsl::cache_config& config){

        //This function hashes the (potential) secondary occurrences of the phrases and store the frequencies
        // of the suffixes that appear in one phrase

        //parallel
        n_threads = std::max<size_t>(1, n_threads);
        std::vector<preproc_thread> threads_data;
        size_t syms_per_thread = INT_CEIL(prev_alph.size(), n_threads);
        size_t start,end;
        std::string tmp_prefix=config.dir+"/pre_proc";
        for(size_t i=0;i<n_threads;i++){
            start = i*syms_per_thread;
            end = std::min<size_t>((i+1)*syms_per_thread, prev_alph.size());
            threads_data.emplace_back(start, end, tmp_prefix, g,
                                      prev_alph, alph, prev_bwt);
        }
        std::vector<pthread_t> threads(threads_data.size());
        for (size_t j = 0; j < threads_data.size(); j++) {
            int ret = pthread_create(&threads[j],
                                     nullptr,
                                     &pre_proc_par,
                                     (void *) &threads_data[j]);
            if (ret != 0) {
                printf("Error: pthread_create() failed\n");
                exit(EXIT_FAILURE);
            }
        }
        for (size_t j = 0; j < threads_data.size(); j++) {
            pthread_join(threads[j], nullptr);
        }
        //

        //gather the information that was obtained in the parallel run
        size_t val;
        int_array<size_t> pair(2, sdsl::bits::hi(g.tree.nodes())+1);
        std::string bwt_trip_file = sdsl::cache_file_name("bwt_trip", config);
        o_file_stream<size_t> q1(bwt_trip_file, 1024*1024, std::ios::out);

        for(size_t i=0;i<n_threads;i++){
            for(size_t j=0; j<threads_data[i].q1_buff.size(); j++){
                q1.push_back(threads_data[i].q1_buff.read(j));
            }
            for(size_t j=0;j<threads_data[i].sec_occ_buff.size();j+=3){
                pair.write(0, threads_data[i].sec_occ_buff[j]);
                pair.write(1, threads_data[i].sec_occ_buff[j+1]);
                val = threads_data[i].sec_occ_buff[j+2];
                sec_occ.insert(pair.data(), pair.n_bits(), val);
            }
            threads_data[i].q1_buff.close(true);
            threads_data[i].sec_occ_buff.close(true);
        }
        //
    }

    static void* pre_proc_par(void * data){
        auto t_data = reinterpret_cast<preproc_thread*>(data);
        (*t_data)();
        pthread_exit(nullptr);
    }

    static void* split_bwt_par(void * data){
        auto t_data = reinterpret_cast<split_bwt_thread*>(data);
        (*t_data)();
        pthread_exit(nullptr);
    }

    static void split_bwt(const grammar_t &g, split_data &sp_data,
                          sdsl::cache_config &config, size_t &iter, size_t n_threads) {

        std::string prev_alph_file = sdsl::cache_file_name("alph_iter_"+std::to_string(iter), config);
        iter_alph prev_alph;
        sdsl::load_from_file(prev_alph, prev_alph_file);

        std::string alph_file = sdsl::cache_file_name("alph_iter_"+std::to_string(iter-1), config);
        iter_alph alph;
        sdsl::load_from_file(alph, alph_file);

        std::string prev_bwt_file=sdsl::cache_file_name("rlfmi_iter_"+std::to_string(iter), config);
        rlfm_index<> prev_bwt;
        sdsl::load_from_file(prev_bwt, prev_bwt_file);

        bit_hash_table<size_t, 44> sec_occ;
        preproc_data(g, prev_bwt, prev_alph, alph, n_threads, sec_occ, config);

        //I save this for the next steps
        sp_data.alphabet_size = alph.size();

        //parallel
        n_threads = std::max<size_t>(1, n_threads);
        std::vector<split_bwt_thread> threads_data;
        size_t heads_per_thread = INT_CEIL(prev_bwt.runs(), n_threads);
        size_t start,end;
        std::string tmp_prefix=config.dir+"/rc";
        std::string p_ranks_file = sdsl::cache_file_name("p_ranks", config);

        //buffer for collapsing BWT symbols that are placed close to each other in B^{i}
        auto bwt_run_buffer = reinterpret_cast<char *>(malloc(524288*n_threads));
        auto lf_buffer = reinterpret_cast<char *>(malloc(524288*n_threads));
        char * tmp_buff = bwt_run_buffer;
        char * tmp_buff2 = lf_buffer;

        for(size_t i=0;i<n_threads;i++){
            start = i*heads_per_thread;
            end = std::min<size_t>((i+1)*heads_per_thread, prev_bwt.runs());
            threads_data.emplace_back(start, end, tmp_prefix, g,
                                      prev_alph, alph, prev_bwt,
                                      p_ranks_file, sec_occ, tmp_buff, tmp_buff2);
            tmp_buff +=524288;
            tmp_buff2 +=524288;
        }

        std::vector<pthread_t> threads(threads_data.size());
        for (size_t j = 0; j < threads_data.size(); j++) {
            int ret = pthread_create(&threads[j],
                                     nullptr,
                                     &split_bwt_par,
                                     (void *) &threads_data[j]);
            if (ret != 0) {
                printf("Error: pthread_create() failed\n");
                exit(EXIT_FAILURE);
            }
        }
        for (size_t j = 0; j < threads_data.size(); j++) {
            pthread_join(threads[j], nullptr);
        }

        free(bwt_run_buffer);
        free(lf_buffer);

        std::string bwt_trip_file = sdsl::cache_file_name("bwt_trip", config);
        sp_data.q1_chunks.push_back(bwt_trip_file);
        for(size_t i=0;i<n_threads;i++){
            sp_data.q1_chunks.push_back(threads_data[i].q1.filename());
            sp_data.q2_chunks.push_back(threads_data[i].q2.filename());
        }

        if(remove(p_ranks_file.c_str())){
            std::cout<<"Error trying to remove "<<p_ranks_file<<std::endl;
        }
        if(remove(prev_alph_file.c_str())){
            std::cout<<"Error trying to remove "<<prev_alph_file<<std::endl;
        }
        if(remove(prev_bwt_file.c_str())){
            std::cout<<"Error trying to remove "<<prev_bwt_file<<std::endl;
        }
        //
    }

    inline static size_t insert_symbols(i_file_stream<size_t>& ifs,
                                        bitstream<size_t>& tmp_data,
                                        size_t& width1,
                                        size_t& width2,
                                        sdsl::int_vector_buffer<>& bwt_r_heads,
                                        sdsl::int_vector_buffer<>& bwt_r_len,
                                        size_t n_bwt_syms,
                                        size_t& bwt_head,
                                        size_t& tail,
                                        size_t& pos) {

        size_t sym, sym_freq,  acc=0;
        size_t width = width1+width2;
        for(size_t i=0;i<n_bwt_syms;i++){
            ifs.read_chunk(tmp_data.stream, pos*width, (pos+1)*width-1);
            sym_freq = tmp_data.read(0, width1-1);
            sym = tmp_data.read(width1, width-1);

            pos++;
            if(bwt_r_heads.size()==0 || bwt_head!=sym){
                bwt_r_heads.push_back(sym);
                bwt_r_len.push_back(sym_freq);
                bwt_head = sym;
                tail++;
            }else {
                bwt_r_len[tail-1]+=sym_freq;
            }
            acc+=sym_freq;
        }
        return acc;
    }

    /*
    // The adjective "implicit" or "explicit" indicates if the right
    // context of a BWT symbol can be directly extracted from a
    // grammar tree node or not.
    static void finish_sort(const grammar_t &g, const size_t &iter, sdsl::cache_config &config) {

        iter_alph alph;
        std::string alph_file = sdsl::cache_file_name("alph_iter_"+std::to_string(iter-1), config);
        sdsl::load_from_file(alph, alph_file);

        //fsyms are the bucket sizes of the sorted elements of Q1
        std::string new_alph_bck_file = sdsl::cache_file_name("fsyms_iter_"+std::to_string(iter-1), config);
        sdsl::int_vector_buffer<> tmp_new_alph_bck(new_alph_bck_file, std::ios::in);
        sdsl::int_vector_buffer<> new_alph_bck("some_file", std::ios::out);
        size_t tmp = tmp_new_alph_bck[0], tmp2;

        new_alph_bck.push_back(0);
        for (size_t i = 1; i < tmp_new_alph_bck.size(); i++) {
            tmp2=tmp_new_alph_bck[i];
            new_alph_bck.push_back(tmp);
            tmp += tmp2;
        }
        //

        //explicit right contexts
        std::string rc_sorted_1_file = sdsl::cache_file_name("ssyms_iter_"+std::to_string(iter-1), config);
        sdsl::int_vector_buffer<> rc_sorted_1(rc_sorted_1_file, std::ios::in);
        std::string rc1_r_file = sdsl::cache_file_name("rc1_r_"+std::to_string(iter-1), config);
        sdsl::int_vector_buffer<> rc1_ranks(rc1_r_file, std::ios::out);
        //

        //implicit right contexts
        std::string rc_sorted_2_file = sdsl::cache_file_name("rc_sorted_2", config);
        sdsl::int_vector_buffer<> rc_sorted_2(rc_sorted_2_file,std::ios::in);
        std::string rc2_r_file = sdsl::cache_file_name("rc2_r_"+std::to_string(iter-1), config);
        sdsl::int_vector_buffer<> rc2_ranks(rc1_r_file, std::ios::out);
        //

        size_t pref_1, pref_2;
        size_t c2=0, c1=0, next_pos, n_bck=1, n_bck_pos, rank=0;
        bool decomp_1, new_bck_finished;

        n_bck_pos = new_alph_bck[n_bck];
        while(c1<rc_sorted_1.size() && c2<rc_sorted_2.size()){

            next_pos = new_alph_bck[rc_sorted_2[c2]];
            while(c1<next_pos){
                rc1_ranks.push_back(rank++);

                c1++;
                if(c1>=n_bck_pos) n_bck_pos = new_alph_bck[++n_bck];
            }
            if(c1==rc_sorted_1.size()) break;
            pref_1 = alph.access_sym(g, rc_sorted_1[c1], 0);

            if(pref_1!=rc_sorted_2[c2]){
                while(c2<rc_sorted_2.size() && pref_1>rc_sorted_2[c2]){
                    rc2_ranks.push_back(rank++);

                    c2+=2;
                    if(c2>=rc_sorted_2.size() ||
                      (c2<rc_sorted_2.size() && rc_sorted_2[c2]!=rc_sorted_2[c2-2])){
                        n_bck_pos = new_alph_bck[++n_bck];
                    }
                }
            }else{
                new_bck_finished = false;
                decomp_1=false;

                while(!new_bck_finished){

                    if(decomp_1){
                        pref_1 = alph.access_sym(g, rc_sorted_1[c1], 0);
                    }else{
                        pref_2 = rc_sorted_2[c2];
                    }

                    if(pref_1<rc_sorted_2[c2] ||
                       (pref_1==rc_sorted_2[c2] &&
                        alph.compare_phrases(g, rc_sorted_1[c1]+1, rc_sorted_2[c2+1]))){
                        rc1_ranks.push_back(rank++);

                        c1++;
                        decomp_1 = true;
                    }else{
                        rc2_ranks.push_back(rank++);
                        c2+=2;
                        decomp_1 = false;
                    }

                    if(pref_1!=(n_bck-1) || c1 >= rc_sorted_1.size()){
                        while(c2 < rc_sorted_2.size() && rc_sorted_2[c2]==(n_bck-1)){
                            rc2_ranks.push_back(rank++);
                            c2+=2;
                        }
                        decomp_1 = false;
                        new_bck_finished = true;
                    }else if(pref_2!=(n_bck-1) || c2 >= rc_sorted_2.size()){

                        pref_1 = alph.access_sym(g, rc_sorted_1[c1], 0);
                        while(c1<rc_sorted_1.size() && pref_1==(n_bck-1)){
                            rc1_ranks.push_back(rank++);
                            pref_1 = alph.access_sym(g, rc_sorted_1[++c1], 0);
                        }
                        decomp_1 = true;
                        new_bck_finished = true;
                    }
                }
                n_bck_pos = new_alph_bck[++n_bck];
            }
        }

        while(c1<rc_sorted_1.size()){
            rc1_ranks.push_back(rank++);
            c1++;
        }

        while(c2<rc_sorted_2.size()){
            rc2_ranks.push_back(rank++);
            c2+=2;
        }
        rc_sorted_1.close(false);
        rc_sorted_2.close(false);
        new_alph_bck.close(true);
    }
     */

    // The adjective "implicit" or "explicit" indicates if the right
    // context of a BWT symbol can be directly extracted from a
    // grammar tree node or not.
    static void combine_sorted(const grammar_t &g, split_data &sp_data,
                               const size_t &iter, sdsl::cache_config &config) {

        iter_alph alph;
        std::string alph_file = sdsl::cache_file_name("alph_iter_"+std::to_string(iter-1), config);
        sdsl::load_from_file(alph, alph_file);

        //data structures to build the new BWT
        std::string new_rl_bwt_heads_file = sdsl::cache_file_name("new_rl_bwt_heads", config);
        std::string new_rl_bwt_len_file = sdsl::cache_file_name("new_rl_bwt_len", config);
        sdsl::int_vector_buffer<> bwt_r_heads(new_rl_bwt_heads_file, std::ios::out, 1024*1024, sdsl::bits::hi(alph.size())+1);
        sdsl::int_vector_buffer<> bwt_r_len(new_rl_bwt_len_file, std::ios::out);

        std::string new_alph_bck_file = sdsl::cache_file_name("fsyms_iter_"+std::to_string(iter-1), config);
        sdsl::int_vector_buffer<> new_alph_bck(new_alph_bck_file, std::ios::in);

        size_t tmp = new_alph_bck[0], tmp2;
        new_alph_bck[0] = 0;
        for (size_t i = 1; i < new_alph_bck.size(); i++) {
            tmp2=new_alph_bck[i];
            new_alph_bck[i] = tmp;
            tmp += tmp2;
        }
        std::string new_alph_bck_acc_file = sdsl::cache_file_name("new_alph_bck_acc", config);
        sdsl::int_vector_buffer<> new_alph_bck_acc_buff(new_alph_bck_acc_file, std::ios::out);
        //

        //data structures encoding the BWT symbols with *explicit* right contexts
        std::string rc_sorted_1_file = sdsl::cache_file_name("ssyms_iter_"+std::to_string(iter-1), config);
        std::string rc_freq_1_file = sdsl::cache_file_name("rc_freq_1", config);
        std::string bwt_data1_file = sdsl::cache_file_name("bwt_data1", config);
        sdsl::int_vector_buffer<> rc_sorted_1(rc_sorted_1_file, std::ios::in);
        i_file_stream<size_t> rc_freq_1(rc_freq_1_file, 1024*1024);
        i_file_stream<size_t> bwt_data1(bwt_data1_file, 1024*1024);
        size_t fr_width = sdsl::bits::hi(g.tree.nodes()) + 1;
        //

        //data structures encoding the BWT symbols with *implicit* right contexts
        std::string rc_sorted_2_file = sdsl::cache_file_name("rc_sorted_2", config);
        std::string rc_freq_2_file = sdsl::cache_file_name("rc_freq_2", config);
        std::string bwt_data2_file = sdsl::cache_file_name("bwt_data2", config);
        sdsl::int_vector_buffer<> rc_sorted_2(rc_sorted_2_file,std::ios::in);
        sdsl::int_vector_buffer<> rc_freq_2(rc_freq_2_file,std::ios::in);
        i_file_stream<size_t> bwt_data2(bwt_data2_file, 1024*1024);
        //

        size_t pref_1, pref_2, rc_fr1=0;
        size_t c2=0, c1=0, next_pos, bwt_head=0, tail=0, x=0,y=0, bck_acc=0, n_bck=1, n_bck_pos;
        bool new_bck_finished;
        new_alph_bck_acc_buff.push_back(0);
        size_t width1 = sdsl::bits::hi(sp_data.l_q1_run)+1;
        size_t width2 = sdsl::bits::hi(sp_data.l_q2_run)+1;
        size_t alph_width = sdsl::bits::hi(alph.size())+1;

        bitstream<size_t> tmp_data;
        tmp_data.stream_size = 2;
        tmp_data.stream = new size_t[2];

        n_bck_pos = new_alph_bck[n_bck];

        sdsl::int_vector<>        ranks;
        bit_vector_t              rc_bv;
        bit_vector_t::rank_1_type rc_bv_rs;

        std::string rc_ranks_file = sdsl::cache_file_name("ranks_"+std::to_string(iter-1), config);
        std::string rc_bv_file = sdsl::cache_file_name("rc_bv_"+std::to_string(iter-1), config);
        std::string rc_bv_rs_file = sdsl::cache_file_name("rc_bv_rs_"+std::to_string(iter-1), config);
        sdsl::load_from_file(ranks, rc_ranks_file);
        sdsl::load_from_file(rc_bv, rc_bv_file);
        sdsl::load_from_file(rc_bv_rs, rc_bv_rs_file);
        rc_bv_rs.set_vector(&rc_bv);

        size_t rank_l, rank_r, left, right, prev_right=0, prev_left=0, r_fsym=alph.size();
        bool left_is_smaller, enc_last_sym=false;

        while(c1<rc_sorted_1.size() && c2<rc_sorted_2.size()){

            pref_2 = rc_sorted_2[c2];//next rc2 context

            next_pos = new_alph_bck[pref_2];//number of rc1 contexts before the next rc2 context

            while(c1<next_pos){//bucket contains only rc1 contexts
                rc_freq_1.read_chunk(&rc_fr1, c1*fr_width, (c1+1) * fr_width -1);
                bck_acc +=insert_symbols(bwt_data1, tmp_data, width1, alph_width,
                                         bwt_r_heads, bwt_r_len, rc_fr1,
                                         bwt_head, tail, x);
                c1++;
                if(c1>=n_bck_pos){
                    new_alph_bck_acc_buff.push_back(bck_acc);
                    n_bck_pos = new_alph_bck[++n_bck];
                }
            }
            if(c1==rc_sorted_1.size()) break;

            size_t diff=1;
            while(next_pos==new_alph_bck[pref_2+diff]){//bucket contains only rc2 contexts
                bck_acc +=insert_symbols(bwt_data2, tmp_data, width2, alph_width,
                                         bwt_r_heads, bwt_r_len, rc_freq_2[c2/2],
                                         bwt_head, tail, y);
                c2+=2;
                if(c2>=rc_sorted_2.size() || (c2<rc_sorted_2.size() && rc_sorted_2[c2]!=rc_sorted_2[c2-2])){
                    new_alph_bck_acc_buff.push_back(bck_acc);
                    n_bck_pos = new_alph_bck[++n_bck];
                }
                diff++;
            }
            pref_1 = pref_2+diff-1;

            if(diff==1){//bucket contain both, rc1 and rc2 contexts

                new_bck_finished = false;
                size_t n_rc1 = new_alph_bck[pref_1+1];
                while(!new_bck_finished){

                    left = rc_sorted_1[c1]+1;
                    right = rc_sorted_2[c2+1];

                    if(left!=prev_left){
                        if(!rc_bv[left]){
                            size_t node = g.tree.node_select(left);
                            size_t label = g.label(node);
                            enc_last_sym = alph.in_alph(label);
                            if(!enc_last_sym){
                                node = g.tree.child(g.tree.int_select(label-g.sigma), 1);
                                rank_l = ranks[rc_bv_rs(g.tree.node_map(node))];
                            }else{
                                rank_l = alph.label2rank(label);
                            }
                        }else{
                            enc_last_sym = false;
                            rank_l = ranks[rc_bv_rs(left)];
                        }
                    }

                    if(right!=prev_right){
                        rank_r = ranks[rc_bv_rs(right)];
                        //lazy initialization
                        r_fsym = enc_last_sym ? alph.access_sym(g, right, 0) : alph.size();
                    }

                    if(!enc_last_sym){
                        left_is_smaller = (rank_l < rank_r);
                    }else{
                        if(r_fsym==alph.size()) r_fsym = alph.access_sym(g,right,0);
                        left_is_smaller = (rank_l < r_fsym);
                    }

                    prev_left = left;
                    prev_right =  right;

                    if(left_is_smaller){
                        rc_freq_1.read_chunk(&rc_fr1, c1*fr_width, (c1+1) * fr_width -1);
                        bck_acc +=insert_symbols(bwt_data1, tmp_data, width1, alph_width,
                                                 bwt_r_heads, bwt_r_len, rc_fr1, bwt_head,
                                                 tail, x);
                        c1++;
                        if(c1==n_rc1){
                            while(c2<rc_sorted_2.size() && rc_sorted_2[c2]==pref_2){
                                bck_acc +=insert_symbols(bwt_data2, tmp_data, width2, alph_width,
                                                         bwt_r_heads, bwt_r_len, rc_freq_2[c2/2],
                                                         bwt_head, tail, y);
                                c2+=2;
                            }
                            new_bck_finished = true;
                        }
                    }else {
                        bck_acc +=insert_symbols(bwt_data2, tmp_data, width2, alph_width,
                                                 bwt_r_heads, bwt_r_len, rc_freq_2[c2/2],
                                                 bwt_head, tail, y);
                        c2+=2;

                        if(c2>=rc_sorted_2.size() || rc_sorted_2[c2]!= pref_2){
                            while(c1<n_rc1){
                                rc_freq_1.read_chunk(&rc_fr1, c1*fr_width, (c1+1) * fr_width -1);
                                bck_acc +=insert_symbols(bwt_data1, tmp_data, width1, alph_width,
                                                         bwt_r_heads, bwt_r_len, rc_fr1, bwt_head,
                                                         tail, x);
                                c1++;
                            }
                            new_bck_finished = true;
                        }
                    }
                }
                new_alph_bck_acc_buff.push_back(bck_acc);
                n_bck_pos = new_alph_bck[++n_bck];
            }
        }

        while(c1<rc_sorted_1.size()){
            rc_freq_1.read_chunk(&rc_fr1, c1*fr_width, (c1+1) * fr_width -1);
            bck_acc +=insert_symbols(bwt_data1, tmp_data, width1, alph_width,
                                     bwt_r_heads, bwt_r_len, rc_fr1,
                                     bwt_head, tail, x);
            c1++;
            if(c1>=rc_sorted_1.size() || c1>=n_bck_pos){
                new_alph_bck_acc_buff.push_back(bck_acc);
                if(c1<rc_sorted_1.size()){
                    n_bck_pos = new_alph_bck[++n_bck];
                }
            }
        }

        while(c2<rc_sorted_2.size()){
            bck_acc +=insert_symbols(bwt_data2, tmp_data, width2, alph_width,
                                     bwt_r_heads, bwt_r_len, rc_freq_2[c2/2],
                                     bwt_head, tail, y);
            c2+=2;
            if(c2>=rc_sorted_2.size() ||
               rc_sorted_2[c2-2]!= rc_sorted_2[c2]){
                new_alph_bck_acc_buff.push_back(bck_acc);
            }
        }

        rc_sorted_1.close(true);
        rc_freq_1.close(true);
        bwt_data1.close(true);

        rc_sorted_2.close(true);
        rc_freq_2.close(true);
        new_alph_bck.close(true);
        bwt_data2.close(true);

        if(remove(rc_ranks_file.c_str())){
            std::cout<<"Error trying to remove "<<rc_ranks_file.c_str()<<std::endl;
        }
        if(remove(rc_bv_file.c_str())){
            std::cout<<"Error trying to remove "<<rc_bv_file.c_str()<<std::endl;
        }
        if(remove(rc_bv_rs_file.c_str())){
            std::cout<<"Error trying to remove "<<rc_bv_rs_file.c_str()<<std::endl;
        }

        delete[] tmp_data.stream;
    }

public:
    void static infer(const grammar_t& g, size_t n_threads, sdsl::cache_config& config, size_t& iter);
};

template<class grammar_t>
void infbwt_algo<grammar_t>::infer(const grammar_t &g, const size_t n_threads,
                                   sdsl::cache_config &config, size_t& iter) {

    auto start = std::chrono::steady_clock::now();
    protbwt_algo<grammar_t>::build(g, n_threads, config, iter);
    std::string alph_file, rlfmi_file;
    auto end = std::chrono::steady_clock::now();
    std::cout<<"      Time: "<<std::chrono::duration_cast<std::chrono::microseconds>(end - start).count() << " µs" << std::endl;

    while(iter>1) {

        std::cout<<"  Building the eBWT for the parse number "<<(iter-1)<<std::endl;
        split_data sp_data;

        //infer the next BWT from the current BWT
        std::cout<<"    Splitting previous eBWT"<<std::endl;
        start = std::chrono::steady_clock::now();
        split_bwt(g, sp_data, config, iter, n_threads);
        end = std::chrono::steady_clock::now();
        std::cout<<"      Time: "<<std::chrono::duration_cast<std::chrono::microseconds>(end - start).count() << " µs" << std::endl;

        std::cout<<"    Sorting the right contexts of the eBWT symbols"<<std::endl;

        start = std::chrono::steady_clock::now();
        sort_tuples(g, sp_data, config);
        end = std::chrono::steady_clock::now();
        std::cout<<"      Time: "<<std::chrono::duration_cast<std::chrono::microseconds>(end - start).count() << " µs" << std::endl;

        //finish_sort(g, iter,config);

        start = std::chrono::steady_clock::now();
        sort_triplets(g, sp_data, config, iter);
        end = std::chrono::steady_clock::now();
        std::cout<<"      Time: "<<std::chrono::duration_cast<std::chrono::microseconds>(end - start).count() << " µs" << std::endl;

        std::cout<<"    Combining sorted data"<<std::endl;
        start = std::chrono::steady_clock::now();
        combine_sorted(g, sp_data, iter, config);
        end = std::chrono::steady_clock::now();
        std::cout<<"      Time: "<<std::chrono::duration_cast<std::chrono::microseconds>(end - start).count() << " µs" << std::endl;
        //

        //Build the RLFM-index from the current BWT
        {
            std::string r_heads_file = sdsl::cache_file_name("new_rl_bwt_heads", config);
            std::string r_len_file = sdsl::cache_file_name("new_rl_bwt_len", config);
            std::string bck_acc_file = sdsl::cache_file_name("new_alph_bck_acc", config);
            if(iter>2){

                std::cout<<"    Creating the new eBWT"<<std::endl;
                rlfmi_file = sdsl::cache_file_name("rlfmi_iter_" + std::to_string(iter - 1), config);

                start = std::chrono::steady_clock::now();
                rlfm_index<> rlfmi(r_heads_file, r_len_file, bck_acc_file);
                end = std::chrono::steady_clock::now();
                std::cout<<"      Time: "<<std::chrono::duration_cast<std::chrono::microseconds>(end - start).count() << " µs" << std::endl;

                sdsl::int_vector<> acc_sums(sp_data.alphabet_size, 0);
                std::string p_ranks_file = sdsl::cache_file_name("p_ranks", config);
                sdsl::int_vector_buffer<> p_ranks(p_ranks_file, std::ios::out);
                size_t i=0;
                for(auto const& sym : rlfmi.bwt_run_heads){
                    p_ranks[i++] = acc_sums[sym]++;
                }

                sdsl::store_to_file(rlfmi, rlfmi_file);

                if (remove(r_len_file.c_str())) {
                    std::cout << "Error trying to remove " << r_len_file << std::endl;
                }
                if (remove(r_heads_file.c_str())) {
                    std::cout << "Error trying to remove " << r_heads_file << std::endl;
                }
                if (remove(bck_acc_file.c_str())) {
                    std::cout << "Error trying to remove " << bck_acc_file << std::endl;
                }
                std::cout<<"    Iter stats:"<<std::endl;
                std::cout<<"      BWT size: "<<rlfmi.size()<<std::endl;
                std::cout<<"      BWT runs: "<<rlfmi.runs()<<std::endl;
                std::cout<<"      Alphabet: "<<rlfmi.alphabet_size()<<std::endl;
            }
        }
        //

        //invert the current alphabet for the next iteration
        iter_alph alph;
        alph_file = sdsl::cache_file_name("alph_iter_" + std::to_string(iter - 1), config);
        sdsl::load_from_file(alph, alph_file);
        alph.invert_alphabet(config);
        sdsl::store_to_file(alph, alph_file);
        //

        --iter;
    }

    alph_file = sdsl::cache_file_name("alph_iter_" + std::to_string(iter), config);
    if(remove(alph_file.c_str())){
        std::cout<<"Error trying to remove file "<<alph_file<<std::endl;
        exit(1);
    }

    //TODO refactor this
    //move the resulting file to output
    /*char buf[BUFSIZ];
    size_t size;
    int source = open(rlfmi_file.c_str(), O_RDONLY, 0);
    int dest = open(output_file.c_str(), O_WRONLY | O_CREAT, 0644);
    while ((size = read(source, buf, BUFSIZ)) > 0) {
        write(dest, buf, size);
    }
    close(source);
    close(dest);
    if(remove(rlfmi_file.c_str())){
        std::cout<<"Error trying to rename file "<<rlfmi_file<<" to "<<output_file<<std::endl;
        std::cout<<std::strerror(errno)<<std::endl;
        exit(1);
    }*/
    //
}
#endif //LPG_COMPRESSOR_INFBWT_ALGO_HPP
